# Running a case

From within case directory:  

1.    Define run in `./settings/settingsfile.m4`
2.    Preprocess the settingsfile by executing `../scripts/preprocSettings ./settings/settingsfile.m4` to generate `settingsfile` 
3.    Run via `[mpirun [args]] EIGER -s ./settings/settingsfile -i ./data [other args]`

also consult the main [Eiger repository](https://bitbucket.org/skuechlin/eiger)

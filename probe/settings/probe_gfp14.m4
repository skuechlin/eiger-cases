changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__delay, 1000)
define(__last_ts, 2500)

{
	"description": [
	"HYPERSONIC FLOW OF A PLANETARY PROBE",
	"D     = 0.2 M",
	"N     = 6.6639264e+19 M-3",
	"MFP   = 0.02 M",
	"KN_D  = 0.03",
	"NU    = 19942.772 S-1",
	"1./NU = 5.0143479e-05 S",
	"DX    = MFP"
	]
	,"simulation" : {
		"n timesteps"	: calc(__last_ts+1),
		"info interval" : 1
	},
	"time step control": { "dt"	: 5.44e-7 },
	"algorithm": {
        "type"   : "FOKKER-PLANCK"
        ,"model" : "gfp14"
        //"type"	: "FP-DSMC",
		//,"DSMC"	: {
		//	"type" : "DSMC-MF"
		//}
		//,"FOKKER-PLANCK" : {
        //	"type" : "FOKKER-PLANCK",
        //  "model" : "gfp14"
		//}
	},
	"simulation box": {
		"origin": [ -0.04, -0.07, -0.07 ],
		"extent": [ 0.14, 0.14, 0.14 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
        "m"	    	: 4.65e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
		"Zrot_inf"	: 18.1,
        "Trot_ref"	: 91.5,
        "Cvib_1"	: 9.1,
        "Cvib_2"	: 220.0,
        "Tvib_ref"	: 3340.0
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 32,
	           "nj"    : 32,
	           "nk"    : 32,
	           "level" : 4
	       },
	       "ordering"          : "hilbert Butz",//"morton",//
	       "coordinate system" : { "type" : "cartesian" }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    		//	"output"	    : {
    		//		"interval" : 25,
    		//		"last"     : calc(__delay + 250)
    		//	},
    			"averaging" : { "type" 	: "none" }
    		},
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : 250
    				,"first"	: __delay
    			},
    			"averaging"	: {
    				"type" 	: "switched",
    				"switch"  : __delay, 
                     "first"  : {"type" : "exponential", "window size" : 50},
                     "second" : {"type" : "arithmetic"}
                 }
    		}
    	]
    	, "grid adaption" : {
    	    "cell volume correction" : {
    	       	"correct cell volumes" 	: true,
    	     	"remove empty cells"	: true,
    	      	"boundaries"			: "PlanetaryProbe"
    	    }
            , "dynamic" : {
				  "criterion" : "mfp",
				  "threshold" : 2.0,
				  "grid"	  : "sample",
				  "first" 	  : 50,
				  "last" 	  : __delay,
				  "interval"  : 50
			}
    	}
    	, "load balancing" : {
    		"grid" 	    : "sample",
    		"first"     : 25,
    		"interval"  : 25,
    		"last"	    : __delay,
    		"threshold"	: 0.98
    	}
	},
	"condition": [
        {
          "name"  : "reference",
          "n"     : 3.72e20,
          "number of particles" : 400e6
        },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 3.72e20,
			"T"		: 13.3,
			"Ma"	: 20.2
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 300
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "PlanetaryProbe",
            "level"     : 2,
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {
					"format"	: "stl",
					"name"		: "planetary_probe_meter.stl",
					"tol"		: 1e-6,
					"flip normals" : false
				}
			}
            , "output" : {
                "interval"  : 2500
                ,"first"	: 2500
            } 
            ,"sample"	: {
               "averaging"	: {
                    "type" 	: "switched",
                    "switch"  : __delay, 
                     "first"  : {"type" : "exponential", "window size" : 50},
                     "second" : {"type" : "arithmetic"}
                 }
            } 
        },
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

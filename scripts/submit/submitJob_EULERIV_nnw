#!/bin/csh

#set echo
#set verbose

# test if we have two arguments, exit with warning if we don't
if ($# != 5) then
 # we can't (AFAIK) redirect stdout to stderr in tcsh
 echo "Usage $0 nCores nProcs time jobname settingsFile"
 exit 1
endif

# set number of cores
set nCores = ${1}

# set number of mpi procs
set nProcs = ${2}

# set time
set time = ${3}

# check nCores is multiple of nProcs (assure same number of cores per proc)
@ nCoresIsValid = ( ( (${nCores} / ${nProcs}) * ${nProcs}) == ${nCores} )

if ( ${nCoresIsValid} != 1 ) then
 echo "nCores must be multiple of nProcs"
 exit 1
endif

# set jobname
set jName = ${4}

# set settings file
set sFile = ${5}

# check if settings file exists and is readable, exit with warning otherwise
if ( -r ${sFile} ) then
else
	echo "settings file '${sFile}' not found or not readable"
	exit 1
endif

# make sure out dir exists
set oDir = "${SCRATCH}/${jName}_OUT"

if ( -d ${oDir} ) then
else
	# create out dir
	mkdir ${oDir}
	
	# check if creation succeeded, exit with warning otherwise
	if ( -d ${oDir} ) then
	else
		echo "failed to create output directory '${oDir}'"
		exit 1
	endif
	
	
endif

# set in dir
set iDir = "${HOME}/data"

# check if in dir exists, exit with warning otherwise
if ( -d ${iDir} ) then
else
	echo "input directory '${iDir}' not found"
	exit 1
endif

# set cout file
set ooFile = "${oDir}/cout_${jName}.txt"

# set cerr file
set eoFile = "${oDir}/cerr_${jName}.txt"

# set executable
set exec = "${HYBRID}/EulerIV/HYBRID_V2"

# check if executable exists and is executable, exit with warning otherwise
if ( -x ${exec} ) then
else
	echo "executable '${exec}' not found or not executable"
	exit 1
endif


# number of threads per mpi proc:
#@ nThreads = ( 2 * (${nCores} / ${nProcs}))

@ nCoresPerProc = ((${nCores} / ${nProcs}))

@ nThreadsPerProc = ( 2 * ${nCoresPerProc} )

# map by node or socket?
if (${nCoresPerProc} > 18) then
	set mapObj = node
else
	set mapObj = socket
endif

set bsubCmd = "bsub -J ${jName} -outdir ${oDir}"


# set mem to 2560, will reserve  61.44GB per node, which seems to leave enough space for OS on 64GB nodes
# higher values will lead to scheduling to fat nodes
#set bsubRsrc = '"'"span[ptile=12] select[ht && ib] rusage[mem=2560]"'"'

set bsubRsrc = '"select[avx512f] span[ptile=36] rusage[mem=5120]"'

set bsubCmd = "${bsubCmd} -n ${nCores} -R ${bsubRsrc} -W ${time}" 

set bsubCmd = "${bsubCmd} -oo ${ooFile} -eo ${eoFile}"

# set mpi options
set mpiCmd = "mpirun"

# OpenMpi >= 1.8
#set mpiCmd = "${mpiCmd} -n ${nProcs}  --map-by ${mapObj}:PE=${nThreads} --bind-to core -report-bindings"
set mpiCmd = "${mpiCmd} --n ${nProcs} --map-by node:PE=${nCoresPerProc} --bind-to core --report-bindings"
set mpiCmd = "${mpiCmd} --mca btl ^vader,openib,tcp --mca pml ucx"
#set mpiCmd = "${mpiCmd} --mca mpi_leave_pinned 0"
#set mpiCmd = "${mpiCmd} --mca opal_abort_delay -1"

# set environment
module purge
module load my_gcc/8.2.0
#module load my_open_mpi/2.1.2
#module load my_open_mpi/3.0.0
#module load my_open_mpi/3.1.0rc2
#module load new
#module load gcc/6.3.0
#module load open_mpi/2.1.1
module load my_open_mpi/3.1.3-ucx

#setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${HOME}/lib

setenv OMP_NUM_THREADS ${nThreadsPerProc}
setenv OMP_WAIT_POLICY active #passive #active
setenv OMP_PROC_BIND true
setenv OMP_PLACES threads
setenv OMP_SCHEDULE auto

setenv HYBRID_oDir ${oDir}
setenv MY_AFFINITY_HOSTFILE ${oDir}/hostAffinityFile

set preCmd = 'cp $LSB_AFFINITY_HOSTFILE'" ${MY_AFFINITY_HOSTFILE}; chmod 644 ${MY_AFFINITY_HOSTFILE}; printenv > ${oDir}/global_environment; "'unset LSB_AFFINITY_HOSTFILE;'

#preallocate memory for 2.5m particles per thread, will be enough for 240m on 48 cores
set mpiCmd = "'${preCmd} ${mpiCmd} ${exec} -s ${sFile} -i ${iDir} -o ${oDir} -n 2500000'"

# final command sequence
set cmd = "${bsubCmd} ${mpiCmd}"

setenv BSUB_CMD "${cmd}"

echo ""

# execute
eval "${cmd}"

echo ""
echo "executet:"
echo "${cmd}"
echo ""

exit 0

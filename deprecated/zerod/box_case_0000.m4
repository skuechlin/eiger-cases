changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__d, 1.0)

define(__xmin, 0)
define(__xmax, __d)
define(__ymin, 0)
define(__ymax, __d)
define(__zmin, 0)
define(__zmax, __d)

define(__particlespercell, 50)
define(__delay, 25000)
define(__interval,100)

{
	"description": [
	"A BOX OF NITROGEN AT EQUILIBRIUM"
	],
	
	"simulation" : {
		"n timesteps"	: 30001, // 5k averaging steps
		"info interval" : 1
	},
	
	"checkpoint" : {
		"interval"  : 5000,
		"first"	    : 5000
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 5.0e-9,	//[s] 
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "FOKKER-PLANCK"
	},


define(__lx, calc( abs(__xmax) + abs(__xmin) ))
define(__ly, calc( abs(__ymax) + abs(__ymin) ))
define(__lz, calc( abs(__zmax) + abs(__zmin) ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: 46.5e-27,		//[kg]
		"mu_ref" 	: 1.656e-5, 	//[N s m-2]
		"alpha" 	: 1.36,
		"omega" 	: 0.74,
		"T_ref" 	: 273.0,			//[K]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3340.0		//[K]
	},	

		
	"mpi": {
		"nx": 30,
		"ny": 30,
		"nz": 30  
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	
	define(__T, 5)
	define(__n, 1.0e20)
	
	"condition": [
		{
			"name"	: "initCond",
			"type"	: "stream",
			"n"		: __n, 	//[m-3]
			"T"		: __T,	//[K]
			"Ma"	: 0.0,
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "periodicCond",
			"type"	: "periodic",
			"distance" : __d
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "left",
			"condition" : "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			}
		},
		{
			"name"		: "right",
			"condition" : "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			}
		},
		{
			"name"		: "front",
			"condition" : "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},
		{
			"name"		: "back",
			"condition" : "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		},
		{
			"name"		: "top",
			"condition" : "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		},
		{
			"name"		: "bottom",
			"condition" : "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		}
		
	]
}
	

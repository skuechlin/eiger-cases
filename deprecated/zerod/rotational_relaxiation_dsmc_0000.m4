changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__d, 1.0) // box width


define(__xmin, 0.0)
define(__xmax, __d)
define(__ymin, 0.0)
define(__ymax, __d)
define(__zmin, 0.0)
define(__zmax, __d)


define(__particlespercell, 100000)

{
	"description": [
	"relaxation of rotational nonequilibrium",
	"validating DSMC and the Larsen-Borgnakke model",
	"BIRD 11.3"	
	],
	
	"simulation" : {
		"n timesteps"	: 100, 
		"info interval" : 1
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.0e-5,	//[s] 
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "DSMC-LB"
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


"gas model": {
		"type" 		: "VHS",
		"species" 	: "bird",
		"diatomic"	: true,
		"m"			: 5.0e-26,		//[kg]
		"d_ref" 	: 3.5e-10, 		//[m]
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3340.0		//[K]
	},	

		
	"mpi": {
		"nx": 1,
		"ny": 1, 
		"nz": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 1
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	// CONDITIONS
		
	"condition": [
		{
			"name"	: "initCond",
			"type"	: "stream",
			"n"		: 1.0e20, 	//[m-3]
			"T"		: 500,		//[K]
			"T rot"	: 1.0,		//[K]
			"Ma"	: 0.0,		//[-]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "periodicCond",
			"type"	: "periodic",
			"distance" : __d
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "right",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition" : "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		},
		{
			"name"		: "back",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		},	
		{
			"name"		: "front",
			"condition" : "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		}
	]
}
	

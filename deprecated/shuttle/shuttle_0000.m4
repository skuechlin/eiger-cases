changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__delay, 2500)

{
	"description": [
	"SPACE SHUTTLE REENTRY",
	"Moss, J., and G. Bird.",
	"''Direct simulation of transitional flow for hypersonic reentry conditions.''",
	"22nd Aerospace Sciences Meeting. 1984.",
	"Case 1",
	"AOA   = 41.15 deg",
	"U_inf = 7.5 km s-1",
	"T_inf = 180 K",
	"rho   = 2.184e-6 kg m-3", 
	"mfp   = 0.036 m",
	"Tw    = 950 K (800-1043)",
	"L     = 37.36 m"
	]
	,"simulation" : {
		"n timesteps"	: 5001,
		"info interval" : 1
	},
	"time step control": {
		"type"	: "fixed",
		"dt"		: 1.0e-5,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "FP-DSMC",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},
	"simulation box": {
		"origin": [ -75, -60, -60 ],
		"extent": [ 120, 120, 120 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"		: 4.65e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
		"Zrot_inf"	: 18.1,
    "Trot_ref"	: 91.5,
    "Cvib_1"	: 9.1,
    "Cvib_2"	: 220.0,
    "Tvib_ref"	: 3340.0
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 32,
	           "nj"    : 32,
	           "nk"    : 32,
	           "level" : 4
	       },
	       "ordering" : "hilbert Butz",//"morton",//
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 50,
    				"last" : calc(__delay + 250)
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : 500
    				,"first"	   : __delay
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay"  : __delay,
    				"window size" : 50
    			}
    		}
    	]
    	, "grid adaption" : {
    	    "cell volume correction" : {
    	       	"correct cell volumes" 	: true,
    	     	  "remove empty cells"	: true,
    	      	"boundaries"			: "shuttle"
    	    }
    		,	"refine intersected cells" : [ {"boundaries" : "shuttle","level" : 2} ]
			  , "dynamic" : {
				  "criterion" : "Np", //"mfp",
				  "threshold" : 15000, // "2.0",
				  "grid"		  : "sample",
				  "first" 	  : 50,
				  "last" 	  : __delay,
				  "interval"  : 50
			}
    	}
    	, "load balancing" : {
    		"grid" 		  : "sample",
    		"first" 	  : 25,
    		"interval"  : 25,
    		"last"		  : __delay,
    		"threshold"	: 0.98
    	}
	},
	"condition": [
    {
      "name"  : "reference",
      "rho"   : 2.184e-6,
      "number of particles" : 400e6
    },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"rho"	: 2.184e-6,
			"T"		: 180,
			"U"	    : -7500
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 950
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "shuttle",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {
					"format"	: "stl",
					"name"		: "shuttle_new.stl",
					"per meter" : 39.37,
					"flip normals" : false,
					"roll" : 90.00,
					"pitch" : 41.15
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay
				}
			},
			"output"	: {
				"first"     : 2500,
				"interval"  : 2500
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

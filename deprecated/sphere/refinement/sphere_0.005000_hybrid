{
	"description": [
	"SUPERSONIC FLOW AROUND SPHERE",
	"TIMESTEP BY CFL",
	"REF SPEED 1932 MS-1",
	"MEAN COLL TIME 6.68E-6s",
	"MFP 0.002666 M"
	],
	"simulation" : {
		"n timesteps"	: 15001,
		"info interval" : 1
	},
	"checkpoint" : {
		"interval"  : 2500,
		"first"	    : 2500
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 1.25e-6,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
        "Kn_crit": 2.0
	},
	"simulation box": {
		"origin": [ -0.5, -0.5, -0.5 ],
		"extent": [ 1, 1, 1 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"mpi": {
		"nx": 1,
		"ny": 200,
		"nz": 200
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 100
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : true,
			"rx"		: 200,
			"ry"		: 1,
			"rz"		: 1
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 100,
				"first"	    : 5000
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 5000
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 5.0e20,
			"T"		: 300,
			"Ma"	: 6,
			"particles per cell" : 25
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 1000
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "sphere",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {
					"format"	: "stl",
					"name"		: "Sphere.stl",
					"tol"		: 1e-6,
					"flip normals" : false
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: 5000
				}
			},
			"output"	: {
				"first"     : 5000,
				"interval"  : 100
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

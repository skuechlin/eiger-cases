{
	"description": [
	"SUPERSONIC FLOW AROUND SPHERE",
	"TIMESTEP BY CFL",
	"REF SPEED 1932 MS-1",
	"MEAN COLL TIME 6.68E-6s",
	"MFP 0.002666 M"
	],
	"simulation" : {
		"n timesteps"	: 20001,
		"info interval" : 1
	},
	"checkpoint" : {
		"interval"  : 5000,
		"first"	    : 5000
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 0.78125e-6,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "DSMC-MF"
		},
	"simulation box": {
		"origin": [ -0.5, 0.0, 0.0 ],
		"extent": [ 1, 0.5, 0.5 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"mpi": {
		"nx": 320,
		"ny": 1,
		"nz": 1
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 200
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"ry"		: 160,
			"rz"		: 160
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 200,
				"first"	    : 10000
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 10000
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 5.0e20,
			"T"		: 300,
			"Ma"	: 6,
			"particles per cell" : 25
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 1000
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "sphere",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {
					"format"	: "stl",
					"name"		: "SphereQuarter.stl",
					"tol"		: 1e-6,
					"flip normals" : false
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: 10000
				}
			},
			"output"	: {
				"first"     : 10000,
				"interval"  : 200
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

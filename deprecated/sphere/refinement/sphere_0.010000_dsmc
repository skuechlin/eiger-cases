{
	"description": [
	"SUPERSONIC FLOW AROUND SPHERE",
	"TIMESTEP BY CFL",
	"REF SPEED 1932 MS-1",
	"MEAN COLL TIME 6.68E-6s",
	"MFP 0.002666 M"
	],
	"simulation" : {
		"n timesteps"	: 12501,
		"info interval" : 1
	},
	"checkpoint" : {
		"interval"  : 2500,
		"first"	    : 2500
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.5e-6,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "DSMC-MF"
		},
	"simulation box": {
		"origin": [ -0.5, -0.5, -0.5 ],
		"extent": [ 1, 1, 1 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"mpi": {
		"nx": 100,
		"ny": 1,
		"nz": 1
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 100
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 100,
			"rz"		: 100
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 100,
				"first"	    : 2500
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 2500
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 5.0e20,
			"T"		: 300,
			"Ma"	: 6,
			"particles per cell" : 100
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: 1000
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "sphere",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "stl",
				"file"	: "Sphere.stl",
				"tol"	: 1e-6,
				"flip normals" : false
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: 2500
				}
			},
			"output"	: {
				"first"     : 2500,
				"interval"  : 100
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		}
	]
}

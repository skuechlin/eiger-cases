changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, -0.5)
define(__xmax, 0.5)
define(__ymin, 0.0)
define(__ymax, 0.5)
define(__zmin, 0.0)
define(__zmax, 0.5)


define(__infile_wall, "SphereQuarter.stl")
define(__tol,1e-6)

define(__particlespercell, 25)
define(__delay, 10000)
define(__interval,200)

{
	"description": [
	"SUPERSONIC FLOW AROUND SPHERE",
	"TIMESTEP BY CFL",
	"REF SPEED 1932 MS-1",
	"MEAN COLL TIME 6.68E-6s",
	"MFP 0.002666 M"
	],
	
	"simulation" : {
		"n timesteps"	: 20001, // 10k averaging steps
		"info interval" : 1
	},
	
	"checkpoint" : {
		"interval"  : 5000,
		"first"	    : 5000
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 1.0e-6,	//[s] based on .5 * (1m/250) / (1932m/s)
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "DSMC-MF" 
		},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,		//[kg]
		"d_ref" 	: 4.17e-10, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"mpi": {
		"nx": 250,
		"ny": 1, 
		"nz": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"ry"		: 125,
			"rz"		: 125
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	//CONDITIONS
	
	define(__Tin, 300)
	define(__Tw, 1000)
	define(__n, 5.0e20)
	define(__Ma, 6)
	
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: __Ma,		//[-]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],

	"state": [
		{
			"name"	: "surfaceState",
			"T"		: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "sphere",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {				
					"format"	: "stl",
					"name"		: __infile_wall,
					"tol"		: __tol,
					"flip normals" : false
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},	
		{
			"name"		: "front",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},		
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}		
	]
}
	

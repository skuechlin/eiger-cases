changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, -0.5)
define(__xmax, 0.5)
define(__ymin, -0.5)
define(__ymax, 0.5)
define(__zmin, -0.5)
define(__zmax, 0.5)


define(__infile_wall, "Sphere.stl")
define(__tol,1e-6)

define(__particlespercell, 100)
define(__delay, 2500)
define(__interval,100)

{
	"description": [
	"SUPERSONIC FLOW AROUND SPHERE",
	"TIMESTEP BY CFL",
	"REF SPEED 1932 MS-1",
	"MEAN COLL TIME 6.68E-6s",
	"MFP 0.002666 M"
	],
	
	"simulation" : {
		"n timesteps"	: 12501, // 10k averaging steps
		"info interval" : 1
	},
	
	"checkpoint" : {
		"interval"  : 2500,
		"first"	    : 2500
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.5e-6,	//[s] based on .5 * (1m/100) / (1932m/s)
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
        "Kn_crit": 2.0
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,		//[kg]
		"d_ref" 	: 4.17e-10, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"mpi": {
		"nx": 100,
		"ny": 1, 
		"nz": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 100,
			"rz"		: 100
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	//CONDITIONS
	
	define(__Tin, 300)
	define(__Tw, 1000)
	define(__n, 5.0e20)
	define(__Ma, 6)
	
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: __Ma,		//[-]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "sphere",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "stl",
				"file"	: __infile_wall,
				"tol"	: __tol,
				"flip normals" : false
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		},	
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},		
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		}		
	]
}
	

changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, -0.5)
define(__xmax, 0.5)
define(__ymin, 0.0)
define(__ymax, 0.5)
define(__zmin, 0.0)
define(__zmax, 0.5)


define(__infile_wall, "SphereQuarter.stl")
define(__tol,1e-6)

define(__particlespercell, 50)
define(__delay, 1000)
define(__nts,calc(2*__delay+1))
define(__interval,100)


	define(__n, 6.664e19)
	define(__dt,5.0e-5)
	define(__ni,50)
	define(__nj,calc(__ni/2))
	define(__nk,__nj)

{
	"description": [
	"SUPERSONIC FLOW AROUND SPHERE",
	"D     = 0.2 M",
	"N     = 6.6639264e+19 M-3",
	"MFP   = 0.02 M",
	"KN_D  = 0.1",
	"NU    = 19942.772 S-1",
	"1./NU = 5.0143479e-05 S",
	"DX    = MFP"
	],
	
	"simulation" : {
		"n timesteps"	: __nts,
		"info interval" : 1
	},
	
	//"checkpoint" : {
	//	"interval"  : 5000,
	//	"first"	    : 5000
	//},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: __dt,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "DSMC-MF" 
		},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,		//[kg]
		"d_ref" 	: 4.17e-10, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"grid": {
	
	   "topology" : {	   
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : __ni,
	           "nj"    : __nj,
	           "nk"    : __nk,
	           "level" : 2
	       },	   
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 2 
    	       	,"cell volume correction" : {
    	       		"correct cell volumes" 	: true,
    	       		"remove empty cells"	: true,
    	       		"boundaries"			: "sphere"
    	       	}
    			//,"refine intersected cells" : "sphere"
    	},	   
	   
	   "data grid" : [
	   
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : __interval
    			},
    			"averaging"	    : {
    				"type" 	: "none"				
    			}
    		},
    		
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : __interval,
    				"first"	    : __delay
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay" : __delay,
    				"window size" : 100
    			}
    		}
    		
    	]

    	//,"load balancing" : {
    	//	"load grid" : "sample",
    	//	"first" 	: 25,
    	//	"interval" 	: 100,
    	//	"last"		: __delay,
    	//	"threshold"	: 0.98
    	//}
	
	},
		
	//CONDITIONS
	
	define(__Tin, 300)
	define(__Tw, 1000)
	define(__Ma, 6)
	
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: __Ma,		//[-]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],

	"state": [
		{
			"name"	: "surfaceState",
			"T"		: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "sphere",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {				
					"format"	: "stl",
					"name"		: __infile_wall,
					"tol"		: __tol,
					"flip normals" : false
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},	
		{
			"name"		: "front",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},		
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}		
	]
}
	

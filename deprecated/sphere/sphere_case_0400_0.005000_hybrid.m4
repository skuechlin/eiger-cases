changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, -0.5)
define(__xmax, 0.5)
define(__ymin, -0.5)
define(__ymax, 0.5)
define(__zmin, -0.5)
define(__zmax, 0.5)


define(__infile_wall, "Sphere.stl")
define(__tol,1e-6)

define(__particlespercell, 25)
define(__delay, 5000)
define(__interval,100)

{
	"description": [
	"SUPERSONIC FLOW AROUND SPHERE",
	"TIMESTEP BY CFL",
	"REF SPEED 1932 MS-1",
	"MEAN COLL TIME 6.68E-6s",
	"MFP 0.002666 M"
	],
	
	"simulation" : {
		"n timesteps"	: 15001, // 10k averaging steps
		"info interval" : 1
	},
	
	"time step control": {
		"dt"		: 1.25e-6	//[s] based on .5 * (1m/200) / (1932m/s)
	},
	
	"algorithm": {
		"type"	: "FP-DSMC",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
        "Kn_crit": 2.0
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,		//[kg]
		"d_ref" 	: 4.17e-10, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

"grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
               "type"  : "uniform",
               "ni"    : 200,
               "nj"    : 200,
               "nk"    : 200,
               "level" : 4
           },
           "ordering" : "hilbert chI",
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
                "name"  : "default",
                "level" : 4
        },
       "data grid" : [
            {
                "name"          : "sim",
                "is simulation grid" : true,
                "averaging"     : { "type"  : "none" },
                "output"        : { "interval" : __interval }
            },
            {
                "name"          : "sample",
                "particle grid" : "default",
                "output"        : { "first" : __delay, "interval" : __interval },
                "averaging" : {
                    "type"  : "switched",
                    "switch" : __delay,
                    "first" : { "type" : "none" },
                    "second" : { "type" : "arithmetic" }
                }
            }
        ]
        , "grid adaption" : {
            "cell volume correction" : {
                "correct cell volumes"  : true,
                  "remove empty cells"  : true,
                "boundaries"            : "sphere"
            }          
        }
        , "load balancing" : {
            "grid"      : "sim",
            "first"     : 50,
            "interval"  : 50,
            "last"      : 3500,
            "threshold" : 0.98
        }
    },
    
	//CONDITIONS
	
	define(__Tin, 300)
	define(__Tw, 1000)
	define(__n, 5.0e20)
	define(__Ma, 6)
	
	"condition": [
	    {
            "name"  : "reference",
            "n"     : __n,
            "number of particles" : 200e6
        },            
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: __Ma		//[-]
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],

	"state": [
		{
			"name"	: "surfaceState",
			"T"		: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "sphere",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {				
					"format"	: "stl",
					"name"		: __infile_wall,
					"tol"		: __tol,
					"flip normals" : false
				}
			},
			"sample"	: {
				"averaging"	: {
                    "type"  : "switched",
                    "switch" : __delay,
                    "first" : { "type" : "none" },
                    "second" : { "type" : "arithmetic" }			
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},	
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},		
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}		
	]
}
	

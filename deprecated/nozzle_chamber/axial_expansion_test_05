{
    "description": [
    "EXPANSION FLOW ALONG X AXIS",
    "DODO: SHIFT TO ORIGIN",
    "TIMESTEP BY CFL",
    "REF SPEED 491.3 MS-1",
    "MEAN COLL TIME ???????s",
    "MFP 0.002666 M"
    ],
    "simulation" : {
        "n timesteps"   : 10000,
        "info interval" : 10
    },
    "time step control": {
        "type"      : "fixed",
        "dt"        : 1.27e-8,
        "scaling"   : [
            [0, 1.0     ]
        ]
    },
    "algorithm": {
        "type"  : "FP-DSMC",
        "FOKKER-PLANCK"     : {"type"   : "FOKKER-PLANCK"},
        "DSMC"              : {"type"   : "DSMC-MF"}
        },
    "simulation box": {
        "origin": [ -0.0001, -0.0015, -0.0015 ],
        "extent": [ 0.003, 0.003, 0.003 ]
    },
    "gas model": {
        "type"      : "VSS",
        "species"   : "argon",
        "diatomic"  : false,
        "m"         : 6.63e-26,
        "d_ref"     : 4.17e-10,
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,
        "gamma"     : 1.67
    },
"grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
               "type"  : "uniform",
               "ni"    : 16,
               "nj"    : 16,
               "nk"    : 16,
               "level" : 5
           },
           "ordering" : "hilbert Butz",
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
                "name"  : "default",
                "level" : 5
        },
       "data grid" : [
            {
                "name"          : "sim",
                "is simulation grid" : true,
                "output"        : {
                    "interval" : 100
                },
                "averaging"     : {
                    "type"  : "none"
                }
            },
            {
                "name"          : "sample",
                "output"        : {
                    "interval"  : 100,
                    "first"     : 5000
                },
                "averaging" : {
                    "type"  : "delayed",
                    "delay" : 5000,
                    "window size" : 50
                }
            }
        ]
        ,"grid adaption" : {
            "dynamic" : {
               "criterion"    : "mfp",
                "grid"        : "sample",
                "threshold"   : 4.0,
                "first"       : 25,
                "last"        : 3000,
                "interval"    : 50,
               "level"       : 1
            }
        },
        "load balancing" : {
            "grid" : "sample",
            "first"     : 50,
            "interval"  : 25,
            "last"      : 5000,
            "threshold" : 0.98
        }
    },
    "condition": [
    {
    "name" : "reference",
      "n" : 1.19e26,
      "number of particles" : 300000000000
      },
        {
            "name"  : "streamCond",
            "type"  : "stream",
            "n"     : 1.19e26,
            "T"     : 223.22,
            "Ma"    : 1
        },
        {
            "name"  : "initCond",
            "type"  : "vacuum"
        },
        {
            "name"  : "specularCond",
            "type"  : "specular"
        },
        {
            "name"  : "vacuumCond",
            "type"  : "vacuum"
        },
        {
            "name"  : "surfaceCond",
            "type"  : "diffusive"
        },
        {
            "name"  : "voidCond",
            "type"  : "void"
        }
    ],
    "state": [
        {
            "name"  : "surfaceState",
            "T"     : 298
        }
    ],
    "initialization" : {
        "condition" : "initCond"
    },
    "boundary": [
        {
            "name"      : "right",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "right"
            }
        },
        {
            "name"      : "left",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "left"
            }
        },
        {
            "name"      : "inlet_disk",
            "condition" : {
                "front": "streamCond",
                "back": "voidCond"
            },
            "geometry"  : {
                "type"      : "disk",
                "center"    : [ 0.0, 0.0, 0.0 ],
                "normal"    : [1.0, 0.0, 0.0],
                "radius"    : 0.000050
            }
        },
        {
            "name"      : "top",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "top"
            }
        },
        {
            "name"      : "bottom",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "bottom"
            }
        },
        {
            "name"      : "front",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "front"
            }
        },
        {
            "name"      : "back",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "back"
            }
        }
    ]
}

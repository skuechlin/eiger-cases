changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, -0.001)
define(__xmax, 0.004)
define(__ymin, 0.0045)
define(__ymax, 0.0095)
define(__zmin, -0.0025)
define(__zmax, 0.0025)


define(__infile_wall, "chamber_nice-mesh_100microns.stl")
define(__tol,1e-8)
define(__nozzle_radius, 0.000050)
//define(__nozzle_length, 0.012)
define(__nozzle_exit_xcoord,0.0)
define(__nozzle_exit_ycoord,0.007)
define(__nozzle_exit_zcoord,0.0)

define(__particlespercell, 100000000)
define(__delay, 3000)
define(__interval, 100)

{
    "description": [
    "FLUID THROUGH NOZZLE INTO LASER GENERATION CHAMBER, VACUUM",
    "TIMESTEP BY CFL",
    "REF SPEED 491.3 MS-1",
    "MEAN COLL TIME ???????s",
    "MFP 0.002666 M"
    ],
    
    "simulation" : {
        "n timesteps"   : 5000, // 15k averaging steps
        "info interval" : 10
    },
    
    //"checkpoint" : {
    //  "interval"  : 5000,
    //  "first"     : 5000
    //},
    
    "time step control": {
        "type"      : "fixed",
        "dt"        : 1.27e-8,  //[s] based on 
        "scaling"   : [
            [0, 1.0     ]
        ]
    },
    
    "algorithm": {
        "type"  : "FP-DSMC",
        "FOKKER-PLANCK"     : {"type"   : "FOKKER-PLANCK"},
        "DSMC"              : {"type"   : "DSMC-MF"}
        },


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

    "simulation box": {
        "origin": [ __xmin, __ymin, __zmin ],
        "extent": [ __lx, __ly, __lz ]
    },


    "gas model": {
        "type"      : "VSS",
        "species"   : "argon",
        "diatomic"  : false,
        "m"         : 6.63e-26,     //[kg]
        "d_ref"     : 4.17e-10,     //[m]
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,        //[K]
        "gamma"     : 1.67          //[-] 1 + 2/f, f=3 (f: DoF)
    },  

        
"grid": {
    
       "topology" : {      
           "dimension" : 3,
           "discretization" : {
               "type"  : "uniform",
               "ni"    : 25,
               "nj"    : 25,
               "nk"    : 25,
               "level" : 5
           },      
           "ordering" : "morton",
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
    
       "particle grid" : {
                "name"  : "default",
                "level" : 5           
        },     
       
       "data grid" : [
       
            {
                "name"          : "sim",
                "is simulation grid" : true,
                "output"        : {
                    "interval" : __interval
                },
                "averaging"     : {
                    "type"  : "none"                
                }
            },
            
            {
                "name"          : "sample",
                // "particle grid" : "default",
                "output"        : {
                    "interval"  : __interval,
                    "first"     : __delay
                },
                "averaging" : {
                    "type"  : "delayed",
                    "delay" : __delay,
                    "window size" : 200
                }
            }
            
        ]
        ,"grid adaption" : {
            "cell volume correction" : {
                  "correct cell volumes"     : true,
                  "remove empty cells"       : true,
                  "boundaries"               : ["nozzle_100microns"]
            },

            "refine intersected cells" : [ 
                {"boundaries" : ["inlet_disk"],
                "level"        : 0},
                {"boundaries" : ["nozzle_100microns"],
                 "level"       : 2 }
            ],
            
            
            "dynamic" : {
               "criterion"    : "mfp",
                "grid"        : "sample",
                "threshold"   : 2.0,
                "first"       : 25,
                "last"        : 2500,
                "interval"    : 50
            }
        },
        "load balancing" : {
            "grid" : "sample",
            "first"     : 50,
            "interval"  : 50,
            "last"      : __delay,
            "threshold" : 0.98
        }
    
    },
        
    //CONDITIONS
    
    define(__Tin, 223.22)
    define(__Tw, 298)
    define(__n, 1.19e26)
    define(__Ma, 1)
    
    "condition": [
        {
            "name"  : "streamCond",
            "type"  : "stream",
            "n"     : __n,      //[m-3]
            "T"     : __Tin,    //[K]
            "Ma"    : __Ma,     //[-]
            "particles per cell" : __particlespercell
        },
        {
            "name"  : "initCond",
            "type"  : "vacuum"
        },
        {
            "name"  : "specularCond",
            "type"  : "specular"
        },
        {
            "name"  : "vacuumCond",
            "type"  : "vacuum"
        },
        {
            "name"  : "surfaceCond",
            "type"  : "diffusive"
        },
        {
            "name"  : "voidCond",
            "type"  : "void"
        }
    ],

    "state": [
        {
            "name"  : "surfaceState",
            "T"     : __Tw
        }
    ],
        
    "initialization" : {
        "condition" : "initCond"
    },
        
    "boundary": [
        {
            "name"      : "nozzle_100microns",
            "condition" : "surfaceCond",
            "state"     : "surfaceState",
            "geometry"  : {
                "type"  : "fe_collection",
                "file"  : {             
                    "format"    : "stl",
                    "name"      : __infile_wall,
                //    "tol"       : __tol,
                    "flip normals" : false
                }
            },

            //"sample"    : {
            //    "averaging" : {
            //        "type"  : "delayed",
            //        "delay" : __delay               
            //    }
            //},

            "output"    : {
            //    "first"     : __delay,
            //    "interval"  : __interval
            }
        },
//
// STANDARD INLETS & OUTLETS
        {
            "name"      : "right",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "right"
            }
        },
        {
            "name"      : "left",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "left"
            }
        },
//      {
//          "name"      : "inlet",
//          "condition" : "streamCond",
//          "geometry"  : {
//              "type"     : "parallelogram",
//              "box_face" : "left"
//          }
//      },
        {
            "name"      : "inlet_disk",
            "condition" : {
                "front": "streamCond",
                "back": "voidCond"
            },
            "geometry"  : {
                "type"      : "disk",
                "center"    : [ __nozzle_exit_xcoord, __nozzle_exit_ycoord, __nozzle_exit_zcoord ],
                "normal"    : [1.0, 0.0, 0.0],
                "radius"    : __nozzle_radius
            }
        },      
        {
            "name"      : "top",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "top"
            }
        },  
        {
            "name"      : "bottom",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "bottom"
            }
        },  
        {
            "name"      : "front",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "front"
            }
        },      
        {
            "name"      : "back",
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "back"
            }
        }       
    ]
}
    

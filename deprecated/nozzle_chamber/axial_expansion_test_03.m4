changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, -0.0001)    // define chamber size
define(__xmax, 0.0039)
define(__ymin, -0.0015) //former 0.0025 instead of 0.0015
define(__ymax, 0.0015)
define(__zmin, -0.0015)
define(__zmax, 0.0015)


define(__infile_wall, "chamber_nice-mesh_100microns.stl")       //define boundary condition and chamber design/structure
define(__tol,1e-8)                                              //tolerance for input of stl-file
define(__nozzle_radius, 0.000050)                               //define size of flow source
//define(__nozzle_length, 0.012)
define(__nozzle_exit_xcoord,0.0)                                //define flow source position 
define(__nozzle_exit_ycoord,0.0)
define(__nozzle_exit_zcoord,0.0)

define(__n_parts_ref, 300000000000)                                //define amount of computational particles in the chamber
define(__delay, 5000)                                           //delay between start of simulation to the point where the flow is more or less stationary
define(__interval, 100)                                         //definition of interval between "measurements"

{
    "description": [
    "EXPANSION FLOW ALONG X AXIS",                              // general description of Experiment with some data
    "DODO: SHIFT TO ORIGIN",
    "TIMESTEP BY CFL",
    "REF SPEED 491.3 MS-1",
    "MEAN COLL TIME ???????s",
    "MFP 0.002666 M"
    ],
    
    "simulation" : {
        "n timesteps"   : 10000, // 15k averaging steps          //Number of timesteps for the the simulation
        "info interval" : 10                                    //Number of timespteps between giving out information
    },
    
    //"checkpoint" : {
    //  "interval"  : 5000,                                     //
    //  "first"     : 5000                                      //
    //},
    
    "time step control": {                                      // Time Step Settings
        "type"      : "fixed",                                  // What type of Time Step (variable or fixed)
        "dt"        : 1.27e-8,                                  //[s] based on    - Size of timestep   
        "scaling"   : [                                         // behält den wert bei
            [0, 1.0     ]
        ]
    },
    
    "algorithm": {
        "type"  : "FP-DSMC",                                    //What kind of algorithm
        "FOKKER-PLANCK"     : {"type"   : "FOKKER-PLANCK"},
        "DSMC"              : {"type"   : "DSMC-MF"}
        },


define(__lx, calc( __xmax - __xmin ))                           //define length of the edge of the box
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

    "simulation box": {
        "origin": [ __xmin, __ymin, __zmin ],                   //Set origin position
        "extent": [ __lx, __ly, __lz ]                          //define the extension of the box starting from the origin
    },


    "gas model": {                                              //define the gas parameters
        "type"      : "VSS",                                    // model for the gas generation and property vss=variable soft sphere
        "species"   : "argon",                                  //
        "diatomic"  : false,                                    //
        "m"         : 6.63e-26,     //[kg]                      //mass
        "d_ref"     : 4.17e-10,     //[m]                       //reference diameter of one molecule
        "alpha"     : 1.0,                                      //
        "omega"     : 0.81,                                     //
        "T_ref"     : 273.0,        //[K]                       // reference Temperature
        "gamma"     : 1.67          //[-] 1 + 2/f, f=3 (f: DoF)
    },  

        
"grid": {                                                       //Grid parameters
    
       "topology" : {                                           //
           "dimension" : 3,
           "discretization" : {                                 // what kind of discretization (Uniform)
               "type"  : "uniform",
               "ni"    : 22,                                    // 25 nodes in the i-dimension
               "nj"    : 22,
               "nk"    : 22,
               "level" : 5                                      // (2^dimension)^level
           },      
           "ordering" : "morton",                               //How the grid is ordered -> in what order the grids are gone through 
           "coordinate system" : {                              //Coordinate System
               "type" : "cartesian"
           }
       },
    
       "particle grid" : {                                      // How the real discretization of the computational domains
                "name"  : "default",
                "level" : 5                        
        },     
       
       "data grid" : [                                          // how the data is handled and given to the output, definition of data grids:
       
            {
                "name"          : "sim",                        // Naming the data grid "sim"    
                "is simulation grid" : true,                    //?
                "output"        : {                             //Implementation of interval, in that the data is "measured" and given out 
                    "interval" : __interval
                },
                "averaging"     : {                             //if and in what kind the data is averaged at the output
                    "type"  : "none"                
                }
            },
            
            {
                "name"          : "sample",                     // Naming the data grid sample
                // "particle grid" : "default",
                "output"        : {                             //At what time the data is given out
                    "interval"  : __interval,
                    "first"     : __delay
                },
                "averaging" : {                                 //IF and in what kind the data is averaged at the output
                    "type"  : "delayed",
                    "delay" : __delay,                          //with delay
                    "window size" : 50                         //?
                }
            }
            
        ]
        ,"grid adaption" : {                                    //Definition of grid adaptions
            //"refine intersected cells" : [                      //Grid adaption if the cells are intersected at the boundary
              //  {"boundaries" : ["inlet_disk"],                 //Boundary Conditions
              //  "level"        : 0}                             //?
            //],
            "refine cells inside" : [
                {"boundaries" : ["sphere"],
                "level"         : 0}
            ],


            
            "dynamic" : {                                       //Dynamic adaption
               "criterion"    : "mfp",                          //At what situation the adaption takes place
                "grid"        : "sample",                       //what data grid type is used
                "threshold"   : 4.0,                            //für das mfp (mean free path), wenn mfp < 1/2*grid size
                "first"       : 25,                             // First dynamic adaption
                "last"        : 3000,                           // Last dynamic adaption before it should be stationary
                "interval"    : 50,                             // Interval beteween adaption
               "level"       : 1
            }
        },
        "load balancing" : {                                    //for performance
            "grid" : "sample",
            "first"     : 50,
            "interval"  : 25,
            "last"      : __delay,
            "threshold" : 0.98
        }
    
    },
        
    //CONDITIONS
    
    define(__Tin, 223.22)                                       //Parameters on how the surrounding conditions are, here T_in
    define(__Tw, 298)                                           // t wall
    define(__n, 1.19e26)                                        //number density
    define(__Ma, 1)
    
    "condition": [
    { 
    "name" : "reference",                                       //Definition of the reference condition
      "n" : __n,                                                // [m-3] number density
      "number of particles" : __n_parts_ref                     //Number of computational particles existent in the condition
      },
        {                                                       // Definition of different flow cases
            "name"  : "streamCond",                             // Condition for normal stream
            "type"  : "stream",
            "n"     : __n,      //[m-3]
            "T"     : __Tin,    //[K]
            "Ma"    : __Ma
        },
        {
            "name"  : "initCond",                               // Initial Condition
            "type"  : "vacuum"
        },
        {
            "name"  : "specularCond",                           // Specular?? (spiegelnd?) Condition
            "type"  : "specular"
        },
        {
            "name"  : "vacuumCond",                             // How the vacuum is characerized
            "type"  : "vacuum"
        },
        {
            "name"  : "surfaceCond",                            //How the surface of the box is
            "type"  : "diffusive"
        },
        {
            "name"  : "voidCond",                               //If there is no condition
            "type"  : "void"
        }
    ],

    "state": [                                                  // The state condition
        {
            "name"  : "surfaceState",                           //how the surface is characterized
            "T"     : __Tw
        }
    ],
        
    "initialization" : {                                        // The condition one can use at the beginning of the simulation
        "condition" : "initCond"
    },
        
    "boundary": [                                               // Boundary conditions


            //"sample"    : {
            //    "averaging" : {
            //        "type"  : "delayed",
            //        "delay" : __delay               
            //    }
            //},

           // "output"    : {
            //    "first"     : __delay,
            //    "interval"  : __interval
            //}
            
//
// STANDARD INLETS & OUTLETS                                    // Definition of the box "walls"/faces
        {
            "name"      : "right",                              //defining the right wall
            "condition" : "vacuumCond",                         //what condition is used
            "geometry"  : {                                     //how the geometry of this Inlet/Outlet looks like, in this case a parallelogram 
                "type"     : "parallelogram",
                "box_face" : "right"
            }
        },
        {
            "name"      : "left",                               //same as above
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "left"
            }
        },
        define(__sphere_offset_frac, 0.1 )                                  //Verfeinerungs-Sphäre am Ausgang der Nozzle
        define(__sphere_rad, calc( __nozzle_radius / ( 2.0*__sphere_offset_frac - __sphere_offset_frac*__sphere_offset_frac ) + 0.0001  ))
        
        {   "name"      : "sphere",
            "geometry"  : {
                "type"      : "sphere",
                "center"    : [ calc( (1.0 - __sphere_offset_frac) * __sphere_rad + __nozzle_exit_xcoord), __nozzle_exit_ycoord, __nozzle_exit_zcoord ],
                "radius"    : __sphere_rad,
                "internal"  : true
            }
        },
//      {
//          "name"      : "inlet",
//          "condition" : "streamCond",
//          "geometry"  : {
//              "type"     : "parallelogram",
//              "box_face" : "left"
//          }
//      },
        {
            "name"      : "inlet_disk",                         // the small inlet at which the flow is initialized
            "condition" : {
                "front": "streamCond",                          //von vorne in richtung des normalenvektors, bedingung
                "back": "voidCond"
            },
            "geometry"  : {                                     //defining the geometry of the inlet disk
                "type"      : "disk",
                "center"    : [ __nozzle_exit_xcoord, __nozzle_exit_ycoord, __nozzle_exit_zcoord ],
                "normal"    : [1.0, 0.0, 0.0],
                "radius"    : __nozzle_radius
            }
        },      
        {
            "name"      : "top",                                //as above
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "top"
            }
        },  
        {
            "name"      : "bottom",                             // as above
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "bottom"
            }
        },  
        {
            "name"      : "front",                              // as above
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "front"
            }
        },      
        {
            "name"      : "back",                               // as above
            "condition" : "vacuumCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "back"
            }
        }       
    ]
}
    

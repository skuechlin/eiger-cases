changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__kB, 1.3806488e-23)

define(__unitperm, 1000.0) //json requires double to not end on .
define(__xmin, calc(-0.625	/ __unitperm))
define(__xmax, calc(7.5	/ __unitperm))
define(__ymin, 0)
define(__ymax, calc(2.0 / __unitperm))
define(__zmin, 0)
define(__zmax, calc(0.3 / __unitperm))
define(__rin,  calc(0.5 / __unitperm))

define(__xprobe, 0.003)

define(__infile_wall, "/scratch/kustepha/MicroNozzle_Wall.stl")
define(__infile_out, "/scratch/kustepha/MicroNozzle_Outlet.stl")
define(__outdir, "/scratch/kustepha/NOZZLE_OUT_VOLATILE/")
define(__tol,1e-6)

define(__particlespercell, 100)
define(__delay, 9999)
define(__interval,100)

{
	
	"simulation": {
		"n timesteps"	: 2000,
		"info interval" : 1
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 5.0e-9	//[s]
		//,"scaling" 	: [
		//	[0,		100.0	],
		//	[500,	1.0		]
		//]
	},
	
	"algorithm": {
		"type"	: "FOKKER-PLANCK"
	},
	
	"movement": {
		"dimension" : "3D"
	},

	"settings copy": {
		"directory" : __outdir
	},

define(__lx, calc( abs(__xmax) + abs(__xmin) ))
define(__ly, calc( abs(__ymax) + abs(__ymin) ))
define(__lz, calc( abs(__zmax) + abs(__zmin) ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},

define(__m, 46.5e-27)


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: __m,			//[kg]
		"d_ref" 	: 4.11e-10, 	//[m]
		"alpha" 	: 1.36,
		"omega" 	: 0.74,
		"T_ref" 	: 273.15,		//[K]
		"mu_ref" 	: 1.656e-5,		//[N s m-2]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3340.0		//[K]
	},	

		
	"mpi": {
		"nx": 200, // only decompose in x
		"ny": 1,
		"nz": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"directory" : __outdir,
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"rx"		: 1,
			"ry"		: 50,
			"rz"		: 10
		},
		{
			"name" 		: "sample",
			"output"	: {
				"directory" : __outdir,
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
				},
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		

	
	define(__T, 300)
	define(__p, 10e3)
		
	"condition": [
		{
			"name"	: "inletCond",
			"type"	: "stream",
			"p"		: __p, 		//[Pa]
			"T"		: __T,		//[K]
			"Ma"	: 0,
			"gamma" : 1.4,
			"yaw"	: 0.0,		//[deg]
			"pitch" : 0.0, 		//[deg]
			"roll"	: 0.0,		//[deg]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "surfaceCond",
			"type"	: "maxwellian",
			"alpha"	: 0.8,
			"T"		: __T
		},
		{
			"name"	: "outletCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "symmetryCond",
			"type"	: "symmetry"
		},
		{	
			"name"	: "probeCond",
			"type"	: "void"
		}	
	],
	
	"initialization" : {
		"condition"	: "inletCond"
	},
		
	"boundary": [
		{
			"name"		: "nozzle",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "stl",
				"file"	: __infile_wall,
				"per meter" : __unitperm,
				"tol"	: __tol
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"directory" : __outdir,
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"type"		: "default",
			"condition"	: "outletCond",
			"geometry"	: {
				"type"	: "stl",
				"file"	: __infile_out,
				"per meter" : __unitperm,
				"tol"	: __tol
			}
		},
		{
			"name"		: "inlet",
			"type"		: "inlet",
			"condition"	: "inletCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[__xmin, __ymin, __zmin],
					[__xmin, __rin,  __zmin],
					[__xmin, __ymin, __zmax]
				]
			}
		},		
		{
			"name"		: "symmetry",
			"type"		: "default",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		}//,
		//{
			//"name"		: "probe",
			//"type"		: "probe",
			//"condition"	: "probeCond",
			//"geometry"	: {
			//	"type"	: "parallelogram",
			//	"vertices" : [
			//		[__xprobe, __ymin, __zmin],
			//		[__xprobe, __ymin, __zmax],
			//		[__xprobe, __ymax, __zmin]
			//	]
			//},
			//"sample"	: {
			//	"averaging"	: {
			//		"type" 	: "delayed",
			//		"delay"	: __delay				
			//	}
			//},
			//"output"	: {
			//	"directory" : __outdir,
			//	"first"     : __delay,
			//	"interval"  : __interval
			//}
		//}
		
	]
}
	

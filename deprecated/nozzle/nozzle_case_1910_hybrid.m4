changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__unitperm, 1000.0) //json requires double to not end on .
define(__xmin, 0)
//define(__xmax, calc(5.038	/ __unitperm))
define(__xmax, calc(7.5 / __unitperm))
define(__ymin, 0)
define(__ymax, calc(2.0 / __unitperm))
define(__zmin, 0)
define(__zmax, calc(0.15 / __unitperm))
define(__rin,  calc(0.15 / __unitperm))
define(__h, calc(0.3 / __unitperm))
define(__zshift, calc(-0.5*__h))


define(__infile_wall, "MicroNozzle_Wall.stl")
define(__infile_out, "MicroNozzle_Outlet.stl")
define(__tol,1e-6)

define(__particlespercell, 50)
define(__delay, 80000)
define(__interval,100)

{
	
	//"restart" : {
	//	"file" : "/cluster/scratch/kustepha/NOZZLE_1300_OUT/prts_at_time_000000000.0002000050.prts"
	//},
	
	
	
	"description": [
	"CASE FROM ALEXEENKO ET. AL",	 
	"SIMULATE FROM THROAT INCLUDING OUTFLOW REGION",
	"USE CORRECTED CRITICAL PRESSURE 5.2828 kPa",
	"COARSER SIMULATION TO MATCH ALEXEENKO ET AL",
	"LESS PARTICLES THAN 1900",
	"USE Z SYMMETRY"
	],
	
	"simulation" : {
		"n timesteps"	: 100001, // 20k averaging steps
		"info interval" : 1
	},
	
	"checkpoint" : {
		"interval"  : 5000
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.0e-9,	//[s] based on cfl 0.25 500m/s (max av velocity) cell crossing time
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
		"Kn_crit" : 2.0
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: 46.5e-27,		//[kg]
		"mu_ref" 	: 1.656e-5, 	//[N s m-2]
		"alpha" 	: 1.36,
		"omega" 	: 0.74,
		"T_ref" 	: 273.0,			//[K]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3340.0		//[K]
	},	

		
	"mpi": {
		"nx": 900,
		"ny": 1, // dont decompose y
		"nz": 1  // dont decompose z
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 240,
			"rz"		: 18
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	//CRITICAL CONDITIONS
	
	define(__Tin, 250)
	define(__Tw, 300)
	define(__p, 5.2828e3)
	
	"condition": [
		{
			"name"	: "inletCond",
			"type"	: "stream",
			"p"		: __p, 		//[Pa]
			"T"		: __Tin,	//[K]
			"Ma"	: 1.0,
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: __Tw
		},
		{
			"name"	: "outletCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "symmetryCond",
			"type"	: "symmetry"
		},
		{	
			"name"	: "probeCond",
			"type"	: "void"
		}	
	],
	
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "nozzle",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"	: "stl",
				"file"	: __infile_wall,
				"per meter" : __unitperm,
				"translate" : [0.0, 0.0, __zshift],
				"tol"	: __tol
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "outletCond",
			"geometry"	: {
				"type"	: "stl",
				"file"	: __infile_out,
				"per meter" : __unitperm,
				"translate" : [0.0, 0.0, __zshift],
				"tol"	: __tol
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "inletCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[__xmin, __ymin, __zmin],
					[__xmin, __rin,  __zmin],
					[__xmin, __ymin, __zmax]
				]
			}
		},		
		{
			"name"		: "symmetry",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},
		{
			"name"		: "symmetry",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		}
		
	]
}
	

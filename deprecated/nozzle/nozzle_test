{
	"description": [
	"CASE FROM ALEXEENKO ET. AL",
	"SIMULATE FROM THROAT INCLUDING OUTFLOW REGION",
	"USE TOTAL PRESSURE 10 kPa",
	"PROBABLY FINER SIMULATION THAN ALEXEENKO ET AL",
	"RATHER LARGE SIM"
	],
	"simulation" : {
		"n timesteps"	: 110001,
		"info interval" : 1
	},
	"checkpoint" : {
		"interval"  : 2000
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.0e-9,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
		"Kn_crit" : 2.0
	},
	"simulation box": {
		"origin": [ -0.001, 0, 0 ],
		"extent": [ 0.0085, 0.002, 0.0003 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: 46.5e-27,
		"mu_ref" 	: 1.656e-5,
		"alpha" 	: 1.36,
		"omega" 	: 0.74,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
		"Zrot_inf"	: 18,
		"Trot_ref"	: 91.5,
		"Cvib_1"	: 9.1,
		"Cvib_2"	: 220.0,
		"Tvib_ref"	: 3340.0
	},
	"mpi": {
		"nx": 1,
		"ny": 1,
		"nz": 6
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 100
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : false,
			"rx"		: 170,
			"ry"		: 40,
			"rz"		: 1
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 100,
				"first"	    : 80000
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 80000
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "inletCond",
			"type"	: "stream",
			"p"		: 10.0e3,
			"T"		: 300,
			"Ma"	: 0.0,
			"particles per cell" : 400
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "outletCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "symmetryCond",
			"type"	: "symmetry"
		},
		{
			"name"	: "probeCond",
			"type"	: "void"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 300
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "nozzle",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"	: "fe_collection",
				"file"	: {
					"format"		: "stl",
					"name"  		: "MicroNozzle_LargeInlet_Wall.stl",
					"per meter" 	: 1000.0,
					"flip normals" 	: true,
					"tol"			: 1e-6
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: 80000
				}
			},
			"output"	: {
				"first"     : 80000,
				"interval"  : 100
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "outletCond",
			"geometry"	: {
				"type"	: "fe_collection",
				"file"	: {
					"format"		: "stl",
					"name"  		: "MicroNozzle_LargeInlet_Outlet.stl",
					"per meter" 	: 1000.0,
					"flip normals" 	: true,
					"tol"			: 1e-6
				}
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "inletCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[-0.001, 0, 0],
					[-0.001, 0.0015,  0],
					[-0.001, 0, 0.0003]
				]
			}
		},
		{
			"name"		: "symmetry",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		}
	]
}

changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__unitperm, 1000.0) //json requires double to not end on .
define(__xmin, calc(-1.0/ __unitperm))
define(__xmax, calc(7.5 / __unitperm))
define(__ymin, 0)
define(__ymax, calc(2.0 / __unitperm))
define(__zmin, 0)
define(__zmax, calc(0.3 / __unitperm))
define(__rin,  calc(1.5 / __unitperm))
define(__h, calc(0.3 / __unitperm))


define(__infile_wall, "MicroNozzle_LargeInlet_Wall.stl")
define(__infile_out, "MicroNozzle_LargeInlet_Outlet.stl")
define(__tol,1e-6)

define(__particlespercell, 400)
define(__delay, 80000)
define(__interval,100)

{
	
	//"restart" : {
	//	"file" : "/cluster/scratch/kustepha/NOZZLE_1300_OUT/prts_at_time_000000000.0002000050.prts"
	//},
	
	
	
	"description": [
	"CASE FROM ALEXEENKO ET. AL",	 
	"SIMULATE FROM THROAT INCLUDING OUTFLOW REGION",
	"USE TOTAL PRESSURE 10 kPa",
	"PROBABLY FINER SIMULATION THAN ALEXEENKO ET AL",
	"RATHER LARGE SIM"
	],
	
	"simulation" : {
		"n timesteps"	: 110001, // 30k averaging steps
		"info interval" : 1
	},
	
	"checkpoint" : {
		"interval"  : 2000
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.0e-9,	//[s] based on cfl 0.25 500m/s (max av velocity) cell crossing time
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
		"Kn_crit" : 2.0
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: 46.5e-27,		//[kg]
		"mu_ref" 	: 1.656e-5, 	//[N s m-2]
		"alpha" 	: 1.36,
		"omega" 	: 0.74,
		"T_ref" 	: 273.0,			//[K]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3340.0		//[K]
	},	

		
	"mpi": {
		"nx": 1, // dont decompose z
		"ny": 1, // dont decompose y
		"nz": 60  
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1700,
			"ry"		: 400,
			"rz"		: 1
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	//STAGNATION CONDITIONS
	
	define(__Tin, 300)
	define(__Tw, 300)
	define(__p, 10.0e3)
	
	"condition": [
		{
			"name"	: "inletCond",
			"type"	: "stream",
			"p"		: __p, 		//[Pa]
			"T"		: __Tin,	//[K]
			"Ma"	: 0.0,
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "outletCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "symmetryCond",
			"type"	: "symmetry"
		},
		{	
			"name"	: "probeCond",
			"type"	: "void"
		}	
	],

	
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: __Tw
		}
	],
		
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "nozzle",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"	: "fe_collection",
				"file"	: {
					"format"		: "stl",
					"name"  		: __infile_wall,
					"per meter" 	: __unitperm,
					"flip normals" 	: true,
					"tol"			: __tol
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "outletCond",
			"geometry"	: {
				"type"	: "fe_collection",
				"file"	: {
					"format"		: "stl",
					"name"  		: __infile_out,
					"per meter" 	: __unitperm,
					"flip normals" 	: false,
					"tol"			: __tol
				}
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "inletCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[__xmin, __ymin, __zmin],
					[__xmin, __rin,  __zmin],
					[__xmin, __ymin, __zmax]
				]
			}
		},		
		{
			"name"		: "symmetry",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		}		
	]
}
	

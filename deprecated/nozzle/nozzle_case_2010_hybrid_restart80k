{
	"restart" : {
		"file" : "/cluster/scratch/kustepha/NOZZLE_2011_HYBRID_OUT/prts_at_time_000000000.0001600040.prts"
	},
	"description": [
	"CASE FROM ALEXEENKO ET. AL",
	"SIMULATE FROM THROAT INCLUDING OUTFLOW REGION",
	"USE CORRECTED CRITICAL PRESSURE 5.2828 kPa",
	"PROBABLY FINER SIMULATION THAN ALEXEENKO ET AL",
	"FINER THAN 1900, PPC AS IN 1900",
	"USE Z SYMMETRY"
	],
	"simulation" : {
		"n timesteps"	: 20001,
		"info interval" : 1
	},
	"checkpoint" : {
		"interval"  : 10000
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.0e-9,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
		"Kn_crit" : 2.0
	},
	"simulation box": {
		"origin": [ 0, 0, 0 ],
		"extent": [ 0.0075, 0.002, 0.00015 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: 46.5e-27,
		"mu_ref" 	: 1.656e-5,
		"alpha" 	: 1.36,
		"omega" 	: 0.74,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
		"Zrot_inf"	: 18,
		"Trot_ref"	: 91.5,
		"Cvib_1"	: 9.1,
		"Cvib_2"	: 220.0,
		"Tvib_ref"	: 3340.0
	},
	"mpi": {
		"nx": 1050,
		"ny": 1,
		"nz": 1
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 100
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 280,
			"rz"		: 21
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 100,
				"first"	    : 0
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 0
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "inletCond",
			"type"	: "stream",
			"p"		: 5.2828e3,
			"T"		: 250,
			"Ma"	: 1.0,
			"particles per cell" : 200
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "outletCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "symmetryCond",
			"type"	: "symmetry"
		},
		{
			"name"	: "probeCond",
			"type"	: "void"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 300
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "nozzle",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"	: "stl",
				"file"	: "MicroNozzle_Wall.stl",
				"per meter" : 1000.0,
				"translate" : [0.0, 0.0, -0.00015],
				"tol"	: 1e-6
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: 0
				}
			},
			"output"	: {
				"first"     : 0,
				"interval"  : 100
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "outletCond",
			"geometry"	: {
				"type"	: "stl",
				"file"	: "MicroNozzle_Outlet.stl",
				"per meter" : 1000.0,
				"translate" : [0.0, 0.0, -0.00015],
				"tol"	: 1e-6
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "inletCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[0, 0, 0],
					[0, 0.00015,  0],
					[0, 0, 0.00015]
				]
			}
		},
		{
			"name"		: "symmetry",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},
		{
			"name"		: "symmetry",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		}
	]
}

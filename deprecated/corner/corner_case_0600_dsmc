{
	"description": [
	"SUPERSONIC CORNER FLOW",
	"BIRD, 16.2",
	"ALSO: J.-S. Wu AND Y.-Y. Lian",
	"Parallel three-dimensional direct simulation Monte Carlo method and its applications",
	"USE EVEN HIGHER RESOLUTION",
    "DX,DY,DZ ALL .5*.5 OF ORIGINAL CASE",
    "NUMBER DENSITY 10x HIGHER",
    "DT BY CFL"
	],
	"simulation" : {
		"n timesteps"	: 15001,
		"info interval" : 1
	},
	"checkpoint" : {
		"interval"  : 2500,
		"first"	    : 2500
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 8.0e-7,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "DSMC-MF"
	},
	"simulation box": {
		"origin": [ 0.0, 0.0, 0.0 ],
		"extent": [ 0.3, 0.18, 0.18 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"mpi": {
		"nx": 120,
		"ny": 1,
		"nz": 1
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 100
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 72,
			"rz"		: 72
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 100,
				"first"	    : 5000
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 5000
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1.0e21,
			"T"		: 300.0,
			"Ma"	: 6.0,
			"particles per cell" : 100
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: 1000.0
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "corner",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "nastran",
				"file"	: "SupersonicCornerFineFineFine.nas",
				"tol"	: 1e-6,
				"flip normals" : true
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: 5000
				}
			},
			"output"	: {
				"first"     : 5000,
				"interval"  : 100
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		},
		{
			"name"		: "frontBeforeWall",
			"condition" : "specularCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[0.0,  0.0, 0.0],
					[0.0,  0.0, 0.18],
					[0.05, 0.0, 0.0]
				]
			}
		},
		{
			"name"		: "bottomBeforeWall",
			"condition" : "specularCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[0.0,  0.0, 0.0],
					[0.05, 0.0, 0.0],
					[0.0,  0.18, 0.0]
				]
			}
		}
	]
}

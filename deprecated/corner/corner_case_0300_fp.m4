changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, 0.0)
define(__xmax, 0.3)
define(__ymin, 0.0)
define(__ymax, 0.18)
define(__zmin, 0.0)
define(__zmax, 0.18)
define(__xgeom, 0.05)


define(__infile_wall, "SupersonicCornerFine.nas")
define(__tol,1e-6)

define(__particlespercell, 100)
define(__delay, 5000)
define(__interval,100)

{
	"description": [
	"SUPERSONIC CORNER FLOW",	 
	"BIRD, 16.2",
	"ALSO: J.-S. Wu AND Y.-Y. Lian",
	"Parallel three-dimensional direct simulation Monte Carlo method and its applications",
	"USE HIGHER RESOLUTION"
	],
	
	"simulation" : {
		"n timesteps"	: 15001, // 10k averaging steps
		"info interval" : 1
	},
	
//	"checkpoint" : {
//		"interval"  : 3000,
//		"first"	    : 3000
//	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 1.0e-6,	//[s] 
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "FOKKER-PLANCK"
	},


define(__lx, calc( abs(__xmax) + abs(__xmin) ))
define(__ly, calc( abs(__ymax) + abs(__ymin) ))
define(__lz, calc( abs(__zmax) + abs(__zmin) ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,		//[kg]
		"d_ref" 	: 4.17e-10, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"mpi": {
		"nx": 60,
		"ny": 1, 
		"nz": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 36,
			"rz"		: 36
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	//CRITICAL CONDITIONS
	
	define(__Tin, 300)
	define(__Tw, 1000)
	define(__n, 1.0e20)
	define(__Ma, 6)
	
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: __Ma,		//[-]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "corner",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "nastran",
				"file"	: __infile_wall,
				"tol"	: __tol,
				"flip normals" : true
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		},	
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		},	
		{
			"name"		: "frontBeforeWall",
			"condition" : "specularCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[__xmin,  __ymin, __zmin],
					[__xmin,  __ymin, __zmax],
					[__xgeom, __ymin, __zmin]
				]
			}
		},
		{
			"name"		: "bottomBeforeWall",
			"condition" : "specularCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[__xmin,  __ymin, __zmin],
					[__xgeom, __ymin, __zmin],
					[__xmin,  __ymax, __zmin]
				]
			}
		}
		
	]
}
	

{
		"restart" : {
		"file" : "/scratch/kustepha/PROBE_OUT/20150804_Probe_0400_hybrid/prts_at_time_000000000.0007501000.prts"
	},
	"description": [
	"HYPERSONIC FLOW OVER A PLANETARY PROBE",
	"TIMESTEP BY CFL",
	"REF SPEED 1633 MS-1",
	"MFP 0.06e-3 M"
	],
	"simulation" : {
		"n timesteps"	: 20001,
		"info interval" : 1
	},
	"checkpoint" : {
		"interval"  : 5000,
		"first"	    : 5000
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 1.0e-7,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
		"Kn_crit": 3.0
	},
	"simulation box": {
		"origin": [ -0.02, -0.05, -0.05 ],
		"extent": [ 0.12, 0.1, 0.1 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: 6.63e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
		"Zrot_inf"	: 18.1,
		"Trot_ref"	: 91.5,
		"Cvib_1"	: 9.1,
		"Cvib_2"	: 220.0,
		"Tvib_ref"	: 3340.0
	},
	"mpi": {
		"type": "axisymmetric",
		"nx": 300,
		"nr": 1
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 100
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"rr"		: 250
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 100,
				"first"	    : 0
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 0
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"rr"		: 1
		}
	],
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"rho"	: 46.6e-5,
			"T"		: 15.3,
			"U"		: 1633,
			"particles per cell" : 2
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: 300
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "probe",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "stl",
				"file"	: "PlanetaryProbe50k.stl",
				"tol"	: 1e-6,
				"flip normals" : true
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: 0
				}
			},
			"output"	: {
				"first"     : 0,
				"interval"  : 100
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "disk",
				"center": [0.1,0.0,0.0],
				"normal": [-1.0,0.0,0.0],
				"radius": 0.05
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "disk",
				"center": [-0.02,0.0,0.0],
				"normal": [1.0,0.0,0.0],
				"radius": 0.05
			}
		},
		{
			"name"		: "upper",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "cylinder",
				"base"	: [-0.02,0.0,0.0],
				"direction" : [1.0,0.0,0.0],
				"radius" : 0.05,
				"length" : 0.12,
				"internal" : true
			}
		}
	]
}

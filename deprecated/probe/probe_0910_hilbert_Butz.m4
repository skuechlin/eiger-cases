changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__delay, 6000)

{
	"description": [
	"HYPERSONIC FLOW OF A PLANETARY PROBE",
	"D     = 0.2 M",
	"N     = 6.6639264e+19 M-3",
	"MFP   = 0.02 M",
	"KN_D  = 0.03",
	"NU    = 19942.772 S-1",
	"1./NU = 5.0143479e-05 S",
	"DX    = MFP"
	]
	,"simulation" : {
		"n timesteps"	: 9001,
		"info interval" : 1
	},
	"time step control": {
		"type"	: "fixed",
		"dt"		: 0.625e-7,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "FP-DSMC",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},
	"simulation box": {
		"origin": [ -0.04, -0.07, -0.07 ],
		"extent": [ 0.14, 0.14, 0.14 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"		: 4.65e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
		"Zrot_inf"	: 18.1,
    "Trot_ref"	: 91.5,
    "Cvib_1"	: 9.1,
    "Cvib_2"	: 220.0,
    "Tvib_ref"	: 3340.0
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 32,
	           "nj"    : 32,
	           "nk"    : 32,
	           "level" : 5
	       },
	       "ordering" : "hilbert Butz",//"morton",//
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 5
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 1000,
               "last" : __delay
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
                "first"	   : __delay,
                "interval" : 1000
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay"  : __delay,
    				"window size" : 10
    			}
    		}
    	]
    	, "grid adaption" : {
    	    "cell volume correction" : {
    	       	"correct cell volumes" 	: true,
    	     	  "remove empty cells"	: true,
    	      	"boundaries"			: "PlanetaryProbe"
    	    }
    		,	"refine intersected cells" : [ {"boundaries" : "PlanetaryProbe","level" : 0} ]
			  , "dynamic" : {
				  "criterion" : "mfp",
				  "threshold" : 0.6,
				  "grid"		  : "sample",
				  "first" 	  : 200,
				  "last" 	  : __delay,
				  "interval"  : 200
			}
    	}
    	, "load balancing" : {
    		"grid" 		  : "sample",
    		"first" 	  : 100,
    		"interval"  : 100,
    		"last"		  : __delay,
    		"threshold"	: 0.98
    	}
	},
	"condition": [
    {
      "name"  : "reference",
      "n"     : 3.72e20,
      "number of particles" : 400e6
    },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 3.72e20,
			"T"		: 13.3,
			"Ma"	: 20.2
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 300
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "PlanetaryProbe",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {
					"format"	: "stl",
					"name"		: "planetary_probe_v1_D1_a_join_meter.stl",
					"tol"		: 1e-6,
					"flip normals" : false
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay
				}
			},
			"output"	: {
				"first"     : __delay,
            "interval"  : 250
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, -0.02)
define(__xmax, 0.1)
define(__ymin, -0.05)
define(__ymax, 0.05)
define(__zmin, -0.05)
define(__zmax, 0.05)


define(__infile_wall, "PlanetaryProbe50k.stl")
define(__tol,1e-6)

define(__particlespercell, 4)
define(__delay, 3000)
define(__interval,100)

{
	"description": [
	"HYPERSONIC FLOW OVER A PLANETARY PROBE",
	"TIMESTEP BY CFL",
	"REF SPEED 1633 MS-1",
	"MFP 0.06e-3 M"
	],
	
	"simulation" : {
		"n timesteps"	: 13001, // 10k averaging steps
		"info interval" : 1
	},
	
//	"checkpoint" : {
//		"interval"  : 3000,
//		"first"	    : 3000
//	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.0e-7,	//[s] based on .5 * (0.12m/150) / (1633m/s)
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		},
		"Kn_crit": 3.0
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VHS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: 6.63e-26,		//[kg]
		"d_ref" 	: 4.17e-10, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18.1,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3340.0		//[K]
	},	

		
	"mpi": {
		"type": "axisymmetric",
		"nx": 150,
		"nr": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"rr"		: 125
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : true,
			"rx"		: 1,
			"rr"		: 1
		}
	],
		
	//CONDITIONS
	
	define(__Tin, 15.3)
	define(__Tw, 300)
	define(__rho, 46.6e-5)
	define(__U, 1633)
	
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"rho"	: __rho, 	//[kg m-3]
			"T"		: __Tin,	//[K]
			"U"		: __U,		//[m s-1]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "probe",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "stl",
				"file"	: __infile_wall,
				"tol"	: __tol,
				"flip normals" : true
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "disk",
				"center": [__xmax,0.0,0.0],
				"normal": [-1.0,0.0,0.0],
				"radius": calc(__ly*0.5)
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "disk",
				"center": [__xmin,0.0,0.0],
				"normal": [1.0,0.0,0.0],
				"radius": calc(__ly*0.5)
			}
		},		
		{
			"name"		: "upper",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "cylinder",
				"base"	: [__xmin,0.0,0.0],
				"direction" : [1.0,0.0,0.0],
				"radius" : calc(__ly*0.5),
				"length" : __lx,
				"internal" : true
			}
		}
	]
}
	

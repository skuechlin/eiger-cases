{
	"description": [
	"SUPERSONIC FLOW AROUND METEOR",
	"TIMESTEP BY CFL",
	"REF SPEED 1938 MS-1",
	"MEAN COLL TIME 6.68E-6s",
	"MFP 0.002666 M",
	"TEST WITH LOW NUMBER OF TIMESTEPS"
	],
	"simulation" : {
		"n timesteps"	: 101,
		"info interval" : 1
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 1.0e-7,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},
	"simulation box": {
		"origin": [ -0.01, -0.01, -0.01 ],
		"extent": [ 0.02, 0.02, 0.02 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"mpi": {
		"nx": 50,
		"ny": 1,
		"nz": 1
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 10
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 50,
			"rz"		: 50
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 10,
				"first"	    : 0
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 0
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 5.0e20,
			"T"		: 300,
			"Ma"	: 6,
			"particles per cell" : 100
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 1000
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "meteor",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"	: "spheres",
				"file"	: "Meteor_17-Nov-2015_23-33-18 Porosity 89p97.txt",
				"tol"	: 1e-6,
				"per meter" : 1e3
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: 0
				}
			},
			"output"	: {
				"first"     : 0,
				"interval"  : 10
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		}
	]
}

changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, -0.01)
define(__xmax, 0.01)
define(__ymin, -0.01)
define(__ymax, 0.01)
define(__zmin, -0.01)
define(__zmax, 0.01)


define(__infile_wall, "Meteor_23-Nov-2015_21-56-35.txt")
define(__tol,1e-6)
define(__units_per_meter,1e3)

define(__particlespercell, 20)
define(__delay, 1000) // 10 domain crossing times 
define(__interval,100)

{
	"description": [
	"SUPERSONIC FLOW AROUND METEOR",
	"TIMESTEP BY CFL",
	"REF SPEED 24000 MS-1",
	"MEAN COLL TIME 6.68E-6s",
	"MFP 0.002666 M",
	"TEST WITH LOW NUMBER OF TIMESTEPS"
	],
	
	"simulation" : {
		"n timesteps"	: 6001, // 5000 averaging steps
		"info interval" : 1
	},
	
//	"checkpoint" : {
//		"interval"  : 3000,
//		"first"	    : 3000
//	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 4.1e-9,	//[s] based on .5 * (0.02m/100) / (24000m/s) = 4.1e-9, mean free time = 1.1548e-5s
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"		: 46.5e-27,		//[kg]
		"mu_ref" 	: 1.656e-5, 		//[N s m-2]
		"alpha" 	: 1.36,
		"omega" 	: 0.74,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3340.0		//[K]
	},	


		
	"mpi": {
		"nx": 100,
		"ny": 1, 
		"nz": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 100,
			"rz"		: 100
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	//CONDITIONS
	
	define(__Tin, 199)
	define(__Tw, 199)
	define(__n, 3.8e20)
	define(__Ma, 85)
	
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: __Ma,		//[-]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "meteor",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"	: "spheres",
				"file"	: __infile_wall,
				"tol"	: __tol,
				"per meter" : __units_per_meter
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "delayed",
					"delay"	: __delay				
				}
			},
			"output"	: {
				"first"     : __delay,
				"interval"  : __interval
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			}
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		},	
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},		
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		}		
	]
}
	

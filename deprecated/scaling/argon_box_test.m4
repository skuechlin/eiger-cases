changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__unitperm, 100.0) //json requires double to not end on .
define(__xmin, 0)
define(__xmax, calc(30.0 / __unitperm))
define(__ymin, 0)
define(__ymax, calc(10.0 / __unitperm))
define(__zmin, 0)
define(__zmax, calc(10.0 / __unitperm))


define(__particlespercell, 100) // expect 300k particles
define(__delay, 250) // test
define(__interval,500) //test

{
	
	
	"description": [
	"SCALING PERFORMANCE TEST SINGLE NODE",	 
	"ARGON FLOW THROUGH SPECULAR BOX",
	"CASE IN GAO and SCHWARTZENTRUBER (2010)",
	"MFP APPROX. 1.164 CM",
	"MCT APPROX. 3.114e-5 SECONDS"
	],
	
	"simulation" : {
		"n timesteps"	: 1001, // test
		"info interval" : 10
	},
	
	"checkpoint" : {
		"interval"  : 10000
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.5e-5,	//[s] based on cfl 0.5 200m/s (inflow velocity) cell crossing time (0.01m cell width)
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "DSMC-MF"
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,		//[kg]
		"d_ref" 	: 4.17e-10, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"mpi": {
		"nx": 30,
		"ny": 1, // dont decompose y
		"nz": 1  // dont decompose z
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 10,
			"rz"		: 10
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	
	define(__Tin, 50)
	define(__rho, 1.0e-5)
	
	"condition": [
		{
			"name"	: "inletCond",
			"type"	: "stream",
			"rho"	: __rho, 			//[kg m-3]
			"T"		: __Tin,			//[K]
			"U"		: [200.0,0.0,0.0], 	//[m/s]
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "outletCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "symmetryCond",
			"type"	: "symmetry"
		}
	],
	
		
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "inlet",
			"condition"	: "inletCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face" : "left"
			}
		},		
		{
			"name"		: "outlet",
			"condition" : "outletCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "front",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"	   : "parallelogram",
				"box_face" : "front"
			}
		},
				{
			"name"		: "back",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		},
				{
			"name"		: "top",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		}
		
	]
}
	

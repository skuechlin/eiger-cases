{
	"description": [
	"SCALING PERFORMANCE TEST SINGLE NODE",
	"ARGON FLOW THROUGH SPECULAR BOX",
	"CASE IN GAO and SCHWARTZENTRUBER (2010)",
	"MFP APPROX. 1.164 CM",
	"MCT APPROX. 3.114e-5 SECONDS"
	],
	"simulation" : {
		"n timesteps"	: 12001,
		"info interval" : 10
	},
	"checkpoint" : {
		"interval"  : 12000
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.5e-5,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "DSMC-MF"
	},
	"simulation box": {
		"origin": [ 0, 0, 0 ],
		"extent": [ 7.68, 0.1, 0.1 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"mpi": {
		"nx": 768,
		"ny": 1,
		"nz": 1
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 1000
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 10,
			"rz"		: 10
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 1000,
				"first"	    : 4000
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 4000
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "inletCond",
			"type"	: "stream",
			"rho"	: 1.0e-5,
			"T"		: 50,
			"U"		: [200.0,0.0,0.0],
			"particles per cell" : 100
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "outletCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "symmetryCond",
			"type"	: "symmetry"
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "inlet",
			"condition"	: "inletCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "outlet",
			"condition" : "outletCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "front",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"	   : "parallelogram",
				"box_face" : "front"
			}
		},
				{
			"name"		: "back",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		},
				{
			"name"		: "top",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition" : "symmetryCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		}
	]
}

{
	"description": [
	"COUETTE FLOW",
	"L     = 1 M",
	"N     = 1.4e20 M-3",
    "MU    = 2.117e-05 PAS",
	"MFP   = 0.00925253948571841  M (VariableSoftSphere)",
	"KN    = 0.00925253948571841",
	"DT    =  .25*DX/sqrt(theta) = 2.5e-06 S",
	"DX    = L/200 = 0.005 M"
	],
	"simulation" : {
		"n timesteps"	: 200001 ,
		"info interval" : 100
	},
	"time step control": {
        "dt"		: 2.5e-06
    },
	"algorithm": {
    "type"  : "FOKKER-PLANCK",
    "entropic model" : false,
    "conditional position integration" : false
	},
	"simulation box": {
		"origin": [ 0.0, 0.0, 0.0 ],
		"extent": [ 1.0, 1.0, 1.0 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"mu_ref" 	: 2.117e-5,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"grid": {
	   "topology" : {
	       "dimension" : 1,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 200,
	           "nj"    : 1,
	           "nk"    : 1,
	           "level" : 0
	       },
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 0
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 50000
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : 50000 ,
    				"first"	    : 100000
    			},
    			"averaging"	: {
    				"type"   : "switched",
    				"switch" : 100000,
                    "first"  : {"type" : "none"},
                    "second" : {"type" : "arithmetic", "skip" : 9}
                }
    		}
    	]
	},
	"condition": [
        {
            "name" : "reference",
            "n"    : 1.4e20,
            "T"    : 273.0,
            "number of particles" : 200000
        },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1.4e20,
			"T"		: 273.0,
			"Ma"	: 0.0
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: 1.0
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: 1.0
		},
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: 1.0
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],
	"state": [
		{
			"name"		: "leftWallState",
			"velocity"	: [0.0, -500, 0.0],
			"T"			: 273.0
		}
		,{
			"name"		: "rightWallState",
			"velocity"	: [0.0, 500, 0.0],
			"T"			: 273.0
		}
	],
	"initialization" : {
		"condition"	: "streamCond"
	},
	"boundary": [
		{
			"name"		: "right",
			"condition"	: "surfaceCond",
			"state"		: "rightWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "surfaceCond",
			"state"		: "leftWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

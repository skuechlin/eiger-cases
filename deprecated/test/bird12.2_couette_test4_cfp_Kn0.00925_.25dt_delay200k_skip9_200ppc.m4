changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__pi, 3.1415926535897932) 
define(__kB, 1.38064852e-23) 

define(__Tw, 273.0)
define(__Tin,__Tw)
define(__Uw, 1000.0)
define(__m, 6.63e-26)
define(__alpha, 1.0)
define(__omega, 0.81)
define(__mur,2.117e-5) 
define(__dr,4.16844E-10)
define(__Tr,273.0)
define(__mu, calc( __mur*(__Tw/__Tr)**__omega ))

define(__n, 1.4e20)
define(__rho, calc(__m*__n))

define(__lambda, calc( (__mu/__rho)*sqrt(__m/(2.*__pi*__kB*__Tw))*4.*__alpha*(5. - 2.*__omega)*(7. - 2.*__omega)/(5.*(__alpha+1.)*(__alpha+2.)) ))

define(__ni,200)
define(__nj,1)
define(__nk,1)

define(__lx,1.0)
define(__dx,calc(__lx/__ni)) 
define(__dt,calc(.25e-5))

define(__Kn,calc(__lambda/__lx))

define(__xmin, 0.0)
define(__xmax, __lx)

define(__ymin, 0.0)
define(__ymax, __xmax)
define(__ly, 1.0)

define(__zmin, 0.0)
define(__zmax, __xmax)
define(__lz, 1.0)

define(__particlespercell, 200)
define(__delay, 200000)
define(__nts, calc(__delay*3+1) )
define(__interval, calc(__delay/2) )



{
	"description": [
	"COUETTE FLOW",
	"L     = 1 M",
	"N     = __n M-3",
    "MU    = __mu PAS",
	"MFP   = __lambda  M (VariableSoftSphere)",
    "KN    = __Kn",
	"DT    =  .25*DX/sqrt(theta) = __dt S",
	"DX    = L/200 = __dx M"
	],
	
	"simulation" : {
		"n timesteps"	: __nts,
		"info interval" : 100
	}, 
		
	"time step control": {
        "dt"		: __dt
    },
	
	"algorithm": {
    //"type" : "DSMC-MF"
    "type"  : "FOKKER-PLANCK",
    "entropic model" : false,
    "conditional position integration" : false 
	//	"type"	: "HYBRID",
	//	"DSMC"	: {
	//		"type" : "DSMC-MF"
	//	},
	//	"FOKKER-PLANCK" : {
	//		"type" : "FOKKER-PLANCK"
	//	}
	}, 

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: __m,		//[kg]
		"d_ref" 	: __dr, 	//[m]
		"alpha" 	: __alpha,
		"omega" 	: __omega,
		"T_ref" 	: __Tr,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"grid": {
	
	   "topology" : {	   
	       "dimension" : 1,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : __ni,
	           "nj"    : 1,
	           "nk"    : 1,
	           "level" : 0 
	       },	   
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 0 
    	},	   
	   
	   "data grid" : [ 
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : __interval
    			},
    			"averaging"	    : {
    				"type" 	: "none"				
    			}
    		}, 
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : __interval,//calc(__delay/25),
    				"first"	    : __delay
    			},
    			"averaging"	: {
    				"type"   : "switched",
    				"switch" : __delay,
                    "first"  : {"type" : "none"},
                    "second" : {"type" : "arithmetic", "skip" : 9}
                }
    		}
    		
    	]

    	//, "grid adaption" : {
    	//	"dynamic" : {
    	//		"criterion"	: "gradient",
    	//		//"criterion"	: "mfp",
    	//		"grid"		: "sample",
    	//		"variable"	: "n",
    	//		"reference" : __n,
    	//		"threshold" : 0.5,
    	//		"first"		: calc(__delay/5),
    	//		"last"		: calc(__delay-1),
    	//		"interval"	: calc(__delay/10)
    	//	}
    	//}

    	//,"load balancing" : {
    	//	"grid" 		: "sample",
    	//	"first" 	: 25,
    	//	"interval" 	: 100,
    	//	"last"		: __delay,
    	//	"threshold"	: 0.98
    	//}
	
	},
		
	//CONDITIONS

	
	"condition": [
        {
            "name" : "reference",
            "n"    : __n,
            "T"    : __Tin,
            "number of particles" : calc(__particlespercell*__ni)
        },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: 0.0		//[-]
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: __lx
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: __ly
		},		
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: __lz
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],

	"state": [
		{
			"name"		: "leftWallState",
			"velocity"	: [0.0, calc(-__Uw/2.), 0.0],
			"T"			: __Tw
		}
		,{
			"name"		: "rightWallState",
			"velocity"	: [0.0, calc(__Uw/2.), 0.0],
			"T"			: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "streamCond"
	},
		
	"boundary": [		
		{
			"name"		: "right",
			"condition"	: "surfaceCond",
			"state"		: "rightWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "surfaceCond",
			"state"		: "leftWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},	
		{
			"name"		: "front",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},		
		{
			"name"		: "back",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}		
	]
}
	

{
	"description": [
	"COUETTE FLOW",
	"L     = 0.95 M",
	"N     = 1e20 M-3",
	"MFP   = 0.0171  M (HardSphere)",
	"KN    = 0.018",
	"DT    = .5*DX/sqrt(R*T) = 9.96e-6",
	"DX    = L/200"
	],
	"simulation" : {
		"n timesteps"	: 25001,
		"info interval" : 1
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 1.0e-5,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
	"type"  : "DSMC"
	},
	"simulation box": {
		"origin": [ 0.0, 0.0, 0.0 ],
		"extent": [ 0.95, 0.0475, 0.0475 ]
	},
	"gas model": {
		"type" 		: "HS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"d_ref" 	: 3.628e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.5,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 200,
	           "nj"    : 10,
	           "nk"    : 10,
	           "level" : 4
	       },
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 100
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : 200,
    				"first"	    : 5000
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay" : 5000,
    				"window size" : 5000
    			}
    		}
    	]
    	, "grid adaption" : {
    		"dynamic" : {
    			"criterion"	: "gradient",
    			"grid"		: "sample",
    			"variable"	: "n",
    			"reference" : 1e20,
    			"threshold" : 0.5,
    			"first"		: 1000,
    			"last"		: 4999,
    			"interval"	: 500
    		}
    	}
	},
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1e20,
			"T"		: 273.0,
			"Ma"	: 0.0,
			"particles per cell" : 50
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: 0.95
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: 0.0475
		},
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: 0.0475
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],
	"state": [
		{
			"name"		: "leftWallState",
			"velocity"	: [0.0, -150.0, 0.0],
			"T"			: 273.0
		}
		,{
			"name"		: "rightWallState",
			"velocity"	: [0.0, 150.0, 0.0],
			"T"			: 273.0
		}
	],
	"initialization" : {
		"condition"	: "streamCond"
	},
	"boundary": [
		{
			"name"		: "right",
			"condition"	: "surfaceCond",
			"state"		: "rightWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "surfaceCond",
			"state"		: "leftWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

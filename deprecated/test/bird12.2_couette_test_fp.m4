changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])



	define(__n, 1.4e20)
	define(__dt,1e-5)
	define(__ni,200)
	define(__nj,10)
	define(__nk,10)

define(__xmin, 0.0)
define(__xmax, 1.0)
define(__lx, calc( __xmax - __xmin ))

define(__ymin, 0.0)
define(__ymax, calc(__nj*__lx / __ni))
define(__ly, calc( __ymax - __ymin ))

define(__zmin, 0.0)
define(__zmax, calc(__nk*__lx / __ni))
define(__lz, calc( __zmax - __zmin ))

define(__particlespercell, 50)
define(__delay, 5000)
define(__nts,25001)
define(__interval,1000)



{
	"description": [
	"COUETTE FLOW",
	"L     = 1.0 M",
	"N     = 1.4e20 M-3",
	"MFP   = 0.00925  M (VariableHardSphere)",
	"KN    = 0.00925",
	"DT    = .5*DX/sqrt(R*T) = 1e-5",
	"DX    = L/200"
	],
	
	"simulation" : {
		"n timesteps"	: __nts,
		"info interval" : 1
	},
	
	//"checkpoint" : {
	//	"interval"  : 5000,
	//	"first"	    : 5000
	//},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: __dt,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
	"type"  : "FOKKER-PLANCK"
	//	"type"	: "HYBRID",
	//	"DSMC"	: {
	//		"type" : "DSMC-MF"
	//	},
	//	"FOKKER-PLANCK" : {
	//		"type" : "FOKKER-PLANCK"
	//	}
	},




	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,		//[kg]
		"mu_ref" 	: 2.117e-5, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"grid": {
	
	   "topology" : {	   
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : __ni,
	           "nj"    : __nj,
	           "nk"    : __nk,
	           "level" : 4
	       },	   
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},	   
	   
	   "data grid" : [
	   
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : __interval
    			},
    			"averaging"	    : {
    				"type" 	: "none"				
    			}
    		},
    		
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : calc(__delay/25),
    				"first"	    : __delay
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay" : __delay,
    				"window size" : __delay
    			}
    		}
    		
    	]

    	, "grid adaption" : {
    		"dynamic" : {
    			"criterion"	: "gradient",
    			//"criterion"	: "mfp",
    			"grid"		: "sample",
    			"variable"	: "n",
    			"reference" : __n,
    			"threshold" : 0.5,
    			"first"		: calc(__delay/5),
    			"last"		: calc(__delay-1),
    			"interval"	: calc(__delay/10)
    		}
    	}

    	//,"load balancing" : {
    	//	"grid" 		: "sample",
    	//	"first" 	: 25,
    	//	"interval" 	: 100,
    	//	"last"		: __delay,
    	//	"threshold"	: 0.98
    	//}
	
	},
		
	//CONDITIONS
	
	define(__Tin, 273.0)
	define(__Tw, 273.0)
	
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: 0.0,		//[-]
			"particles per cell" : __particlespercell
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: __lx
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: __ly
		},		
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: __lz
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],

	"state": [
		{
			"name"		: "leftWallState",
			"velocity"	: [0.0, -500.0, 0.0],
			"T"			: __Tw
		}
		,{
			"name"		: "rightWallState",
			"velocity"	: [0.0, 500.0, 0.0],
			"T"			: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "streamCond"
	},
		
	"boundary": [		
		{
			"name"		: "right",
			"condition"	: "surfaceCond",
			"state"		: "rightWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "surfaceCond",
			"state"		: "leftWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},	
		{
			"name"		: "front",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},		
		{
			"name"		: "back",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}		
	]
}
	

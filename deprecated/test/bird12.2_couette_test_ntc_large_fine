{
	"description": [
	"COUETTE FLOW",
	"L     = 1 M",
	"N     = 1.4e20 M-3",
	"MFP   = 0.00925 M",
	"KN_D  = 0.00925",
	"NU    = 41'153 S-1",
	"1./NU = 2.43e-5 S",
	"DX    = MFP"
	],
	"simulation" : {
		"n timesteps"	: 50001,
		"info interval" : 1
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.0e-5,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
	"type"  : "DSMC"
	},
	"simulation box": {
		"origin": [ 0.0, 0.0, 0.0 ],
		"extent": [ 1, 0.05, 0.05 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 200,
	           "nj"    : 10,
	           "nk"    : 10,
	           "level" : 4
	       },
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 100
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : 1000,
    				"first"	    : 25000
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay" : 25000,
    				"window size" : 1000
    			}
    		}
    	]
    	, "grid adaption" : {
    		"dynamic" : {
    			"criterion"	: "gradient",
    			"grid"		: "sample",
    			"variable"	: "n",
    			"reference" : 1.4e20,
    			"threshold" : 0.5,
    			"first"		: 5000,
    			"last"		: 24999,
    			"interval"	: 2500
    		}
    	}
	},
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1.4e20,
			"T"		: 273,
			"Ma"	: 0.0,
			"particles per cell" : 100
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: 1
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: 0.05
		},
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: 0.05
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],
	"state": [
		{
			"name"		: "leftWallState",
			"velocity"	: [0.0, -500.0, 0.0],
			"T"			: 273
		}
		,{
			"name"		: "rightWallState",
			"velocity"	: [0.0, 500.0, 0.0],
			"T"			: 273
		}
	],
	"initialization" : {
		"condition"	: "streamCond"
	},
	"boundary": [
		{
			"name"		: "right",
			"condition"	: "surfaceCond",
			"state"		: "rightWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "surfaceCond",
			"state"		: "leftWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

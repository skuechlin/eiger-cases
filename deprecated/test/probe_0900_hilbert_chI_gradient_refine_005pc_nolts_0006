{
	"description": [
	"HYPERSONIC FLOW OF A PLANETARY PROBE",
	"D     = 0.2 M",
	"N     = 6.6639264e+19 M-3",
	"MFP   = 0.02 M",
	"KN_D  = 0.03",
	"NU    = 19942.772 S-1",
	"1./NU = 5.0143479e-05 S",
	"DX    = MFP",
    "U_ref = 1564 MS-1"
	]
	,"simulation" : {
		"n timesteps"	: 5501,
		"info interval" : 10
	},
	"time step control": {
        "dt"		: 1.0e-7
    },
	"algorithm": {
		"type"	: "FP-DSMC",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},
	"simulation box": {
		"origin": [ -0.06, -0.08, -0.08 ],
		"extent": [ 0.16, 0.16, 0.16 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"		: 4.65e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
		"Zrot_inf"	: 18.1,
    "Trot_ref"	: 91.5,
    "Cvib_1"	: 9.1,
    "Cvib_2"	: 220.0,
    "Tvib_ref"	: 3340.0
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 32,
	           "nj"    : 32,
	           "nk"    : 32,
	           "level" : 4
	       },
	       "ordering" : "hilbert chI",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"averaging"	    : { "type" 	: "none" },
                "output"        : {
                   "first"    : 500,
                   "interval" : 500
               }
    		},
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"time steps":[ 5500 ]
    			},
    			"averaging"	: {
    				"type" 	: "switched",
    				"switch" : 4500,
                    "first" : {
                        "type"   : "switched",
                        "switch" : 3000,
                        "first"  : { 
                            "type" : "switched",
                            "switch" : 1500,
                            "first" : {
                                "type" : "switched",
                                "switch" : 500,
                                "first" : {"type" : "exponential", "window size" : 500},
                                "second" : {"type" : "arithmetic"}
                            },
                            "second" : { "type" : "exponential", "window size" : 500 }
                        },
                       "second" : { "type" : "exponential", "window size" : 500 }
                    },
                    "second" : { "type" : "arithmetic" }
                }
            }
    	]
    	, "grid adaption" : {
    	    "cell volume correction" : {
    	       	"correct cell volumes" 	: true,
    	     	  "remove empty cells"	: true,
    	      	"boundaries"			: "PlanetaryProbe"
    	    }
            , "dynamic" : {
				  "criterion" : "gradient",
				  "threshold" : 0.05,
				  "grid"	  : "sample",
				  "time steps": [500,1500,3000]
			} 
        }
    	, "load balancing" : {
    		"grid" 		: "sim",
    		"first" 	: 50,
    		"interval"  : 50,
    		"last"		: 3500,
    		"threshold"	: 0.98
    	}
	},
	"condition": [
    {
      "name"  : "reference",
      "n"     : 3.72e20,
      "number of particles" : 800e6
    },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 3.72e20,
			"T"		: 13.3,
			"Ma"	: 20.2
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
    ],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 300
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
        {
            "name"      : "TimeZone",
            "is time zone boundary" : true,
            "output" : { "time steps" : [0,500,1500,3000] }
        }
        ,{
			"name"		: "PlanetaryProbe",
            "level"     : 0,
            "permanent" : true,
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {
					"format"	: "stl",
					"name"		: "planetary_probe_v1_D1_a_join_meter.stl",
					"tol"		: 1e-6,
					"flip normals" : false
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "switched",
					"switch" : 4500,
                    "first"  : {"type" : "none"},
                    "second" : {"type" : "arithmetic"}
				}
			},
			"output"	: {
				"time steps" : [5500]
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
            , "level" : 4
            , "permanent" : true
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
            , "level" : 4
            , "permanent" : true 
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
            , "level" : 4
            , "permanent" : true
		},
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
            , "level" : 4
            , "permanent" : true
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
            , "level" : 4
            , "permanent" : true
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
            , "level" : 4
            , "permanent" : true
        }        
	]
}

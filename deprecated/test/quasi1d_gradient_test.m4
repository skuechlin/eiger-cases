changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])




define(__xmin, 0.0)
define(__xmax, 0.25)
define(__ymin, 0.0)
define(__ymax, 0.25)
define(__zmin, 0.0)
define(__zmax, 1.0)

define(__particlespercell, 100)
define(__delay, 1024)
define(__nts,calc(2*__delay+1))
define(__interval,100)


	define(__n, 6.664e19)
	define(__dt,5.0e-5)
	define(__ni,16)
	define(__nj,__ni)
	define(__nk,calc(4*__ni))

{
	"description": [
	"COUETTE FLOW",
	"D     = 1 M",
	"N     = 6.6639264e+19 M-3",
	"MFP   = 0.02 M",
	"KN_D  = 0.02",
	"NU    = 19942.772 S-1",
	"1./NU = 5.0143479e-05 S",
	"DX    = MFP"
	],
	
	"simulation" : {
		"n timesteps"	: __nts,
		"info interval" : 1
	},
	
	//"checkpoint" : {
	//	"interval"  : 5000,
	//	"first"	    : 5000
	//},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: __dt,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "HYBRID",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},


define(__lx, calc( __xmax - __xmin ))
define(__ly, calc( __ymax - __ymin ))
define(__lz, calc( __zmax - __zmin ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,		//[kg]
		"d_ref" 	: 4.17e-10, 	//[m]
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.67			//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"grid": {
	
	   "topology" : {	   
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : __ni,
	           "nj"    : __nj,
	           "nk"    : __nk,
	           "level" : 4
	       },	   
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},	   
	   
	   "data grid" : [
	   
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : __interval
    			},
    			"averaging"	    : {
    				"type" 	: "none"				
    			}
    		},
    		
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : calc(__delay/16),
    				"first"	    : calc(__delay/4)
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay" : __delay,
    				"window size" : calc(__delay/64)
    			}
    		}
    		
    	]

    	, "grid adaption" : {
    		"dynamic" : {
    			"criterion"	: "gradient",
    			//"criterion"	: "mfp",
    			"grid"		: "sample",
    			"variable"	: "n",
    			"reference" : __n,
    			"threshold" : 0.5,
    			"first"		: calc(__delay/4),
    			"last"		: calc(__delay-1),
    			"interval"	: calc(__delay/64)
    		}
    	}

    	,"load balancing" : {
    		"grid" 		: "sample",
    		"first" 	: 25,
    		"interval" 	: 100,
    		"last"		: __delay,
    		"threshold"	: 0.98
    	}
	
	},
		
	//CONDITIONS
	
	define(__Tin, 300)
	define(__Tw, 300)
	
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: 0.0,		//[-]
			"particles per cell" : __particlespercell
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: __lx
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: __ly
		},		
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: __lz
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],

	"state": [
		{
			"name"		: "upperWallState",
			"velocity"	: [100.0, 0.0, 0.0],
			"T"			: __Tw
		}
		,{
			"name"		: "lowerWallState",
			"velocity"	: [-100.0, 0.0, 0.0],
			"T"			: __Tw
		}
	],
		
	"initialization" : {
		"condition"	: "streamCond"
	},
		
	"boundary": [		
		{
			"name"		: "right",
			"condition"	: "periodicXCond",
			//"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			//"condition"	: "streamCond",
			"condition"	: "periodicXCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},		
		{
			"name"		: "top",
			"condition"	: "surfaceCond",
			"state"		: "upperWallState",
		//	"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},	
		{
			"name"		: "bottom",
			"condition"	: "surfaceCond",
			"state"		: "lowerWallState",
		//	"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},	
		{
			"name"		: "front",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},		
		{
			"name"		: "back",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}		
	]
}
	

{
	"description": [
	"COUETTE FLOW",
	"L     = 100 MFP",
	"N     = 1.0e20 M-3",
	"MFP   = 0.0137301498938734  M (VariableSoftSphere)",
	"KN    = 0.01",
	"DT    =  .5*DX/Uw",
	"DX    = L/200"
	],
	"simulation" : {
		"n timesteps"	: 40001 ,
		"info interval" : 100
	},
	"time step control": {
        "dt"		: 3.43253747346835e-06
    },
	"algorithm": {
    "type"  : "FOKKER-PLANCK",
    "position integration" : true
	},
	"simulation box": {
		"origin": [ 0.0, 0.0, 0.0 ],
		"extent": [ 1.37301498938734, 1.37301498938734, 1.37301498938734 ]
	},
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"mu_ref" 	: 2.117e-5,
		"alpha" 	: 1.4,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 200,
	           "nj"    : 1,
	           "nk"    : 1,
	           "level" : 4
	       },
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 10000
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : 10000 ,
    				"first"	    : 20000
    			},
    			"averaging"	: {
    				"type"   : "switched",
    				"switch" : 20000,
                    "first"  : {"type" : "none"},
                    "second" : {"type" : "arithmetic"}
                }
    		}
    	]
	},
	"condition": [
        {
            "name" : "reference",
            "n"    : 1.0e20,
            "T"    : 300.0,
            "number of particles" : 200000
        },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1.0e20,
			"T"		: 300.0,
			"Ma"	: 0.0
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: 1.37301498938734
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: 1.37301498938734
		},
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: 1.37301498938734
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],
	"state": [
		{
			"name"		: "leftWallState",
			"velocity"	: [0.0, -500, 0.0],
			"T"			: 300.0
		}
		,{
			"name"		: "rightWallState",
			"velocity"	: [0.0, 500, 0.0],
			"T"			: 300.0
		}
	],
	"initialization" : {
		"condition"	: "streamCond"
	},
	"boundary": [
		{
			"name"		: "right",
			"condition"	: "surfaceCond",
			"state"		: "rightWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "surfaceCond",
			"state"		: "leftWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

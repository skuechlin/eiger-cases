{
	"description": [
	"COUETTE FLOW",
	"L     = 1.0 M",
	"N     = 1.4e20 M-3",
	"MFP   = 0.00925  M (VariableHardSphere)",
	"KN    = 0.00925",
	"DT    = 0.25 * .5*DX/sqrt(R*T) = 0.25e-5",
	"DX    = L/200"
	],
	"simulation" : {
		"n timesteps"	: 100001 ,
		"info interval" : 1
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 0.25e-5,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
	"type"  : "FOKKER-PLANCK"
	},
	"simulation box": {
		"origin": [ 0.0, 0.0, 0.0 ],
		"extent": [ 1, 1, 1 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.63e-26,
		"mu_ref" 	: 2.117e-5,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.67
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 200,
	           "nj"    : 1,
	           "nk"    : 1,
	           "level" : 4
	       },
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 25000
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : 25000 ,
    				"first"	    : 50000
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay" : 50000
    			}
    		}
    	]
	},
	"condition": [
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1.4e20,
			"T"		: 273.0,
			"Ma"	: 0.0,
			"particles per cell" : 1000
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: 1
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: 1
		},
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: 1
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],
	"state": [
		{
			"name"		: "leftWallState",
			"velocity"	: [0.0, -500.0, 0.0],
			"T"			: 273.0
		}
		,{
			"name"		: "rightWallState",
			"velocity"	: [0.0, 500.0, 0.0],
			"T"			: 273.0
		}
	],
	"initialization" : {
		"condition"	: "streamCond"
	},
	"boundary": [
		{
			"name"		: "right",
			"condition"	: "surfaceCond",
			"state"		: "rightWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "surfaceCond",
			"state"		: "leftWallState",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "top",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "specularCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		},
		{
			"name"		: "front",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "periodicYCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		}
	]
}

{
	"description": [
	"HYPERSONIC FLOW OF A PLANETARY PROBE",
	"D     = 0.2 M",
	"N     = 6.6639264e+19 M-3",
	"MFP   = 0.02 M",
	"KN_D  = 0.03",
	"NU    = 19942.772 S-1",
	"1./NU = 5.0143479e-05 S",
	"DX    = MFP",
    "U_ref = 1258 MS-1"
	]
	,"simulation" : {
		"n timesteps"	: 2501,
		"info interval" : 1
	},
	"time step control": {
        "dt"		: 5.44e-7
    },
	"algorithm": {
		"type"	: "FP-DSMC",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},
	"simulation box": {
		"origin": [ -0.05, -0.07, -0.07 ],
		"extent": [ 0.14, 0.14, 0.14 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"		: 4.65e-26,
		"d_ref" 	: 4.17e-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
		"Zrot_inf"	: 18.1,
    "Trot_ref"	: 91.5,
    "Cvib_1"	: 9.1,
    "Cvib_2"	: 220.0,
    "Tvib_ref"	: 3340.0
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 32,
	           "nj"    : 32,
	           "nk"    : 32,
	           "level" : 4
	       },
	       "ordering" : "hilbert chI",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 4
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 25,
    				"last" : 1250
    			},
    			"averaging"	    : { "type" 	: "none" }
    		},
    		{
    			"name" 		    : "sample",
    			"particle grid" : "default",
    			"output"	    : {
    				"interval"  : 100
    				,"first"    : 100
    			},
    			"averaging"	: {
    				"type" 	: "switched",
    				"switch" : 1250,
                    "first" : {
                        "type" : "switched",
                        "switch" : 100,
                        "first"  : { "type" : "none" },
                        "second" : { "type" : "exponential", "window size" : 100 }
                        },
                   "second" : { "type" : "arithmetic" }
    			}
    		}
    	]
    	, "grid adaption" : {
    	    "cell volume correction" : {
    	       	"correct cell volumes" 	: true,
    	     	  "remove empty cells"	: true,
    	      	"boundaries"			: "PlanetaryProbe"
    	    }
            , "dynamic" : {
				  "criterion" : "gradient",
				  "threshold" : 0.10,
				  "grid"	  : "sample",
				  "first" 	  : 500,
				  "last" 	  : 1000,
				  "interval"  : 250
			} 
        }
    	, "load balancing" : {
    		"grid" 		: "sim",
    		"first" 	: 25,
    		"interval"  : 25,
    		"last"		: 1250,
    		"threshold"	: 0.98
    	}
	},
	"condition": [
    {
      "name"  : "reference",
      "n"     : 3.72e20,
      "number of particles" : 400e6
    },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 3.72e20,
			"T"		: 13.3,
			"Ma"	: 20.2
		},
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 300
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "PlanetaryProbe",
            "level"     : 1,
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {
					"format"	: "stl",
					"name"		: "planetary_probe_v1_D1_a_join_meter.stl",
					"tol"		: 1e-6,
					"flip normals" : false
				}
			},
			"sample"	: {
				"averaging"	: {
					"type" 	: "switched",
					"switch" : 1250,
                    "first"  : {"type" : "none"},
                    "second" : {"type" : "arithmetic"}
				}
			},
			"output"	: {
				"first"     : 0,
				"interval"  : 50
			}
		},
		{
			"name"		: "outlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
            , "level" : 4
            , "permanent" : true
		},
		{
			"name"		: "inlet",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
            , "level" : 4
            , "permanent" : true 
		},
		{
			"name"		: "top",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
            , "level" : 4
            , "permanent" : true
		},
		{
			"name"		: "bottom",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
            , "level" : 4
            , "permanent" : true
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
            , "level" : 4
            , "permanent" : true
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
            , "level" : 4
            , "permanent" : true
        }        
	]
}

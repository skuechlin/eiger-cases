{
    "description": [
       "ARGON EXPANSION AND SKIMMER FLOW",
       "D     = 0.5e-3 M",
       "N     = 1.607e25 M-3",
       "MFP   = 3.047e-8 M",
       "KN_D  = 1.2045e-5 ",
       "NU    = 4530865338.69 S-1",
       "1./NU = 2.207e-10 S",
       "DX    = MFP"
    ]
    ,"simulation" : {
            "n timesteps"   : 6001,
            "info interval" : 1
    },
    "time step control": {
            "type"	  : "fixed",
            "dt"      : 1.0e-7,
            "scaling" 	: [
                    [0,	1.0		]
            ]
    },
    "algorithm": {
            "type"	: "FP-DSMC",
            "DSMC"	: {
                    "type" : "DSMC-MF"
            },
            "FOKKER-PLANCK" : {
                    "type" : "FOKKER-PLANCK"
            }
    },
    "simulation box": {
            "origin": [ -130.5e-3, -130.5e-3, -0.5e-3 ],
            "extent": [ 261.0e-3, 261.0e-3, 161.0e-3 ]
    },
    "gas model": {
        "type"      : "VSS",
            "species"   : "argon",
            "diatomic"  : false,
            "m"         : 6.63e-26,
            "d_ref"     : 4.17e-10,
            "alpha"     : 1.0,
            "omega"     : 0.81,
            "T_ref"     : 273.0,
            "gamma"     : 1.67
    },
    "grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
               "type"  : "uniform",
               "ni"    : 32,
               "nj"    : 32,
               "nk"    : 20,
               "level" : 7
           },
           "ordering" : "hilbert Butz",
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
            "name"  : "default",
            "level" : 6
       },
       "data grid" : [
       {
            "name"		         : "sim",
            "is simulation grid" : true,
            "output"	    : {
                "interval" : 100,
                "last" : 3250
            },
            "averaging"	    : {
                "type" 	: "none"
            }
        },
        {
            "name" 		    : "sample",
            "output"	    : {
                        "interval"  : 500
                        ,"first"	: 3000
                },
                "averaging"	: {
                        "type" 	: "delayed",
                        "delay"  : 3000,
                        "window size" : 100
                }
        }
        ],
         "grid adaption" : {
	    "cell volume correction" : {
	       	"correct cell volumes" 	: true,
	    	"remove empty cells"	: true
            }
		,
           "refine intersected cells" : [ {"boundaries" : ["nozzleskimmer","inlet_disk"],"level" : 0} ]
		  , "dynamic" : {
                	  "criterion" : "mfp",
			  "threshold" : 25.0,
			  "grid"      : "sample",
			  "first"     : 100,
			  "last"      : 3000,
			  "interval"  : 100
		}
        },
         "load balancing" : {
                "grid"      : "sample",
                "first"     : 25,
                "interval"  : 25,
                "last"      : 3000,
                "threshold" : 0.99
        }
        },
        "condition": [
        {
            "name"  : "reference",
            "n"     : 1.0e22,
            "number of particles" : 200.0e6
        },
        {
                "name"	: "inflow",
                "type"	: "stream",
                "n"	: 3.2122e23,
                "T"	: 219.48,
                "U"	: [ 0.0, 0.0, 276.246 ]
        },
        {
            "name"  : "backgroundone",
            "type"  : "stream",
            "p"     : 40.0,
            "T"     : 293.0,
            "Ma"    : 0.0
        },
        {
            "name"  : "backgroundtwo",
            "type"  : "stream",
            "p"     : 0.1,
            "T"     : 293.0,
            "Ma"    : 0.0
        },
        {
                "name"	: "initCond",
                "type"	: "vacuum"
        },
        {	"name" : "vacuumCond",
                "type" : "vacuum"
        },
        {
                "name"	: "specularCond",
                "type"	: "specular"
        },
        {
                "name"	: "surfaceCond",
                "type"	: "diffusive"
        }
        ],
        "state": [
                {
                        "name"	: "surfaceState",
                        "T"		: 293.0
                }
        ],
	"initialization" : {
		"condition"	: "backgroundone"
	},
        "boundary": [
        {
                "name"		: "wall",
                "condition"	: "surfaceCond",
                "state"		: "surfaceState",
                "geometry"	: {
                        "type"  : "fe_collection",
                        "file"	: {
                                "format"	: "stl",
                                "name"		: "ChamberWall_05.stl",
                                "per meter"     : 1.0,
                                "flip normals" : true
                        }
                }
        },
        {
                "name"		: "nozzleskimmer",
                "condition"	: "surfaceCond",
                "state"		: "surfaceState",
                "geometry"	: {
                        "type"  : "fe_collection",
                        "file"	: {
                                "format"	: "stl",
                                "name"		: "NozzleSkimmer_05.stl",
                                "per meter"     : 1.0,
                                "flip normals" : true
                        }
                }
        },
        {
                "name"		: "chamberone",
                "condition"	: "backgroundone",
                "geometry"   : {
                  "type"  : "fe_collection",
                  "file"  : {
                    "format"    : "stl",
                    "name"      : "ChamberOne_05.stl",
                    "per meter" : 1.0,
                    "flip normals" : true
                }
            }
                },
        {
                "name"		: "crossing",
                "condition"	: "backgroundtwo",
                "geometry"   : {
                  "type"  : "fe_collection",
                  "file"  : {
                    "format"    : "stl",
                    "name"      : "OutletDisk_05.stl",
                    "per meter" : 1.0,
                    "flip normals" : true
                }
            }
                },
        {
                "name"      : "inlet_disk",
                "condition" : {
                "front": "inflow"
            },
        "geometry"   : {
        "type"  : "fe_collection",
        "file"  : {
            "format"    : "stl",
            "name"      : "CriticalDisk_05.stl",
            "per meter" : 1.0,
            "flip normals" : true
        }
    }
        }
    ]
}

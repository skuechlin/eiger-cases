{
	"description": [
	   "ARGON EXPANSION AND SKIMMER FLOW",
	   "D     = 0.5e-3 M",
	   "N     = 1.607e25 M-3",
	   "MFP   = 3.047e-8 M",
	   "KN_D  = 1.2045e-5 ",
	   "NU    = 4530865338.69 S-1",
	   "1./NU = 2.207e-10 S",
	   "DX    = MFP"
	]
	,"simulation" : {
		"n timesteps"	: 2501,
		"info interval" : 1
	},
	"time step control": {
		"type"	  : "fixed",
		"dt"      : 1.0e-6,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "FP-DSMC",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},
	"simulation box": {
		"origin": [ -130.5e-3, -130.5e-3, -0.5e-3 ],
		"extent": [ 261.0e-3, 261.0e-3, 370.0e-3 ]
	},
    "gas model": {
        "type"      : "VSS",
        "species"   : "argon",
        "diatomic"  : false,
        "m"         : 6.63e-26,
        "d_ref"     : 4.17e-10,
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,
        "gamma"     : 1.67
    },
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 32,
	           "nj"    : 32,
	           "nk"    : 45,
	           "level" : 5
	       },
	       "ordering" : "hilbert Butz",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 3
    	},
	   "data grid" : [
    		{
    			"name"		         : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 50,
    				"last" : 1250
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : 500
    				,"first"	: 1000
    			},
    			"averaging"	: {
    				"type" 	: "delayed",
    				"delay"  : 1000,
    				"window size" : 50
    			}
    		}
    	]
    	, "grid adaption" : {
    	    "cell volume correction" : {
    	       	"correct cell volumes" 	: true,
    	    	"remove empty cells"	: true,
    	    	"boundaries"		: ["nozzleskimmer","wall","chamberone","chambertwo"]
    	    },
	"refine intersected cells" : [ {"boundaries" : ["nozzleskimmer","inlet_disk"],"level" : 0} ]
    	}
    	, "load balancing" : {
    		"grid" 		  : "sample",
    		"first" 	  : 25,
    		"interval"    : 25,
    		"last"		  : 1000,
    		"threshold"	  : 0.99
    	}
	},
	"condition": [
        {
            "name"  : "reference",
            "n"     : 3.72e20,
            "number of particles" : 200e6
        },
		{
			"name"	: "inflow",
			"type"	: "stream",
			"n"		: 1.607e25,
			"T"		: 219.48,
			"U"	: [ 0.0, 0.0, 276.11 ]
		},
		{
            "name"  : "backgroundone",
            "type"  : "stream",
            "p"     : 0.4e2,
            "T"     : 293.0,
            "Ma"    : 0.0
        },
        {
            "name"  : "backgroundtwo",
            "type"  : "stream",
            "p"     : 10.0e-2,
            "T"     : 293.0,
            "Ma"    : 0.0
        },
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{	"name" : "vacuumCond",
			"type" : "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: 293.0
		}
	],
	"boundary": [
        {
		"name"		: "wall",
		"condition"	: "surfaceCond",
		"state"		: "surfaceState",
		"geometry"	: {
			"type"  : "fe_collection",
			"file"	: {
				"format"	: "stl",
				"name"		: "ChamberWall_00.stl",
				"tol"		: 1e-7,
             			"per meter"     : 1.0,
				"flip normals" : true
			}
		}
        },
	{
		"name"		: "nozzleskimmer",
		"condition"	: "surfaceCond",
		"state"		: "surfaceState",
		"geometry"	: {
			"type"  : "fe_collection",
			"file"	: {
				"format"	: "stl",
				"name"		: "NozzleSkimmer_00.stl",
				"tol"		: 1e-7,
             			"per meter"     : 1.0,
				"flip normals" : true
			}
		}
	},
	{
		"name"		: "chamberone",
		"condition"	: "backgroundone",
		"geometry"   : {
               	  "type"  : "fe_collection",
                  "file"  : {
                    "format"    : "stl",
                    "name"      : "ChamberOne_00.stl",
                    "per meter" : 1.0,
                    "tol"       : 1e-7,
                    "flip normals" : true
                }
            }
		},
	    {
            "name"      : "chambertwo",
            "condition" : "backgroundtwo",
            "geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "ChamberTwo_00.stl",
		    "per meter" : 1.0,
                    "tol"       : 1e-7,
                    "flip normals" : true
                }
            }
        },
		{
            "name"      : "inlet_disk",
            "condition" : {
                "front": "inflow"
            },
            "geometry"  : {
                "type"      : "disk",
                "center"    : [ 0.0, 0.0, 0.0 ],
                "normal"    : [0.0, 0.0, 1.0],
                "radius"    : 3.1175e-3
            }
        }
	]
}

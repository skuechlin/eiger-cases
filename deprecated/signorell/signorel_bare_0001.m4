changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__delay, 1000)
define(__tw,293.0) // wall temperature

{
	"description": [
	   "ARGON EXPANSION AND SKIMMER FLOW",
	   "D     = 0.5e-3 M", // skimmer diameter
	   "N     = 1.607e25 M-3", // P=rho*R*T=n*k*T
	   "MFP   = 3.047e-8 M",
	   "KN_D  = 1.2045e-5 ", // lambda0/dcrit; lambda0=(2^0.5*Pi*dref^2*n*(Tref/T)^(w-0.5))^-1
	   "NU    = 4530865338.69 S-1", // collision frequency; 4*dref^2*n*(Pi*k*Tref/m)^(1/2)*(T/Tref)^(1-w)
	   "1./NU = 2.207e-10 S",
	   "DX    = MFP" // v*dt=v*0.5*(1/Nu)*1.0
	]
	,"simulation" : {
		"n timesteps"	: 2501, // guess: 5 domain crossing times
		"info interval" : 1
	},
	"time step control": {
		"type"	  : "fixed",
		"dt"      : 1.0e-6, // CFL < 0.5 at inflow on grid level 1
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "FP-DSMC",
		"DSMC"	: {
			"type" : "DSMC-MF"
		},
		"FOKKER-PLANCK" : {
			"type" : "FOKKER-PLANCK"
		}
	},
	"simulation box": { // bounding box of simulated volume
		"origin": [ -130.5e-3, -130.5e-3, -1.0e-3 ], // coordinates of lowest corner, values in m
		"extent": [ 261.0e-3, 261.0e-3, 370.0e-3 ] // edge length of box in m
	},
    "gas model": {
        "type"      : "VSS",
        "species"   : "argon",
        "diatomic"  : false,
        "m"         : 6.63e-26,     //[kg]
        "d_ref"     : 4.17e-10,     //[m]
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,        //[K]
        "gamma"     : 1.67          //[-] 1 + 2/f, f=3 (f: DoF)
    },  
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 32, // number of cells in x-dimension
	           "nj"    : 32, // y
	           "nk"    : 45, // z
	           "level" : 5   // background resolution: n(i,j,k) * 2^level
	       },
	       "ordering" : "hilbert Butz",//"morton",//
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 3
    	},
	   "data grid" : [
    		{
    			"name"		         : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 50, // time step interval to write output file
    				"last" : calc(__delay + 250) // last time step at which to write output
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : 500
    				,"first"	: __delay // first time step at which to start writing output files
    			},
    			"averaging"	: {
    				"type" 	: "delayed", // do exponentially weighted moving time average from timestep 0 to delay-1 with given window size, then arithmetic average
    				"delay"  : __delay, 
    				"window size" : 50
    			}
    		}
    	]
    	, "grid adaption" : {
    	    "cell volume correction" : {
    	       	"correct cell volumes" 	: true,
    	    	"remove empty cells"	: true,
    	    	"boundaries"		: ["nozzleskimmer","wall","chamberone","chambertwo"]
    	    },
    //		,
	"refine intersected cells" : [ {"boundaries" : ["nozzleskimmer","inlet_disk"],"level" : 0} ]
		//	  , "dynamic" : {
		//		  "criterion" : "mfp",
		//		  "threshold" : 2.0,
		//		  "grid"		  : "sample",
		//		  "first" 	  : 50,
		//		  "last" 	  : __delay,
		//		  "interval"  : 50
		//	}
    	}
    	, "load balancing" : {
    		"grid" 		  : "sample",
    		"first" 	  : 25,
    		"interval"    : 25,
    		"last"		  : __delay,
    		"threshold"	  : 0.99
    	}
	},
	"condition": [
        {
            "name"  : "reference", // at density n, aim for given number of particles
            "n"     : 3.72e20, // number density # M-3
            "number of particles" : 400e6
        },
		{
			"name"	: "inflow",
			"type"	: "stream",
			"n"		: 1.607e25, // # M-3
			"T"		: 219.48, // K
			"U"	: [ 0.0, 0.0, 276.11 ] // - in critical crosssection
		},
		{
            "name"  : "backgroundone",
            "type"  : "stream",
            "p"     : 0.4e2, // Pa
            "T"     : __tw, // K
            "Ma"    : 0.0 // -
        },
        {
            "name"  : "backgroundtwo",
            "type"  : "stream",
            "p"     : 10.0e-2, // Pa
            "T"     : __tw, // K
            "Ma"    : 0.0 // -
        },
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{	"name" : "vacuumCond",
			"type" : "vacuum"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		}
	],
	"state": [
		{
			"name"	: "surfaceState",
			"T"		: __tw
		}
	],
	"initialization" : { // initial population of simulation volume
		"condition"	: "backgroundtwo"
	},
	"boundary": [
        {
		"name"		: "wall",
		"condition"	: "surfaceCond",
		"state"		: "surfaceState",
		"geometry"	: {
			"type"  : "fe_collection",
			"file"	: {
				"format"	: "stl",
				"name"		: "ChamberWall.stl",
				"tol"		: 1e-6,
             			"per meter"     : 1.0,
				"flip normals" : false // normals should point into fluid volume
			}
		}
		//,"sample"	: {
		//	"averaging"	: {
		//		"type" 	: "delayed",
		//		"delay"	: __delay
		//	}
		//},
		//"output"	: {
		//	"first"     : 2500,
		//	"interval"  : 2500
		//}
        },
	{
		"name"		: "nozzleskimmer",
		"condition"	: "surfaceCond",
		"state"		: "surfaceState",
		"geometry"	: {
			"type"  : "fe_collection",
			"file"	: {
				"format"	: "stl",
				"name"		: "NozzleSkimmer.stl",
				"tol"		: 1e-6,
             			"per meter"     : 1.0,
				"flip normals" : false // normals should point into fluid volume
			}
		}
		//,"sample"	: {
		//	"averaging"	: {
		//		"type" 	: "delayed",
		//		"delay"	: __delay
		//	}
		//},
		//"output"	: {
		//	"first"     : 2500,
		//	"interval"  : 2500
		//}
	},
	//	{
	//		"name"		: "Skimmer", // wall
	//		"condition"	: "surfaceCond",
	//		"state"		: "surfaceState",
	//		"geometry"	: {
	//			"type"  : "fe_collection",
	//			"file"	: {
	//				"format"	: "stl",
	//				"name"		: "Skimmer_asci.stl",
	//				"tol"		: 1e-6,
        //             			"per meter"     : 1000.0,
	//				"flip normals" : false // normals should point into fluid volume
	//			}
	//		}
			//,"sample"	: {
			//	"averaging"	: {
			//		"type" 	: "delayed",
			//		"delay"	: __delay
			//	}
			//},
			//"output"	: {
			//	"first"     : 2500,
			//	"interval"  : 2500
			//}
	//	},
		{
			"name"		: "chamberone",
			"condition"	: "backgroundone",
			"geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "LeftChamber.stl",
                    "per meter" : 1.0,
                    "tol"       : 1e-6,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
		},
	    {
            "name"      : "chambertwo",
            "condition" : "backgroundtwo",
            "geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "RightChamber.stl",
		    "per meter" : 1.0,
                    "tol"       : 1e-6,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
        },
		{
            "name"      : "inlet_disk",
            "condition" : {
                "front": "inflow",
                "back": "vacuumCond"
            },
            "geometry"  : {
                "type"      : "disk",
                "center"    : [ 0.0, 0.0, 0.0 ],
                "normal"    : [0.0, 0.0, 1.0],
                "radius"    : 3.1e-3
            }
        }
//,     
//	{
//			"name"		: "left",
//			"condition"	: "vacuumCond",
//			"geometry"	: {
//				"type"     : "parallelogram",
//				"box_face" : "left"
//			}
//		},
//		{
//			"name"		: "right",
//			"condition"	: "vacuumCond",
//			"geometry"	: {
//				"type"     : "parallelogram",
//				"box_face" : "right"
//			}
//		},
//		{
//			"name"		: "top",
//			"condition"	: "vacuumCond",
//			"geometry"	: {
//				"type"     : "parallelogram",
//				"box_face" : "top"
//			}
//		},
//		{
//			"name"		: "bottom",
//			"condition"	: "vacuumCond",
//			"geometry"	: {
//				"type"     : "parallelogram",
//				"box_face" : "bottom"
//			}
//		},
//		{
//			"name"		: "front",
//			"condition"	: "vacuumCond",
//			"geometry"	: {
//				"type"     : "parallelogram",
//				"box_face" : "front"
//			}
//		},
//		{
//			"name"		: "back",
//			"condition"	: "vacuumCond",
//			"geometry"	: {
//				"type"     : "parallelogram",
//				"box_face" : "back"
//			}
//		}
 
	]
}

{
    "description": [
        "ARGON SKIMMER FLOW",
        "D     = 0.5e-3 M        (skimmer diameter)",
        "Stagnation conditions:",
        "       p0 = 3000 Pa",
        "       T0 = 293 K",
        "Nozze geometry:",
        "       r* = 3.118332092e-3 M",
        "       A* = 3.05488324e-5 M2",
        "       r_in = 5e-3 M",
        "       A_in = 7.854e-5 M2",
        " A_in/A* = 2.57095977",
        " Ma_in   = 4.0",
        " T_in    = 47.5 K",
        " p_in    = 40 Pa"
    ],
    "simulation": {
        "n timesteps": 18001,
        "info interval": 1
    },
    "time step control": {
        "type": "fixed",
        "dt": 1.5e-8,
        "scaling": [
            [
                0,
                1.0
            ]
        ]
    },
    "algorithm": {
        "type": "DSMC-MF"
    },
    "simulation box": {
        "origin": [
            0.0,
            0.0,
            -0.001
        ],
        "extent": [
            0.005,
            0.025,
            0.0106
        ]
    },
    "gas model": {
        "type": "VSS",
        "species": "argon",
        "diatomic": false,
        "m": 6.630000000000001e-26,
        "d_ref": 4.17e-10,
        "alpha": 1.0,
        "omega": 0.81,
        "T_ref": 273.0,
        "gamma": 1.67
    },
    "grid": {
        "topology": {
            "dimension": 3,
            "discretization": {
                "type": "uniform",
                "ni": 499,
                "nj": 1,
                "nk": 1060,
                "level": 7
            },
            "ordering": "hilbert ChI",
            "coordinate system": {
                "type": "cartesian"
            }
        },
        "particle grid": {
            "name": "default",
            "level": 7
        },
        "data grid": [
            {
                "name": "sim",
                "is simulation grid": true,
                "output": {
                    "interval": 650,
                    "last": 13650
                },
                "averaging": {
                    "type": "none"
                }
            },
            {
                "name": "sample",
                "output": {
                    "interval": 1000,
                    "first": 13000
                },
                "averaging": {
                    "type": "switched",
                    "switch": 13000,
                    "first" : { "type" : "exponential", "window size" : 20 },
                    "second" : { "type" : "arithmetic" }
                }
            }
        ],
        "grid adaption": {
            "cell volume correction": {
                "correct cell volumes": true,
                "remove empty cells": true,
                "boundaries": [
                    "Skimmer"
                ]
            }
        },
        "load balancing": {
            "grid": "sample",
            "first": 25,
            "interval": 25,
            "last": 13000,
            "threshold": 0.99
        }
    },
    "condition": [
        {
            "name": "reference",
            "p": 40.0,
            "T": 47.5,
            "number of particles": 30000000.0
        },
        {
            "name": "freestream",
            "type": "stream",
            "Ma": 4.0,
            "p": 40.0,
            "T": 47.5,
            "direction": [
                0.0,
                0.0,
                1.0
            ]
        },
        {
            "name": "outflow",
            "type": "stream",
            "p": 0.01,
            "T": 293.0,
            "Ma": 0.0
        },
        {
            "name": "initCond",
            "type": "vacuum"
        },
        {
            "name": "vacuumCond",
            "type": "vacuum"
        },
        {
            "name": "specularCond",
            "type": "specular"
        },
        {
            "name": "surfaceCond",
            "type": "diffusive"
        },
        {
            "name": "periodicCond",
            "type": "periodic",
            "distance": 0.025
        }
    ],
    "state": [
        {
            "name": "surfaceState",
            "T": 293.0
        }
    ],
    "initialization": {
        "condition": "vacuumCond"
    },
    "boundary": [
        {
            "name": "Skimmer",
            "condition": "surfaceCond",
            "state": "surfaceState",
            "geometry": {
                "type": "fe_collection",
                "file": {
                    "format": "stl",
                    "name": "07_05_skimmer.stl",
                    "per meter": 1.0,
                    "tol": 1e-9,
                    "flip normals": true
                }
            },
            "output": {
                "first": -1
            }
        },
        {
            "name": "TopOutflow",
            "condition": "outflow",
            "geometry": {
                "type": "fe_collection",
                "file": {
                    "format": "stl",
                    "name": "07_05_top_out.stl",
                    "per meter": 1.0,
                    "tol": 1e-9,
                    "flip normals": true
                }
            },
            "output": {
                "first": -1
            }
        },
        {
            "name": "TopFreestream",
            "condition": "freestream",
            "geometry": {
                "type": "fe_collection",
                "file": {
                    "format": "stl",
                    "name": "07_05_top_free.stl",
                    "per meter": 1.0,
                    "tol": 1e-9,
                    "flip normals": true
                }
            },
            "output": {
                "first": -1
            }
        },
        {
            "name": "Right",
            "condition": "freestream",
            "geometry": {
                "type": "fe_collection",
                "file": {
                    "format": "stl",
                    "name": "07_05_right.stl",
                    "per meter": 1.0,
                    "tol": 1e-9,
                    "flip normals": true
                }
            },
            "output": {
                "first": -1
            }
        },
        {
            "name": "Left",
            "condition": "specularCond",
            "geometry": {
                "type": "fe_collection",
                "file": {
                    "format": "stl",
                    "name": "07_05_left.stl",
                    "per meter": 1.0,
                    "tol": 1e-9,
                    "flip normals": true
                }
            },
            "output": {
                "first": -1
            }
        },
        {
            "name": "Front",
            "condition": "periodicCond",
            "geometry": {
                "type": "fe_collection",
                "file": {
                    "format": "stl",
                    "name": "07_05_front.stl",
                    "per meter": 1.0,
                    "tol": 1e-9,
                    "flip normals": true
                }
            },
            "output": {
                "first": -1
            }
        },
        {
            "name": "Back",
            "condition": "periodicCond",
            "geometry": {
                "type": "fe_collection",
                "file": {
                    "format": "stl",
                    "name": "07_05_back.stl",
                    "per meter": 1.0,
                    "tol": 1e-9,
                    "flip normals": true
                }
            },
            "output": {
                "first": -1
            }
        },
        {
            "name": "Bottom",
            "condition": "freestream",
            "geometry": {
                "type": "fe_collection",
                "file": {
                    "format": "stl",
                    "name": "07_05_bottom.stl",
                    "per meter": 1.0,
                    "tol": 1e-9,
                    "flip normals": true
                }
            },
            "output": {
                "first": -1
            }
        }
    ]
}

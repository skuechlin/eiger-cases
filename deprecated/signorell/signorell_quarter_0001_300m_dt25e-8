





{
    "description": [
        "ARGON EXPANSION AND SKIMMER FLOW",
        "D     = 0.5e-3 M        (skimmer diameter)",                     
        "Stagnation conditions:",
        "       p0 = 3000 Pa",
        "       T0 = 293 K",
        "Nozze geometry:",
        "       r* = 3.118332092e-3 M", 
        "       A* = 3.05488324e-5 M2",
        "       r_in = 5e-3 M",
        "       A_in = 7.854e-5 M2",
        " A_in/A* = 2.57095977",
        " Ma_in   = 0.22625195",
        " T_in    = 288.06016064 K",
        " p_in    = 2875.51284 Pa" 
    ]
    ,"simulation" : {
            "n timesteps"	: 24001,
            "info interval" : 1
    },
    "time step control": {
            "type" : "fixed",
            "dt"   : 25.0e-8,
            "scaling" 	: [ [0,	1.0] ]
    },
    "algorithm": {
            "type"	: "FP-DSMC",
            "DSMC"	: {
                    "type" : "DSMC-MF"
            },
            "FOKKER-PLANCK" : {
                    "type" : "FOKKER-PLANCK"
            }
    },
    "simulation box": {
            "origin": [ 0.0, 0.0, 0.0 ],
            "extent": [ 50.0e-3, 50.0e-3, 170.0e-3 ]
    },
    "gas model": {
        "type"      : "VSS",
        "species"   : "argon",
        "diatomic"  : false,
        "m"         : 6.63e-26,
        "d_ref"     : 4.17e-10,
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,
        "gamma"     : 1.67
    },  
    "grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
               "type"  : "uniform",
               "ni"    : 10,
               "nj"    : 10,
               "nk"    : 34,
               "level" : 7
           },
           "ordering" : "hilbert Butz",
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
            "name"  : "default",
            "level" : 6 
       },
       "data grid" : [
       {
            "name"		 : "sim",
            "is simulation grid" : true,
            "output"	    : {
                "interval" : 200,
                "last" : 12250
            },
            "averaging"	    : {
                "type" 	: "none"
            }
        },
        {
            "name" 	    : "sample",
            "output"	    : {
                        "interval"  : 500
                        ,"first"	: 12000
                },
                "averaging"	: {
                        "type" 	: "delayed",
                        "delay"  : 12000, 
                        "window size" : 50
                }
        }
        ],
         "grid adaption" : {
	    "cell volume correction" : {
	       	"correct cell volumes" 	: true,
	    	"remove empty cells"	: true,
                "boundaries"            : ["Wall","Channel","Skimmer","Nozzle","Background","Inflow","Outflow"]
            } 
           ,"refine intersected cells" : [ {"boundaries" : ["Skimmer","Nozzle"],"level" : 1} ]
           ,"refine cells inside"      : [ {"boundaries" : ["cylinder","ldisk","rdisk"],"level" : 2}, {"boundaries" : ["cylinder2","ldisk2","rdisk2"],"level" : 0} ]
        },
         "load balancing" : {
                "grid" 	   : "sample",
                "first"    : 25,
                "interval" : 25,
                "last"	   : 12000,
                "threshold" : 0.99
        }
        },
        "condition": [
        {
            "name"  : "reference",
            "n"     : 1.0e22,
            "number of particles" : 300.0e6 
        },
        {
            "name"	: "inflow",
            "type"	: "stream",
            "Ma"        : 0.22625195,
            "p"	        : 2875.51284,
            "T"	        : 288.06016064 ,
            "direction"	: [ 0.0, 0.0, 1.0 ] 
        },
        {
            "name"  : "backgroundone",
            "type"  : "stream",
            "p"     : 40.0,
            "T"     : 293.0,
            "Ma"    : 0.0
        },
        {
            "name"  : "backgroundtwo",
            "type"  : "stream",
            "p"     : 0.1,
            "T"     : 293.0,
            "Ma"    : 0.0
        },
        {
                "name"	: "initCond",
                "type"	: "vacuum"
        },
        {	"name" : "vacuumCond",
                "type" : "vacuum"
        },
        {
                "name"	: "specularCond",
                "type"	: "specular"
        },
        {
                "name"	: "surfaceCond",
                "type"	: "diffusive"
        }
        ],
        "state": [
                {
                        "name"	: "surfaceState",
                        "T"	: 293.0
                }
        ],
	"initialization" : {
		"condition"	: "backgroundone"
	},
        "boundary": [
        {
            "name"	: "Wall",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                    "type" : "fe_collection",
                    "file" : {
                            "format"       : "stl",
                            "name"	   : "Wall_Quarter_00.stl",
                            "per meter"    : 1.0,
                            "tol"          : 1.0e-9,
                            "flip normals" : true
                    }
            } 
            ,"output"   : {
                "first"     : -1
            }
        },
        {
            "name"	: "Nozzle",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                "type"  : "fe_collection",
                "file"	: {
                    "format"       : "stl",
                    "name"	   : "Nozzle_Quarter_00.stl",
                    "per meter"    : 1.0,
                    "tol"          : 1.0e-9,
                    "flip normals" : true
                }
            }
           ,"output"	: {
                "first" : -1
           }
        },
        {
            "name"	: "Skimmer",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                "type"  : "fe_collection",
                "file"	: {
                    "format"       : "stl",
                    "name"	   : "Skimmer_Quarter_00.stl",
                    "per meter"    : 1.0,
                    "tol"          : 1.0e-9,
                    "flip normals" : true
                }
            }
           ,"output"	: {
                "first" : -1
           }
        },
        {
            "name"      : "Background",
            "condition" : "backgroundone",
            "geometry"  : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Background_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : 1.0e-9,
                    "flip normals" : true
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
        {
            "name"       : "Symmetry",
            "condition"  : "specularCond",
            "geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Symmetry_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : 1.0e-9,
                    "flip normals" : true
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
	{
            "name"	: "Channel",
            "condition"	: "surfaceCond",
            "state"     : "surfaceState",
            "geometry"  : {
		"type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"	: "Channel_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : 1.0e-9,
                    "flip normals" : true
                }
            }
            ,"output" : {
                "first" : -1
            }
	},
        {
            "name"      : "Inflow",
            "condition" : {
                "front": "inflow"
            },
            "geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Inflow_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : 1.0e-9,
                    "flip normals" : true
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
        {
            "name"      : "Outflow",
            "condition" : "backgroundtwo",
            "geometry"  : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Outflow_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : 1.0e-9,
                    "flip normals" : true
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
        {
            "name"      : "cylinder",
            "geometry"  : {
            "type"      : "cylinder",
                "base"      : [0.0, 0.0, 0.0],
                "direction" : [0.0, 0.0, 1.0],
                "radius"    : 0.01,
                "length"    : 0.160,
                "internal"  : true
            }
        },  
        {
            "name"      : "ldisk",
            "geometry"  : {
                "type"      : "disk",
                "center"    : [0.0, 0.0, 0.0],
                "normal"    : [0.0, 0.0, 1.0],
                "radius"    : 0.01 
            }
        },
        {
            "name"      : "rdisk",
            "geometry"  : {      
                "type"      : "disk",
                "center"    : [0.0, 0.0, 0.160],
                "normal"    : [0.0, 0.0, -1.0],
                "radius"    : 0.01 
            }
        },
        {
            "name"      : "cylinder2",
            "geometry"  : {
            "type"      : "cylinder",
                "base"      : [0.0, 0.0, 0.1315],
                "direction" : [0.0, 0.0, 1.0],
                "radius"    : 0.001275,
                "length"    : 0.0285,
                "internal"  : true
            }
        }, 
        {
            "name"      : "ldisk2",
            "geometry"  : {
                "type"      : "disk",
                "center"    : [0.0, 0.0, 0.1315],
                "normal"    : [0.0, 0.0, 1.0],
                "radius"    : 0.001275 
            }
        },
        {
            "name"      : "rdisk2",
            "geometry"  : {      
                "type"      : "disk",
                "center"    : [0.0, 0.0, 0.160],
                "normal"    : [0.0, 0.0, -1.0],
                "radius"    : 0.001275 
            }
        } 
    ]
}

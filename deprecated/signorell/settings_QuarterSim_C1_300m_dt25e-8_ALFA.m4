changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__delay, 12000) // guess: 5 domain crossing times at u_crit = 270m/s
define(__tw,293.0) // wall temperature
define(__tol,1.0e-9) // geometry file import abolute tolerance [m]

{
    "description": [
       "ARGON EXPANSION AND SKIMMER FLOW",
       "D     = 0.5e-3 M", // skimmer diameter
       "N     = 1.607e25 M-3", // P=rho*R*T=n*k*T
       "MFP   = 3.047e-8 M",
       "KN_D  = 1.2045e-5 ", // lambda0/dcrit; lambda0=(2^0.5*Pi*dref^2*n*(Tref/T)^(w-0.5))^-1
       "NU    = 4530865338.69 S-1", // collision frequency; 4*dref^2*n*(Pi*k*Tref/m)^(1/2)*(T/Tref)^(1-w)
       "1./NU = 2.207e-10 S",
       "DX    = MFP" // v*dt=v*0.5*(1/Nu)*1.0
    ]
    ,"simulation" : {
            "n timesteps"	: 24001, // 2x delay
            "info interval" : 1
    },
    "time step control": {
            "type"	  : "fixed",
            "dt"      : 25.0e-8, //Neu: h_ref = 0.1935mm, u_ref = (1+1/sqrt(gamma))*sqrt(gamma*R*T) = 380m/s -> 0.5 h_ref/u_ref = 2.5e-7s  
            "scaling" 	: [
                    [0,	1.0		]
            ]
    },
    "algorithm": {
            "type"	: "FP-DSMC",
            "DSMC"	: {
                    "type" : "DSMC-MF"
            },
            "FOKKER-PLANCK" : {
                    "type" : "FOKKER-PLANCK"
            }
    },
    "simulation box": { // bounding box of simulated volume
            "origin": [ -50.5e-3, 0.0, -0.5e-3 ], // coordinates of lowest corner, values in m
            "extent": [ 50.5e-3, 50.5e-3, 161.0e-3 ] // edge length of box in m
    },
    "gas model": {
        "type"      : "VSS",
            "species"   : "argon",
            "diatomic"  : false,
            "m"         : 6.63e-26,     //[kg]
            "d_ref"     : 4.17e-10,     //[m]
            "alpha"     : 1.0,
            "omega"     : 0.81,
            "T_ref"     : 273.0,        //[K]
            "gamma"     : 1.67          //[-] 1 + 2/f, f=3 (f: DoF)
    },  
    "grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
               "type"  : "uniform",
               "ni"    : 8, // number of cells in x-dimension
               "nj"    : 8, // y
               "nk"    : 26, // z
               "level" : 5   // background resolution: n(i,j,k) * 2^level
           },
           "ordering" : "hilbert Alfa",//
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
            "name"  : "default",
            "level" : 4
       },
       "data grid" : [
       {
            "name"		         : "sim",
            "is simulation grid" : true,
            "output"	    : {
                "interval" : 200, // time step interval to write output file
                "last" : calc(__delay + 250) // last time step at which to write output
            },
            "averaging"	    : {
                "type" 	: "none"
            }
        },
        {
            "name" 		    : "sample",
            "output"	    : {
                        "interval"  : 500
                        ,"first"	: __delay // first time step at which to start writing output files
                },
                "averaging"	: {
                        "type" 	: "delayed", // do exponentially weighted moving time average from timestep 0 to delay-1 with given window size, then arithmetic average
                        "delay"  : __delay, 
                        "window size" : 50
                }
        }
        ],
         "grid adaption" : {
	    "cell volume correction" : {
	       	"correct cell volumes" 	: true,
	    	"remove empty cells"	: true
            }
		,
           "refine intersected cells" : [ {"boundaries" : ["Nozzle_Skimmer","CriticalDisk"],"level" : 1} ]
		  , "dynamic" : {
                	  "criterion" : "mfp",
			  "threshold" : 50.0,
                          "grid"      : "sample",
			  "first"     : 50,
			  "last"      : __delay,
			  "interval"  : 50
		}
        },
         "load balancing" : {
                "grid" 	   : "sample",
                "first"    : 25,
                "interval" : 25,
                "last"	   : __delay,
                "threshold" : 0.99
        }
        },
        "condition": [
        {
            "name"  : "reference", // at density n, aim for given number of particles
            "n"     : 1.0e22, // number density # M-3
            "number of particles" : 300.0e6 
        },
        {
                "name"	: "inflow",
                "type"	: "stream",
                "n"		: 4.818e23, // # M-3
                "T"		: 219.48, // K
                "U"	: [ 0.0, 0.0, 276.27 ] // - in critical crosssection
        },
        {
            "name"  : "backgroundone",
            "type"  : "stream",
            "p"     : 40.0, // Pa
            "T"     : __tw, // K
            "Ma"    : 0.0 // -
        },
        {
            "name"  : "backgroundtwo",
            "type"  : "stream",
            "p"     : 0.1, // Pa
            "T"     : __tw, // K
            "Ma"    : 0.0 // -
        },
        {
                "name"	: "initCond",
                "type"	: "vacuum"
        },
        {	"name" : "vacuumCond",
                "type" : "vacuum"
        },
        {
                "name"	: "specularCond",
                "type"	: "specular"
        },
        {
                "name"	: "surfaceCond",
                "type"	: "diffusive"
        }
        ],
        "state": [
                {
                        "name"	: "surfaceState",
                        "T"	: __tw
                }
        ],
	"initialization" : { // initial population of simulation volume
		"condition"	: "backgroundone"
	},
        "boundary": [
        {
            "name"	: "Wall",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                    "type" : "fe_collection",
                    "file" : {
                            "format"       : "stl",
                            "name"	   : "Wall_C1.stl",
                            "per meter"    : 1.0,
                            "tol"          : __tol,
                            "flip normals" : true // normals should point into fluid volume
                    }
            }
                //,"sample"	: {
                //	"averaging"	: {
                //		"type" 	: "delayed",
                //		"delay"	: __delay
                //	}
                //},
            ,"output"   : {
                "first"     : -1
            }
        },
        {
            "name"	: "Nozzle_Skimmer",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                "type"  : "fe_collection",
                "file"	: {
                    "format"       : "stl",
                    "name"	   : "Nozzle_Skimmer_C1.stl",
                    "per meter"    : 1.0,
                    "tol"          : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
                //,"sample"	: {
                //	"averaging"	: {
                //		"type" 	: "delayed",
                //		"delay"	: __delay
                //	}
                //},
           ,"output"	: {
                "first" : -1
           }
        },
        {
            "name"      : "ChamberOne",
            "condition" : "backgroundone",//"backgroundone",
            "geometry"  : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Chamber1_C1.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
        {
            "name"       : "SymmPlane_XZ",
            "condition"  : "specularCond",
            "geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "SymmPlane_XZ_C1.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
        {
                "name"		: "SymmPlane_YZ",
                "condition"	: "specularCond",
                "geometry"   : {
                    "type"  : "fe_collection",
                    "file"  : {
                        "format"    : "stl",
                        "name"      : "SymmPlane_YZ_C1.stl",
                        "per meter" : 1.0,
                        "tol"       : __tol,
                        "flip normals" : true // normals should point into fluid volume
                    }
                }
                ,"output" : {
                    "first" : -1
                }
        },
	{
            "name"	: "crossing",
            "condition"	: "backgroundtwo",
            "geometry"  : {
		"type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"	: "Outlet_C1.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
	},
        {
            "name"      : "CriticalDisk",
            "condition" : {
                "front": "inflow"
                //,"back": "vacuumCond"
            },
            "geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "CriticalDisk_C1.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
//            "geometry"  : {
//                "type"      : "disk",
//                "center"    : [ 0.0, 0.0, 0.0 ],
//                "normal"    : [0.0, 0.0, 1.0],
//                "radius"    : 3.1175e-3
//            }
        }
    ]
}

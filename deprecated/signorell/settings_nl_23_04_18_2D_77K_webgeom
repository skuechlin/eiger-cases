







{
    "description": [
        "ARGON SKIMMER FLOW",
        "D     = 0.5e-3 M        (skimmer diameter)",                     
        "Stagnation conditions:",
        "       p0 = 3000 Pa",
        "       T0 = 293 K",
        "Nozze geometry:",
        "       r* = 3.118332092e-3 M", 
        "       A* = 3.05488324e-5 M2",
        "       r_in = 5e-3 M",
        "       A_in = 7.854e-5 M2",
        " A_in/A* = 2.57095977",
        " Ma_in   = 4.0",
        " T_in    = 47.5 K",
        " p_in    = 40 Pa" 
    ]
    ,"simulation" : {
            "n timesteps"	: 11701,
            "info interval" : 1
    },
    "time step control": {
            "type" : "fixed",
            "dt"   : 1.5e-8,
            "scaling" 	: [ [0,	1.0] ]
    },
    "algorithm": {
            "type"	: "DSMC-MF"
    },
    "simulation box": {
            "origin": [ 0.0, 0.0, -0.001 ],
            "extent": [ 15.0e-3, 25.0e-3, 25.835e-3 ]
    },
    "gas model": {
        "type"      : "VSS",
        "species"   : "argon",
        "diatomic"  : false,
        "m"         : 6.63e-26,
        "d_ref"     : 4.17e-10,
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,
        "gamma"     : 1.67
    },  
    "grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
              "type"  : "uniform",
              "ni"    : 7500,
              "nj"    : 1,
              "nk"    : 12917,
              "level" : 0
           },
           "ordering" : "hilbert ChI",
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
            "name"  : "default",
            "level" : 8 
       },
       "data grid" : [
       {
            "name"		 : "sim",
            "is simulation grid" : true,
            "output"	    : {
                "interval" : 335,
                "last" : 7035
            },
            "averaging"	    : {
                "type" 	: "none"
            }
        },
        {
            "name" 	    : "sample",
            "output"	    : {
                        "interval"  : 1000
                        ,"first"	: 6700
                },
                "averaging"	: {
                        "type" 	: "delayed",
                        "delay"  : 6700,
                        "window size" : 20
                }
        }
        ],
         "grid adaption" : {
	    "cell volume correction" : {
	       	"correct cell volumes" 	: true,
	    	"remove empty cells"	: true
            } 
        },
         "load balancing" : {
                "grid" 	   : "sample",
                "first"    : 25,
                "interval" : 25,
                "last"	   : 6700,
                "threshold" : 0.99
        }
        },
        "condition": [
        {
            "name"  : "reference",
            "p"	        : 40.0,
            "T"	        : 47.5 ,
           "number of particles" : 60.0e6 
        },
        {
            "name"	: "freestream",
            "type"	: "stream",
            "Ma"        : 4.0,
            "p"	        : 40.0,
            "T"	        : 47.5 ,
            "direction"	: [ 0.0, 0.0, 1.0 ] 
        },
        {
            "name"  : "outflow",
            "type"  : "stream",
            "p"     : 0.01,
            "T"     : 77.0,
            "Ma"    : 0.0
        },
        {
                "name"	: "initCond",
                "type"	: "vacuum"
        },
        {	"name" : "vacuumCond",
                "type" : "vacuum"
        },
        {
                "name"	: "specularCond",
                "type"	: "specular"
        },
        {
                "name"	: "surfaceCond",
                "type"	: "diffusive"
        },
	{
		"name" : "periodicCond",
		"type" : "periodic",
		"distance" : 25.0e-3
 	}
        ],
        "state": [
                {
                        "name"	: "surfaceState",
                        "T"	: 77.0
                }
        ],
	"initialization" : {
		"condition"	: "vacuumCond"
	},
        "boundary": [
        {
            "name"	: "Skimmer",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_skimmer.stl",
                     	"per meter"    : 1.0,
                        "tol"          : 1.0e-9,
                        "flip normals" : true
		}
		
            } 
        },
		{
            "name"	: "TopOutflow",
            "condition"	: "outflow",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_top_out.stl",
                     	"per meter"    : 1.0,
                        "tol"          : 1.0e-9,
                        "flip normals" : true
		}
		
            } 
        },
		{
            "name"	: "TopFreestream",
            "condition"	: "freestream",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_top_free.stl",
                     	"per meter"    : 1.0,
                        "tol"          : 1.0e-9,
                        "flip normals" : true
		}
		
            } 
        },
		{
            "name"	: "Right",
            "condition"	: "freestream",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_right.stl",
                     	"per meter"    : 1.0,
                        "tol"          : 1.0e-9,
                        "flip normals" : true
		}
		
            } 
        }
		,
		{
            "name"	: "Left",
            "condition"	: "specularCond",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "23_04_left.stl",
                     	"per meter"    : 1.0,
                        "tol"          : 1.0e-9,
                        "flip normals" : true
		}
		
            } 
        },
		{
            "name"	: "Front",
            "condition"	: "periodicCond",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_front.stl",
                     	"per meter"    : 1.0,
                        "tol"          : 1.0e-9,
                        "flip normals" : true
		}
		
            } 
        },
		{
            "name"	: "Back",
            "condition"	: "periodicCond",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_back.stl",
                     	"per meter"    : 1.0,
                        "tol"          : 1.0e-9,
                        "flip normals" : true
		}
		
            } 
        },
		{
            "name"	: "Bottom",
            "condition"	: "freestream",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_bottom.stl",
                     	"per meter"    : 1.0,
                        "tol"          : 1.0e-9,
                        "flip normals" : true
		}
		
            } 
        }
	
            ]
}

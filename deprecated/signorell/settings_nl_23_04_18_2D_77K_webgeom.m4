changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'use Math::Trig; printf ($1)')]])

//Math
define(__pi,3.141592653589793) //pi

define(__lambda,2.4e-4) //mean free path [m]
define(__uref,620.0) //reference velocity [m/s]
//Geometry Definition
define(__rskimmer,0.25e-3)//radius of the skimmer [m]]
define(__h,2.0e-6)//grid size [m]
define(__lx,15.0e-3)
define(__ly,25.0e-3)
define(__lz,25.835e-3)
define(__d,0.001)//distance between upstream boundary and skimmer [m]
define(__d2,calc(__lz-__d))//distancwee in z direction in order to get the same geometry [m]
define(__b,50.0e-6)//skimmer inlet thickness (assumed to be 0.05mm, tenth of the radius)
define(__theta,100.0) //inner angle
define(__alpha,75.0) //outer anlge
define(__gamma,22.5)//bottom angle
define(__lp1,37.0*__lambda) //length of plate (parallelogram) 1
define(__x,calc(sin(__pi/180.0*__gamma)/sin(__pi/180.0*(__alpha-__gamma))*__b)) //help variable bottom plate
define(__y,calc(__x*sin(__pi/180*(90.0-__alpha)))) //additional distance due to geometry change of bottom plate
define(__z,calc(__x*cos(__pi/180*(90.0-__alpha)))) //z-distance due to geometry change of bottom plate
//Time Definitions
//define(__ts,calc(0.5*__h/__uref))//time step [s] =3.263e-8s
//define(__tdc,calc(__lz/__uref)) //domain crossing time [s]
//define(__ttot,calc(10.0*__tdc))//total simulation time [s]
define(__delay,6700) // guess: 5 domain crossing times at u_ref = 620m/s
define(__tw,77.0) // wall temperature
define(__tol,1.0e-9) // geometry file import abolute tolerance [m]
//Inflow Paramters
define(__tin,47.5) // inflow temperature K
define(__pin,40.0) // inflow pressure Pa
define(__main,4.0) // inflow Mach number

{
    "description": [// not interpreted by code
        "ARGON SKIMMER FLOW",
        "D     = 0.5e-3 M        (skimmer diameter)",                     
        "Stagnation conditions:",
        "       p0 = 3000 Pa",
        "       T0 = 293 K",
        "Nozze geometry:",
        "       r* = 3.118332092e-3 M", 
        "       A* = 3.05488324e-5 M2",
        "       r_in = 5e-3 M",
        "       A_in = 7.854e-5 M2",
        " A_in/A* = 2.57095977",
        " Ma_in   = 4.0",
        " T_in    = 47.5 K",
        " p_in    = 40 Pa" 
    ]
    ,"simulation" : {
            "n timesteps"	: 11701,// total number of timesteps in the simulation, chosen as twice the delay time 
            "info interval" : 1
    },
    "time step control": {
            "type" : "fixed",
            "dt"   : 1.5e-8, // Time Step in [s] Neu: h_ref = 0.1935mm, u_ref = (1+1/sqrt(gamma))*sqrt(gamma*R*T) = 380m/s -> 0.5 h_ref/u_ref = 2.5e-7s  
            "scaling" 	: [ [0,	1.0] ]
    },
    "algorithm": {
            "type"	: "DSMC-MF"//"FP-DSMC",
            //"DSMC"	: {
             //       "type" : "DSMC-MF"
            //},
            //"FOKKER-PLANCK" : {
             //       "type" : "FOKKER-PLANCK"
            //}
    },
    "simulation box": { // bounding box of simulated volume
            "origin": [ 0.0, 0.0, calc(-__d) ], // coordinates of lowest corner, values in m
            "extent": [ __lx, __ly, __lz ] // edge length of box in m
    },
    "gas model": {
        "type"      : "VSS",
        "species"   : "argon", //not interpreted
        "diatomic"  : false,
        "m"         : 6.63e-26,     //[kg]
        "d_ref"     : 4.17e-10,     //[m]
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,        //[K]
        "gamma"     : 1.67          //[-] 1 + 2/f, f=3 (f: DoF)
    },  
    "grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
              "type"  : "uniform",
              "ni"    : calc(int(__lx/__h)), // __lx/h number of cells in x-dimension, approx. 125 cells
              "nj"    : 1, // y
              "nk"    : calc(int(__lz/__h)), // __lz/h z, approx. 1200 cells
              "level" : 0   // background resolution: n(i,j,k) * 2^level
           },
           "ordering" : "hilbert ChI",//"morton",//
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
            "name"  : "default",
            "level" : 8 
       },
       "data grid" : [
       {
            "name"		 : "sim",
            "is simulation grid" : true,
            "output"	    : {
                "interval" : 335, // time step interval to write output file
                "last" : calc(__delay + 335) // last time step at which to write output
            },
            "averaging"	    : {//instantaneous results
                "type" 	: "none"
            }
        },
        {
            "name" 	    : "sample",
            "output"	    : {
                        "interval"  : 1000 // choose to have output ts coincide with last ts 28000 is multiple of 500
                        ,"first"	: __delay // first time step at which to start writing output files
                },
                "averaging"	: {
                        "type" 	: "delayed", // do exponentially weighted moving time average from timestep 0 to delay-1 with given window size, then arithmetic average
                        "delay"  : __delay,// number of timesteps before beginning time averaging 
                        "window size" : 20
                }
        }
        ],
         "grid adaption" : {
	    "cell volume correction" : {
	       	"correct cell volumes" 	: true,
	    	"remove empty cells"	: true
                //,"boundaries"            : ["Wall1","Wall2","Wall3","Wall ...]
            } 
          // ,"refine intersected cells" : [ {"boundaries" : ["Skimmer","Nozzle"],"level" : 2} ]
          // ,"refine cells inside"      : [
          //      {"boundaries" : ["cylinder","ldisk","rdisk"],"level" : 3}
                //, {"boundaries" : [d"cylinder2","ldisk2","rdisk2"],"level" : 0}
          // ]
//		  , "dynamic" : {
//                	  "criterion" : "mfp",
//			  "threshold" : 50.0,
//                        "grid"      : "sample",
//			  "first"     : 50,
//			  "last"      : __delay,
//			  "interval"  : 50
//		}
        },
         "load balancing" : {
                "grid" 	   : "sample",
                "first"    : 25,
                "interval" : 25,
                "last"	   : __delay,
                "threshold" : 0.99
        }
        },
        "condition": [
        {
            "name"  : "reference",      // at density n, aim for given number of particles
            "p"	        : __pin, // Pa 
            "T"	        : __tin , // K
           "number of particles" : 60.0e6 
        },
        {
            "name"	: "freestream",
            "type"	: "stream",
            "Ma"        : __main, // -
            "p"	        : __pin, // Pa 
            "T"	        : __tin , // K
            "direction"	: [ 0.0, 0.0, 1.0 ] 
        },
//        {
//            "name"  : "backgroundone",
//            "type"  : "stream",
//            "p"     : 40.0, // Pa
//            "T"     : __tw, // K
//            "Ma"    : 0.0 // -
//        },
        {
            "name"  : "outflow",
            "type"  : "stream",
            "p"     : 0.01, // Pa
            "T"     : __tw, // K
            "Ma"    : 0.0 // -
        },
        {
                "name"	: "initCond",
                "type"	: "vacuum"
        },
        {	"name" : "vacuumCond",
                "type" : "vacuum"
        },
        {
                "name"	: "specularCond",
                "type"	: "specular"
        },
        {
                "name"	: "surfaceCond",
                "type"	: "diffusive"
        },
	{
		"name" : "periodicCond",
		"type" : "periodic",
		"distance" : __ly
 	}
        ],
        "state": [
                {
                        "name"	: "surfaceState",
                        "T"	: __tw //wall temperature
                }
        ],
	"initialization" : { // initial population of simulation volume
		"condition"	: "vacuumCond"
	},
        "boundary": [
        {
            "name"	: "Skimmer",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_skimmer.stl",
                     	"per meter"    : 1.0,
                        "tol"          : __tol,
                        "flip normals" : true // normals should point into fluid volume
		}
		
            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        },
		{
            "name"	: "TopOutflow",
            "condition"	: "outflow",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_top_out.stl",
                     	"per meter"    : 1.0,
                        "tol"          : __tol,
                        "flip normals" : true // normals should point into fluid volume
		}
		
            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        },
		{
            "name"	: "TopFreestream",
            "condition"	: "freestream",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_top_free.stl",
                     	"per meter"    : 1.0,
                        "tol"          : __tol,
                        "flip normals" : true // normals should point into fluid volume
		}
		
            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        },
		{
            "name"	: "Right",
            "condition"	: "freestream",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_right.stl",
                     	"per meter"    : 1.0,
                        "tol"          : __tol,
                        "flip normals" : true // normals should point into fluid volume
		}
		
            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        }
		,
		{
            "name"	: "Left",
            "condition"	: "specularCond",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "23_04_left.stl",
                     	"per meter"    : 1.0,
                        "tol"          : __tol,
                        "flip normals" : true // normals should point into fluid volume
		}
		
            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        },
		{
            "name"	: "Front",
            "condition"	: "periodicCond",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_front.stl",
                     	"per meter"    : 1.0,
                        "tol"          : __tol,
                        "flip normals" : true // normals should point into fluid volume
		}
		
            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        },
		{
            "name"	: "Back",
            "condition"	: "periodicCond",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_back.stl",
                     	"per meter"    : 1.0,
                        "tol"          : __tol,
                        "flip normals" : true // normals should point into fluid volume
		}
		
            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        },
		{
            "name"	: "Bottom",
            "condition"	: "freestream",
            "geometry"	: {
              "type" : "fe_collection",
		"file":{
			"format"       : "stl",
                        "name"	   : "23_04_bottom.stl",
                     	"per meter"    : 1.0,
                        "tol"          : __tol,
                        "flip normals" : true // normals should point into fluid volume
		}
		
            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        }
	
            ]
}

changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__delay, 12000) // guess: 5 domain crossing times at u_crit = 270m/s
define(__tw,293.0) // wall temperature
define(__tol,1.0e-9) // geometry file import abolute tolerance [m]

define(__tin,288.06016064) // inflow temperature K
define(__pin,2875.51284) // inflow pressure Pa
define(__main,0.22625195) // inflow Mach number

{
    "description": [
        "ARGON EXPANSION AND SKIMMER FLOW",
        "D     = 0.5e-3 M        (skimmer diameter)",                     
        "Stagnation conditions:",
        "       p0 = 3000 Pa",
        "       T0 = 293 K",
        "Nozze geometry:",
        "       r* = 3.118332092e-3 M", 
        "       A* = 3.05488324e-5 M2",
        "       r_in = 5e-3 M",
        "       A_in = 7.854e-5 M2",
        " A_in/A* = 2.57095977",
        " Ma_in   = 0.22625195",
        " T_in    = 288.06016064 K",
        " p_in    = 2875.51284 Pa" 
    ]
    ,"simulation" : {
            "n timesteps"	: 24001, // 2x delay
            "info interval" : 1
    },
    "time step control": {
            "type" : "fixed",
            "dt"   : 25.0e-8, //Neu: h_ref = 0.1935mm, u_ref = (1+1/sqrt(gamma))*sqrt(gamma*R*T) = 380m/s -> 0.5 h_ref/u_ref = 2.5e-7s  
            "scaling" 	: [ [0,	1.0] ]
    },
    "algorithm": {
            "type"	: "FP-DSMC",
            "DSMC"	: {
                    "type" : "DSMC-MF"
            },
            "FOKKER-PLANCK" : {
                    "type" : "FOKKER-PLANCK"
            }
    },
    "simulation box": { // bounding box of simulated volume
            "origin": [ 0.0, 0.0, 0.0 ], // coordinates of lowest corner, values in m
            "extent": [ 50.0e-3, 50.0e-3, 170.0e-3 ] // edge length of box in m
    },
    "gas model": {
        "type"      : "VSS",
        "species"   : "argon",
        "diatomic"  : false,
        "m"         : 6.63e-26,     //[kg]
        "d_ref"     : 4.17e-10,     //[m]
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,        //[K]
        "gamma"     : 1.67          //[-] 1 + 2/f, f=3 (f: DoF)
    },  
    "grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
               "type"  : "uniform",
               "ni"    : 10, // number of cells in x-dimension
               "nj"    : 10, // y
               "nk"    : 34, // z
               "level" : 7   // background resolution: n(i,j,k) * 2^level
           },
           "ordering" : "hilbert Butz",//"morton",//
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
            "name"  : "default",
            "level" : 6 
       },
       "data grid" : [
       {
            "name"		 : "sim",
            "is simulation grid" : true,
            "output"	    : {
                "interval" : 200, // time step interval to write output file
                "last" : calc(__delay + 250) // last time step at which to write output
            },
            "averaging"	    : {
                "type" 	: "none"
            }
        },
        {
            "name" 	    : "sample",
            "output"	    : {
                        "interval"  : 500
                        ,"first"	: __delay // first time step at which to start writing output files
                },
                "averaging"	: {
                        "type" 	: "delayed", // do exponentially weighted moving time average from timestep 0 to delay-1 with given window size, then arithmetic average
                        "delay"  : __delay, 
                        "window size" : 50
                }
        }
        ],
         "grid adaption" : {
	    "cell volume correction" : {
	       	"correct cell volumes" 	: true,
	    	"remove empty cells"	: true,
                "boundaries"            : ["Wall","Channel","Skimmer","Nozzle","Background","Inflow","Outflow"]
            } 
           ,"refine intersected cells" : [ {"boundaries" : ["Skimmer","Nozzle"],"level" : 1} ]
           ,"refine cells inside"      : [ {"boundaries" : ["cylinder","ldisk","rdisk"],"level" : 2}, {"boundaries" : ["cylinder2","ldisk2","rdisk2"],"level" : 0} ]
//		  , "dynamic" : {
//                	  "criterion" : "mfp",
//			  "threshold" : 50.0,
//                        "grid"      : "sample",
//			  "first"     : 50,
//			  "last"      : __delay,
//			  "interval"  : 50
//		}
        },
         "load balancing" : {
                "grid" 	   : "sample",
                "first"    : 25,
                "interval" : 25,
                "last"	   : __delay,
                "threshold" : 0.99
        }
        },
        "condition": [
        {
            "name"  : "reference",      // at density n, aim for given number of particles
            "n"     : 1.0e22,           // number density # M-3
            "number of particles" : 300.0e6 
        },
        {
            "name"	: "inflow",
            "type"	: "stream",
            "Ma"        : __main, // -
            "p"	        : __pin, // Pa 
            "T"	        : __tin , // K
            "direction"	: [ 0.0, 0.0, 1.0 ] 
        },
        {
            "name"  : "backgroundone",
            "type"  : "stream",
            "p"     : 40.0, // Pa
            "T"     : __tw, // K
            "Ma"    : 0.0 // -
        },
        {
            "name"  : "backgroundtwo",
            "type"  : "stream",
            "p"     : 0.1, // Pa
            "T"     : __tw, // K
            "Ma"    : 0.0 // -
        },
        {
                "name"	: "initCond",
                "type"	: "vacuum"
        },
        {	"name" : "vacuumCond",
                "type" : "vacuum"
        },
        {
                "name"	: "specularCond",
                "type"	: "specular"
        },
        {
                "name"	: "surfaceCond",
                "type"	: "diffusive"
        }
        ],
        "state": [
                {
                        "name"	: "surfaceState",
                        "T"	: __tw
                }
        ],
	"initialization" : { // initial population of simulation volume
		"condition"	: "backgroundone"
	},
        "boundary": [
        {
            "name"	: "Wall",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                    "type" : "fe_collection",
                    "file" : {
                            "format"       : "stl",
                            "name"	   : "Wall_Quarter_00.stl",
                            "per meter"    : 1.0,
                            "tol"          : __tol,
                            "flip normals" : true // normals should point into fluid volume
                    }
            } 
            ,"output"   : {
                "first"     : -1
            }
        },
        {
            "name"	: "Nozzle",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                "type"  : "fe_collection",
                "file"	: {
                    "format"       : "stl",
                    "name"	   : "Nozzle_Quarter_00.stl",
                    "per meter"    : 1.0,
                    "tol"          : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
           ,"output"	: {
                "first" : -1
           }
        },
        {
            "name"	: "Skimmer",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                "type"  : "fe_collection",
                "file"	: {
                    "format"       : "stl",
                    "name"	   : "Skimmer_Quarter_00.stl",
                    "per meter"    : 1.0,
                    "tol"          : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
           ,"output"	: {
                "first" : -1
           }
        },
        {
            "name"      : "Background",
            "condition" : "backgroundone",
            "geometry"  : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Background_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
        {
            "name"       : "Symmetry",
            "condition"  : "specularCond",
            "geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Symmetry_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
	{
            "name"	: "Channel",
            "condition"	: "surfaceCond",
            "state"     : "surfaceState",
            "geometry"  : {
		"type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"	: "Channel_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
	},
        {
            "name"      : "Inflow",
            "condition" : {
                "front": "inflow"
                //,"back": "vacuumCond"
            },
            "geometry"   : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Inflow_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
        {
            "name"      : "Outflow",
            "condition" : "backgroundtwo",
            "geometry"  : {
                "type"  : "fe_collection",
                "file"  : {
                    "format"    : "stl",
                    "name"      : "Outflow_Quarter_00.stl",
                    "per meter" : 1.0,
                    "tol"       : __tol,
                    "flip normals" : true // normals should point into fluid volume
                }
            }
            ,"output" : {
                "first" : -1
            }
        },
        {
            "name"      : "cylinder",
            "geometry"  : {
            "type"      : "cylinder",
                "base"      : [0.0, 0.0, 0.0],
                "direction" : [0.0, 0.0, 1.0],
                "radius"    : 0.01,
                "length"    : 0.160,
                "internal"  : true
            }
        },  
        {
            "name"      : "ldisk",
            "geometry"  : {
                "type"      : "disk",
                "center"    : [0.0, 0.0, 0.0],
                "normal"    : [0.0, 0.0, 1.0],
                "radius"    : 0.01 
            }
        },
        {
            "name"      : "rdisk",
            "geometry"  : {      
                "type"      : "disk",
                "center"    : [0.0, 0.0, 0.160],
                "normal"    : [0.0, 0.0, -1.0],
                "radius"    : 0.01 
            }
        },
        {
            "name"      : "cylinder2",
            "geometry"  : {
            "type"      : "cylinder",
                "base"      : [0.0, 0.0, 0.1315],
                "direction" : [0.0, 0.0, 1.0],
                "radius"    : 0.001275,
                "length"    : 0.0285,
                "internal"  : true
            }
        }, 
        {
            "name"      : "ldisk2",
            "geometry"  : {
                "type"      : "disk",
                "center"    : [0.0, 0.0, 0.1315],
                "normal"    : [0.0, 0.0, 1.0],
                "radius"    : 0.001275 
            }
        },
        {
            "name"      : "rdisk2",
            "geometry"  : {      
                "type"      : "disk",
                "center"    : [0.0, 0.0, 0.160],
                "normal"    : [0.0, 0.0, -1.0],
                "radius"    : 0.001275 
            }
        } 
    ]
}

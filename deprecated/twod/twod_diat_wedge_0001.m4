changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'use Math::Trig; printf ($1)')]])

define(__pi, 3.1415926535)

define(__xmin, 0)
define(__xmax, 0.175) // 17.5 cm
define(__ymin, 0)
define(__ymax, 0.15) // 15 cm
define(__zmin, 0)
define(__zmax, 1.0)

// the wedge
define(__Lw, 0.051)
define(__alphaw, 35.0*__pi/180.0)
define(__xw0, calc( __xmin + 0.03))
define(__yw0, __ymin)
define(__xw1, calc( __xw0 + __Lw*cos(__alphaw)))
define(__yw1, calc( __yw0 + __Lw*sin(__alphaw)))


define(__delay, 11000)
define(__interval,250)

{
	"description": [
	"Ma-11 NITROGEN FLOW OVER WEDGE",	 
	"GORJI, 10.5.6",
	"BOYD (1991)"
	],
	
	"simulation": {
		"n timesteps"	: 22001, // 11000 averaging steps
		"info interval" : 100
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 6.0e-8,	//[s] cfl 0.5 for Ma11 and x cell width
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "FOKKER-PLANCK"
	},


define(__lx, calc( __xmax - __xmin) )
define(__ly, calc( __ymax - __ymin) )
define(__lz, calc( __zmax - __zmin) )

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},

	// use coefficients from Hosseins code
	"gas model": {
		"type" 		: "VSS",
		"species" 	: "Nitrogen",
		"diatomic"	: true,
		"m"			: 46.5e-27,		//[kg]
		"mu_ref" 	: 1.656e-5, 	//[N s m-2]
		"omega" 	: 0.74,
		"alpha"		: 1.36,
		"T_ref" 	: 273.0,		//[K]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18.0,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3390.0		//[K]
	},	

		
 "grid": {
        "topology": {
            "dimension": 3,
            "discretization": {
                "type": "uniform",
                "ni": 250,
                "nj": 250,
                "nk": 1,
                "level": 0
            },
            "ordering": "morton",
            "coordinate system": {
                "type": "cartesian"
            }
        },
        "particle grid": {
            "name": "default",
            "level": 0
        },
        "data grid": [
            {
                "name": "sim",
                "is simulation grid": true,
                "output": {
                    "interval": __interval
                },
                "averaging": {
                    "type": "none"
                }
            },
            {
                "name": "sample",
                "output": {
                    "interval": __interval,
                    "first": __delay
                },
                "averaging": {
                    "type": "delayed",
                    "delay": __delay,
                    "window size": __delay
                }
            }
        ],
        "load balancing" : {
            "grid" : "sample",
            "first"     : 50,
            "interval"  : 50,
            "last"      : __delay,
            "threshold" : 0.98
        }
    },
    "condition": [
        {
          "name" : "reference",
          "rho": 2.4e-5,
          "number of particles" : 5e6
        },
		{
			"name"	: "initCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "outletCond",
			"type"	: "vacuum"
		},
		{
			"name"	: "inletCond",
			"type"	: "stream",
			"PDF"	: "Maxwellian",
			"rho"	: 2.4e-5, 	//[kg m-3]
			"T"		: 600.0,	//[K]
			"Ma"	: 11.0
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specCond",
			"type"	: "specular"
		},
		{
			"name"	: "periodicCond",
			"type"	: "periodic",
			"distance" : __lz
		}
	],
	    "state": [
        {
            "name": "surfaceState",
            "T": 300.0
        }
    ],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name" 	: "inlet",
			"type"	: "inlet",
			"condition" : "inletCond",
            "geometry"  : {
                "type"     : "parallelogram",
                "box_face" : "left"
            }
		},
		{
			"name"	: "outlet",
			"type"	: "default",
			"condition"	: "outletCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "right"
			}
		},
		{
			"name"		: "wedgeFront",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[__xw0, __yw0, __zmin],
					[__xw0, __yw0, __zmax],
					[__xw1, __yw1, __zmin]
				]
			}
		},
		{
			"name"		: "wedgeBack",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"state"		: "surfaceState",
			"geometry"	: {
				"type"	: "parallelogram",
				"vertices" : [
					[__xw1, __yw0, __zmax],
					[__xw1, __yw0, __zmin],
					[__xw1, __yw1, __zmax]
				]
			}
		},

		{
			"name"		: "specY0",
			"type"		: "default",
			"condition"	: "specCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "front"
			}
		},		
		{
			"name"		: "specY1",
			"type"		: "default",
			"condition"	: "specCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "back"
			}
		},	
		{
			"name"		: "periodicZ0",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "bottom"
			}
		},		
		{
			"name"		: "periodicZ1",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "top"
			}
		}		
	]
}
	

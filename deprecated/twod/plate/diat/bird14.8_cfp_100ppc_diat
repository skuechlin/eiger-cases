{
	"description": [
	"FLAT PLATE AT INCIDENCE",
	"L      = 0.215014181737239 M",
	"N      = 1.4e20 M-3",
    "MU     = 1.67353e-05 PAS",
	"MFP    = 0.00924560981470126  M (VariableHardSphere)",
    "KN     = 0.043",
	"DT CFL = .5*DX/sqrt(theta) = 9.57409777805649e-07 S",
    "DT NU0 = .5*1.0/nu0        = 1.0175107823931e-05  S",
    "DT     = 9.57409777805649e-07 S",
	"DX     = 0.645042545211717/200 = 0.00322521272605859 M"
	],
	"simulation" : {
		"n timesteps"	: 18001 ,
		"info interval" : 400
	},
	"time step control": {
        "dt"		: 9.57409777805649e-07
    },
	"algorithm": {
    "type"  : "FOKKER-PLANCK"
    ,"model" : "cubic"
	},
	"simulation box": {
		"origin": [ -0.215014181737239, -0.215014181737239, -0.5 ],
		"extent": [ 0.645042545211717, 0.645042545211717, 1.0 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "nitrogen",
		"diatomic"	: true,
		"m"			: 4.65e-26,
		"d_ref" 	: 4.16999722773876e-10 ,
		"alpha" 	: 1.0,
		"omega" 	: 0.75,
		"T_ref" 	: 273.0,
		"gamma"		: 1.4,
        "Zrot_inf"	: 18.1,
        "Trot_ref"	: 91.5,
        "Cvib_1"	: 9.1,
        "Cvib_2"	: 220.0,
        "Tvib_ref"	: 3340.0
    },
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 200,
	           "nj"    : 200,
	           "nk"    : 1,
	           "level" : 0
	       },
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 0
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 200
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : 200,
    				"first"	    : 6000
    			},
    			"averaging"	: {
    				"type"   : "switched",
    				"switch" : 6000,
                    "first"  : {"type" : "exponential","window size" : 100},
                    "second" : {"type" : "arithmetic", "skip" : 0}
                }
    		}
    	]
    	, "grid adaption" : {
            "cell volume correction" : { "correct cell volumes" : false, "remove empty cells" : false }
    	}
	},
	"condition": [
        {
            "name" : "reference",
            "n"    : 1.4e20,
            "T"    : 273.0,
            "number of particles" : 4000000
        },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1.4e20,
			"T"		: 273.0,
			"Ma"	: 5.0,
            "yaw"   : 30.0
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: 0.645042545211717
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: 0.645042545211717
		},
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: 1.0
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],
	"state": [
		{
			"name"	: "windwardPlateState",
			"T"		: 1638
        },
        {
            "name" : "leewardPlateState",
            "T"    :  273.0
		}
	],
	"initialization" : {
		"condition"	: "streamCond"
	},
	"boundary": [
        {
            "name"      : "plate"
            ,"condition" : {"front" : "surfaceCond", "back" : "surfaceCond"}
            ,"state"     : {"front" : "windwardPlateState", "back" : "leewardPlateState" }
            ,"geometry"	: {
				"type"  : "fe_collection",
				"file"	: {
					"format"	: "stl",
					"name"		: "plate_00.stl",
					"tol"		: 1e-6,
					"flip normals" : false
				}
			}
            ,"sample" : {
				"averaging"	: {
					"type"  	: "switched",
					"switch"	: 6000,
                    "first"  : { "type" : "none" },
                    "second" : { "type" : "arithmetic" }
				}
			},
			"output"	: { "first" : 18000 }
        },
        {
			"name"		: "right",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		},
		{
			"name"		: "top",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		}
	]
}

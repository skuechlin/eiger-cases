{
	"description": [
	"FLAT PLATE AT INCIDENCE",
	"L     = 1 M",
	"N     = 1.4e20 M-3",
    "MU    = 2.117e-05 PAS",
	"MFP   = 0.00925253787733737  M (VariableHardSphere)",
    "KN    = 0.0107874865156418",
	"DT    =  .25*DX/sqrt(theta) = 1.39322107287901e-06  S",
	"DX    = L/200 = 0.00428855130614587 M"
	],
	"simulation" : {
		"n timesteps"	: 4001 ,
		"info interval" : 1000
	},
	"time step control": {
        "dt"		: 1.39322107287901e-06
    },
	"algorithm": {
    "type"  : "FOKKER-PLANCK",
    "model" : "gfp13",
    "position integration" : "euler"
	},
	"simulation box": {
		"origin": [ 0.0, 0.0, 0.0 ],
		"extent": [ 0.857710261229175, 0.857710261229175, 1.0 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 66.3e-27,
		"d_ref" 	: 4.17E-10,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.66666666666667
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 200,
	           "nj"    : 200,
	           "nk"    : 1,
	           "level" : 0
	       },
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 0
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 1000
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : 1000 ,
    				"first"	    : 2000
    			},
    			"averaging"	: {
    				"type"   : "switched",
    				"switch" : 2000,
                    "first"  : {"type" : "none"},
                    "second" : {"type" : "arithmetic", "skip" : 0}
                }
    		}
    	]
	},
	"condition": [
        {
            "name" : "reference",
            "n"    : 1.4e20,
            "T"    : 273.0,
            "number of particles" : 100000
        },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1.4e20,
			"T"		: 273.0,
			"Ma"	: 5.0,
            "yaw"   : 30.0
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: 0.857710261229175
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: 0.857710261229175
		},
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: 1.0
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],
	"state": [
		{
			"name"		: "plateState",
			"T"			: 2548.00000000001 ,
            "T back"    : 273.0
		}
	],
	"initialization" : {
		"condition"	: "streamCond"
	},
	"boundary": [
        {
            "name"      : "plate",
            "condition" : {"front" : "surfaceCond", "back" : "surfaceCond"},
            "state"     : "plateState",
            "geometry"  : {
                "type"      : "parallelogram",
                "vertices"  : [
                    [0.285903420409725,0.285903420409725,1],
                    [0.57180684081945,0.285903420409725,1],
                    [0.285903420409725,0.285903420409725,0.0]
               ]
            }
        },
        {
			"name"		: "right",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		},
		{
			"name"		: "top",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		}
	]
}

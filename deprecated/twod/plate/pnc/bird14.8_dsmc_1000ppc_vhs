{
	"description": [
	"FLAT PLATE AT INCIDENCE",
	"L      = 0.215175299472962 M",
	"N      = 1.4e20 M-3",
    "MU     = 2.117e-05 PAS",
	"MFP    = 0.00925253787733735  M (VariableHardSphere)",
    "KN     = 0.043",
	"DT CFL = .5*DX/sqrt(theta) = 1.04855954909235e-06 S",
    "DT NU0 = .5*1.0/nu0        = 1.21589092818255e-05  S",
    "DT     = 1.04855954909235e-06 S",
	"DX     = 0.645525898418886/200 = 0.00322762949209443 M"
	],
	"simulation" : {
		"n timesteps"	: 12001 ,
		"info interval" : 400
	},
	"time step control": {
        "dt"		: 1.04855954909235e-06
    },
	"algorithm": {
    "type" : "DSMC-MF"
	},
	"simulation box": {
		"origin": [ -0.215175299472962, -0.215175299472962, -0.5 ],
		"extent": [ 0.645525898418886, 0.645525898418886, 1.0 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 66.3e-27,
		"d_ref" 	: 4.16843574199195e-10 ,
		"alpha" 	: 1.0,
		"omega" 	: 0.81,
		"T_ref" 	: 273.0,
		"gamma"		: 1.66666666666667
	},
	"grid": {
	   "topology" : {
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : 200,
	           "nj"    : 200,
	           "nk"    : 1,
	           "level" : 0
	       },
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 0
    	},
	   "data grid" : [
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : 200
    			},
    			"averaging"	    : {
    				"type" 	: "none"
    			}
    		},
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : 200 ,
    				"first"	    : 4000
    			},
    			"averaging"	: {
    				"type"   : "switched",
    				"switch" : 4000,
                    "first"  : {"type" : "exponential","window size" : 75},
                    "second" : {"type" : "arithmetic", "skip" : 0}
                }
    		}
    	]
	},
	"condition": [
        {
            "name" : "reference",
            "n"    : 1.4e20,
            "T"    : 273.0,
            "number of particles" : 40000000
        },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: 1.4e20,
			"T"		: 273.0,
			"Ma"	: 5.0,
            "yaw"   : 30.0
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: 0.645525898418886
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: 0.645525898418886
		},
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: 1.0
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],
	"state": [
		{
			"name"	: "windwardPlateState",
			"T"		: 2548.00000000001
        },
        {
            "name" : "leewardPlateState",
            "T"    :  273.0
		}
	],
	"initialization" : {
		"condition"	: "streamCond"
	},
	"boundary": [
        {
            "name"      : "plate",
            "condition" : {"front" : "surfaceCond", "back" : "surfaceCond"},
            "state"     : {"front" : "windwardPlateState", "back" : "leewardPlateState" },
            "geometry"  : {
                "type"      : "parallelogram",
                "vertices"  : [
                    [0.0,0.0,0.5],
                    [0.0,0.0,-0.5],
                    [0.215175299472962,0.0,0.5]
               ]
            }
        },
        {
			"name"		: "right",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		},
		{
			"name"		: "top",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},
		{
			"name"		: "bottom",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		}
	]
}

changecom(//)changequote([[,]])
//define(min, [[esyscmd(perl -e 'use List::Util qw[min max]; printf("%e",min($1,$2))')]])
define(calc, [[esyscmd(perl -e 'use Math::Trig; use List::Util qw[min max]; printf ($1)')]])


define(__pi, 3.1415926535897932) 
define(__kB, 1.380649e-23) 

define(__yaw, 30.0)
define(__Ma,5.0)
define(__gamma, calc(5.0/3.0))

define(__Tin, 273.0)
define(__Tin_stag, calc(__Tin*(1.0 + 0.5*(__gamma - 1.0)*__Ma*__Ma)) )
define(__Tw_leeward,__Tin)
define(__Tw_windward,__Tin_stag)

// argon
define(__m, 66.3e-27)
define(__alpha, 1.0)
define(__omega, 0.81)
define(__mur,2.117e-5) 
define(__Tr,273.0)
define(__mu, calc( __mur*(__Tin/__Tr)**__omega ))
define(__R, calc(__kB / __m))
define(__dr, calc( sqrt(5.0*(__alpha + 1.0)*(__alpha + 2.0)*sqrt(__m*__kB*__Tr/__pi)/(4.0*__alpha*(5.0-2.0*__omega)*(7.0-2.0*__omega)*__mur)) ) )

define(__n, 1.4e20)
define(__rho, calc(__m*__n))

define(__Kn,0.043)

define(__lambda, calc( 1.0 / ( sqrt(2.0)*__pi*__dr*__dr*__n*(__Tr/__Tin)**(__omega-0.5) ) ))
define(__l_plate, calc(__lambda/__Kn))
define(__nu, calc( 4.0*__dr*__dr*__n*sqrt(__pi*__R*__Tr)*(__Tin/__Tr)**(1.0-__omega)  ) )

define(__ni,200)
define(__nj,200)
define(__nk,1)

define(__lx, calc(3.0*__l_plate))
define(__x_plate, 0.0)
define(__y_plate, 0.0)

define(__dx, calc(__lx/__ni)) 

define(__vin, __Ma*sqrt(__gamma*__R*__Tin))

define(__dt_cfl, calc(.5*__dx/(max(__vin,1.0)) ))
define(__dt_nu0, calc(.5*1.0/__nu) )
define(__dt, calc(min(__dt_cfl,__dt_nu0)))


define(__xmin, calc(-__l_plate))
define(__xmax, calc(__xmin + __lx))

define(__ly, __lx)
define(__ymin, __xmin)
define(__ymax, __xmax)

define(__zmin, -0.5)
define(__lz, 1.0)
define(__zmax,calc(__zmin+__lz))

define(__particlespercell, 100)
define(__delay, 4000)
define(__nts, calc(__delay*3+1) )
define(__interval, calc(__delay/20) )



{
	"description": [
	"FLAT PLATE AT INCIDENCE",
	"L      = __l_plate M",
	"N      = __n M-3",
    "MU     = __mu PAS",
	"MFP    = __lambda  M (VariableHardSphere)",
    "KN     = __Kn",
	"DT CFL = .5*DX/sqrt(theta) = __dt_cfl S",
    "DT NU0 = .5*1.0/nu0        = __dt_nu0 S", 
    "DT     = __dt S",
	"DX     = __lx/__ni = __dx M"
	],
	
	"simulation" : {
		"n timesteps"	: __nts,
		"info interval" : 400
	}, 
		
	"time step control": {
        "dt"		: __dt
    },
	
	"algorithm": {
    //"type" : "DSMC-MF"
    "type"  : "FOKKER-PLANCK",
    "model" : "gfp13",
    "productions" : "grad 26",
    "position integration" : "euler"
	//	"type"	: "HYBRID",
	//	"DSMC"	: {
	//		"type" : "DSMC-MF"
	//	},
	//	"FOKKER-PLANCK" : {
	//		"type" : "FOKKER-PLANCK"
	//	}
	}, 

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},


	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: __m,		//[kg]
		"d_ref" 	: __dr, 	//[m]
		"alpha" 	: __alpha,
		"omega" 	: __omega,
		"T_ref" 	: __Tr,		//[K]
		"gamma"		: __gamma	//[-] 1 + 2/f, f=3 (f: DoF)
	},	

		
	"grid": {
	
	   "topology" : {	   
	       "dimension" : 3,
	       "discretization" : {
	           "type"  : "uniform",
	           "ni"    : __ni,
	           "nj"    : __nj,
	           "nk"    : __nk,
	           "level" : 0 
	       },	   
	       "ordering" : "morton",
	       "coordinate system" : {
	           "type" : "cartesian"
	       }
	   },
	
	   "particle grid" : {
    	       	"name"  : "default",
    	       	"level" : 0 
    	},	   
	   
	   "data grid" : [ 
    		{
    			"name"		    : "sim",
    			"is simulation grid" : true,
    			"output"	    : {
    				"interval" : __interval
    			},
    			"averaging"	    : {
    				"type" 	: "none"				
    			}
    		}, 
    		{
    			"name" 		    : "sample",
    			"output"	    : {
    				"interval"  : __interval,//calc(__delay/25),
    				"first"	    : __delay
    			},
    			"averaging"	: {
    				"type"   : "switched",
    				"switch" : __delay,
                    "first"  : {"type" : "none"},
                    "second" : {"type" : "arithmetic", "skip" : 0}
                }
    		}
    		
    	]

    	//, "grid adaption" : {
    	//	"dynamic" : {
    	//		"criterion"	: "gradient",
    	//		//"criterion"	: "mfp",
    	//		"grid"		: "sample",
    	//		"variable"	: "n",
    	//		"reference" : __n,
    	//		"threshold" : 0.5,
    	//		"first"		: calc(__delay/5),
    	//		"last"		: calc(__delay-1),
    	//		"interval"	: calc(__delay/10)
    	//	}
    	//}

    	//,"load balancing" : {
    	//	"grid" 		: "sample",
    	//	"first" 	: 25,
    	//	"interval" 	: 100,
    	//	"last"		: __delay,
    	//	"threshold"	: 0.98
    	//}
	
	},
		
	//CONDITIONS

	
	"condition": [
        {
            "name" : "reference",
            "n"    : __n,
            "T"    : __Tin,
            "number of particles" : calc(__particlespercell*__ni*__nj)
        },
		{
			"name"	: "streamCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tin,	//[K]
			"Ma"	: __Ma,		//[-]
            "yaw"   : __yaw     //[deg]
		},
		{
			"name"		: "periodicXCond",
			"type"		: "periodic",
			"distance"	: __lx
		},
		{
			"name"		: "periodicYCond",
			"type"		: "periodic",
			"distance"	: __ly
		},		
		{
			"name"		: "periodicZCond",
			"type"		: "periodic",
			"distance"	: __lz
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive"
		},
		{
			"name"	: "specularCond",
			"type"	: "specular"
		}
	],

	"state": [
		{
			"name"	: "windwardPlateState",
			"T"		: __Tw_windward
        },
        {
            "name" : "leewardPlateState",
            "T"    :  __Tw_leeward
		}
	],
		
	"initialization" : {
		"condition"	: "streamCond"
	},
		
	"boundary": [		
        {
            "name"      : "plate",
            "condition" : {"front" : "surfaceCond", "back" : "surfaceCond"},
            "state"     : {"front" : "windwardPlateState", "back" : "leewardPlateState" }, 
            "geometry"  : {
                "type"      : "parallelogram",
                "vertices"  : [ 
                    [__x_plate,__y_plate,__zmax],
                    [__x_plate,__y_plate,__zmin],
                    [calc(__x_plate + __l_plate),__y_plate,__zmax]
               ] 
            }
        },
        {
			"name"		: "right",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "right"
			}
		},
		{
			"name"		: "left",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "left"
			}
		},		
		{
			"name"		: "front",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "front"
			}
		},	
		{
			"name"		: "back",
			"condition"	: "streamCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "back"
			}
		},	
		{
			"name"		: "top",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "top"
			}
		},		
		{
			"name"		: "bottom",
			"condition"	: "periodicZCond",
			"geometry"	: {
				"type"     : "parallelogram",
				"box_face" : "bottom"
			}
		}		
	]
}
	

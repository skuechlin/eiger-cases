changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__d, 1.3303942) // wall distance

define(__xmin, 0)
define(__xmax, __d)
define(__ymin, 0)
define(__ymax, __d)
define(__zmin, 0)
define(__zmax, __d)

define(__particlespercell, 1000)
define(__delay, 100000)
define(__interval,100)

{
	"description": [
	"COUETTE FLOW OF DIATOMIC NITROGEN AT KN = 0.01",	 
	"GORJI, 10.5",
	"MOVE BOTH WALLS, ONE -50m/s, THE OTHER +50m/s"
	],
	
	"simulation": {
		"n timesteps"	: 170001, // 70000 averaging steps
		"info interval" : 100
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 5.0e-3,	//[s] 
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "FOKKER-PLANCK"
	},


define(__lx, calc( abs(__xmax) + abs(__xmin) ))
define(__ly, calc( abs(__ymax) + abs(__ymin) ))
define(__lz, calc( abs(__zmax) + abs(__zmin) ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},

	"gas model": {
		"type" 		: "VHS",
		"species" 	: "Nitrogen",
		"diatomic"	: true,
		"m"			: 46.5e-27,		//[kg]
		"mu_ref" 	: 1.656e-5, 	//[N s m-2]
		"omega" 	: 0.74,
		"T_ref" 	: 273,			//[K]
		"gamma"		: 1.4,			//[-]
		"Zrot_inf"	: 18,			//[-]
		"Trot_ref"	: 91.5,			//[K]
		"Cvib_1"	: 9.1,			//[-]
		"Cvib_2"	: 220.0,		//[-]
		"Tvib_ref"	: 3340.0		//[K]
	},	

		
	"mpi": {
		"nx": 20, // one d
		"ny": 1,
		"nz": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	//INITIAL CONDITIONS
	
	define(__Tinit, 273)
	define(__Tw, 273)
	define(__n, 1.0e20)
	
	"condition": [
		{
			"name"	: "initCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tinit,	//[K]
			"Ma"	: 0.0,
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: __Tw
		},
		{
			"name"	: "periodicCond",
			"type"	: "periodic",
			"distance" : __d
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "downMovingWall",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			},
			"velocity"	: [0.0, -50.0, 0.0] //[m s-1]
		},
		{
			"name"		: "upMovingWall",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			},
			"velocity"	: [0.0, 50.0, 0.0] //[m s-1]
		},
		{
			"name"		: "periodicY0",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},		
		{
			"name"		: "periodicY1",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		},	
		{
			"name"		: "periodicZ0",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		},		
		{
			"name"		: "periodicZ1",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		}		
	]
}
	

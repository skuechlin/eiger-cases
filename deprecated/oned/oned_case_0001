{
	"description": [
	"VISCOSITY COEFFICIENT OF ARGON",
	"BIRD, 12.2.",
	"DATA AS IN DSMC1.FOR",
	"MOVE BOTH WALLS, ONE -500m/s, THE OTHER +500m/s"
	],
	"simulation": {
		"n timesteps"	: 140001,
		"info interval" : 100
	},
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.5e-6,
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	"algorithm": {
		"type"	: "FOKKER-PLANCK"
	},
	"simulation box": {
		"origin": [ 0, 0, 0 ],
		"extent": [ 1, 1, 1 ]
	},
	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.64e-26,
		"d_ref" 	: 4.092e-10,
		"omega" 	: 0.81,
		"T_ref" 	: 293,
		"gamma"		: 1.67
	},
	"mpi": {
		"nx": 200,
		"ny": 1,
		"nz": 1
	},
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : 100
			},
			"averaging"	: {
				"type" 	: "none"
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : 100,
				"first"	    : 100000
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : 100000
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
	"condition": [
		{
			"name"	: "initCond",
			"type"	: "stream",
			"n"		: 1.4e20,
			"T"		: 273,
			"Ma"	: 0.0,
			"particles per cell" : 200
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: 273
		},
		{
			"name"	: "periodicCond",
			"type"	: "periodic",
			"distance" : 1.0
		}
	],
	"initialization" : {
		"condition"	: "initCond"
	},
	"boundary": [
		{
			"name"		: "downMovingWall",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			},
			"velocity"	: [0.0, -500.0, 0.0]
		},
		{
			"name"		: "upMovingWall",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			},
			"velocity"	: [0.0, 500.0, 0.0]
		},
		{
			"name"		: "periodicY0",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},
		{
			"name"		: "periodicY1",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		},
		{
			"name"		: "periodicZ0",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		},
		{
			"name"		: "periodicZ1",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		}
	]
}

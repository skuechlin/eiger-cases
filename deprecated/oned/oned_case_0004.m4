changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'printf ($1)')]])


define(__d, 1.0) // wall distance

define(__xmin, 0)
define(__xmax, __d)
define(__ymin, 0)
define(__ymax, __d)
define(__zmin, 0)
define(__zmax, __d)

define(__particlespercell, 100)
define(__delay, 100000)
define(__interval,100)

{
	"description": [
	"VISCOSITY COEFFICIENT OF ARGON",	 
	"BIRD, 12.2.",
	"DATA AS IN DSMC1.FOR",
	"MOVE BOTH WALLS, ONE -500m/s, THE OTHER +500m/s",
	"TEST FILTERING WITH FINE FILTER CLOSE TO WALL",
	"COARSE IN MIDDLE"
	],
	
	"simulation": {
		"n timesteps"	: 170001, // 70000 averaging steps
		"info interval" : 100
	},
	
	"time step control": {
		"type"		: "fixed",
		"dt"		: 2.5e-6,	//[s] 
		"scaling" 	: [
			[0,	1.0		]
		]
	},
	
	"algorithm": {
		"type"	: "FOKKER-PLANCK"
	},


define(__lx, calc( abs(__xmax) + abs(__xmin) ))
define(__ly, calc( abs(__ymax) + abs(__ymin) ))
define(__lz, calc( abs(__zmax) + abs(__zmin) ))

	"simulation box": {
		"origin": [ __xmin, __ymin, __zmin ],
		"extent": [ __lx, __ly, __lz ]
	},

// DATA1
//      SP(1,1)=4.092E-10
//      SP(2,1)=293.
//      SP(3,1)=0.81
//      SP(4,1)=0.6015
//      SP(5,1)=6.64E-26
//*--SP(1,N) is the molecular diameter of species N
//*--SP(2,N) is the reference temperature
//*--SP(3,N) is the viscosity-temperatire index
//*--SP(4,N) is the reciprocal of the VSS scattering parameter
//*--SP(5,N) is the molecular mass of species N

	"gas model": {
		"type" 		: "VHS",
		"species" 	: "argon",
		"diatomic"	: false,
		"m"			: 6.64e-26,		//[kg]
		"d_ref" 	: 4.092e-10, 	//[m]
		"omega" 	: 0.81,
		"T_ref" 	: 293,			//[K]
		"gamma"		: 1.67			//[-]
	},	

		
	"mpi": {
		"nx": 800, // one d
		"ny": 1,
		"nz": 1
	},
		
	"grid": [
		{
			"name"		: "sim",
			"output"	: {
				"interval"  : __interval
			},
			"averaging"	: {
				"type" 	: "none"				
			},
			"filter" : {
				"box" : {
					"type" : "polynomial",
					"coefficients" : {
						//     a   bx   by  bz   cx   cy  cz
						"x" : [0.0 32.0 0.0 0.0 -32.0 0.0 0.0]
					}
				},
				"kernel" : {
					"type" : "box"					
				}
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		},
		{
			"name" 		: "sample",
			"output"	: {
				"interval"  : __interval,
				"first"	    : __delay
			},
			"averaging"	: {
				"type" 	: "delayed",
				"delay" : __delay
			},
			"filter" : {
				"box" : {
					"type" : "polynomial",
					"coefficients" : {
						//     a   bx   by  bz   cx   cy  cz
						"x" : [0.0 32.0 0.0 0.0 -32.0 0.0 0.0]
					}
				},
				"kernel" : {
					"type" : "box"					
				}
			},
			"correct cell volumes" : false,
			"rx"		: 1,
			"ry"		: 1,
			"rz"		: 1
		}
	],
		
	//INITIAL CONDITIONS
	
	define(__Tinit, 273)
	define(__Tw, 273)
	define(__n, 1.4e20)
	
	"condition": [
		{
			"name"	: "initCond",
			"type"	: "stream",
			"n"		: __n, 		//[m-3]
			"T"		: __Tinit,	//[K]
			"Ma"	: 0.0,
			"particles per cell" : __particlespercell
		},
		{
			"name"	: "surfaceCond",
			"type"	: "diffusive",
			"T"		: __Tw
		},
		{
			"name"	: "periodicCond",
			"type"	: "periodic",
			"distance" : __d
		}
	],
		
	"initialization" : {
		"condition"	: "initCond"
	},
		
	"boundary": [
		{
			"name"		: "downMovingWall",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "left"
			},
			"velocity"	: [0.0, -500.0, 0.0] //[m s-1]
		},
		{
			"name"		: "upMovingWall",
			"type"		: "default",
			"condition"	: "surfaceCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "right"
			},
			"velocity"	: [0.0, 500.0, 0.0] //[m s-1]
		},
		{
			"name"		: "periodicY0",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "front"
			}
		},		
		{
			"name"		: "periodicY1",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "back"
			}
		},	
		{
			"name"		: "periodicZ0",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "bottom"
			}
		},		
		{
			"name"		: "periodicZ1",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "box",
				"face"	: "top"
			}
		}		
	]
}
	

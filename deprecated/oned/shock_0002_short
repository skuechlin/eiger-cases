{
    "description": [
        "1D SHOCK",
        "Ma 2.0",
        "TIMESTEP BY CFL",
        "UPSTREAM MFP 0.0132 M"
    ],
    "simulation": {
        "n timesteps": 5001,
        "info interval": 100
    },
    "time step control": {
        "type": "fixed",
        "dt": 3e-7,
        "scaling": [
            [
                0,
                1.0
            ]
        ]
    },
    "algorithm": {
        "type": "DSMC-MF"
    },
    "simulation box": {
        "origin": [
            -0.2,
            0.0,
            0.0
        ],
        "extent": [
            0.4,
            1.0,
            1.0
        ]
    },
    "gas model": {
        "type": "VHS",
        "diatomic": false,
        "m": 6.630000000000001e-26,
        "d_ref": 4.17e-10,
        "omega": 0.81,
        "T_ref": 273.0,
        "gamma": 1.6667
    },
    "grid": {
        "topology": {
            "dimension": 1,
            "discretization": {
                "type": "uniform",
                "ni": 1000,
                "nj": 1,
                "nk": 1,
                "level": 0
            },
            "ordering": "morton",
            "coordinate system": {
                "type": "cartesian"
            }
        },
        "particle grid": {
            "name": "default",
            "level": 0
        },
        "data grid": [
            {
                "name": "sim",
                "is simulation grid": true,
                "output": {
                    "interval": 5000
                },
                "averaging": {
                    "type": "none"
                }
            },
            {
                "name": "sample",
                "output": {
                    "interval": 10000,
                    "first": 40000
                },
                "averaging": {
                    "type": "delayed",
                    "delay": 40000
                }
            }
        ]
    },
    "condition": [
        {
            "name": "reference",
            "n": 100000000000000000000.0,
            "number of particles": 1000000.0
        },
        {
            "name": "inCond",
            "type": "stream",
            "PDF": "Maxwellian",
            "n": 100000000000000000000.0,
            "T": 293.0,
            "Ma": 2.0
        },
        {
            "name": "outCond",
            "type": "stream",
            "PDF": "Maxwellian",
            "n": 228570000000000000000.0,
            "T": 608.8906,
            "Ma": 0.607
        },
        {
            "name": "initCond",
            "type": "vacuum"
        },
        {
            "name": "specularCond",
            "type": "specular"
        }
    ],
    "initialization": {
        "condition": "initCond"
    },
    "boundary": [
        {
            "name": "inflow",
            "condition": "inCond",
            "geometry": {
                "type": "parallelogram",
                "box face": "left"
            }
        },
        {
            "name": "inflow",
            "condition": "outCond",
            "geometry": {
                "type": "parallelogram",
                "box face": "right"
            }
        },
        {
            "name": "top",
            "condition": "specularCond",
            "geometry": {
                "type": "parallelogram",
                "box face": "top"
            }
        },
        {
            "name": "bottom",
            "condition": "specularCond",
            "geometry": {
                "type": "parallelogram",
                "box face": "bottom"
            }
        },
        {
            "name": "front",
            "condition": "specularCond",
            "geometry": {
                "type": "parallelogram",
                "box face": "front"
            }
        },
        {
            "name": "back",
            "condition": "specularCond",
            "geometry": {
                "type": "parallelogram",
                "box face": "back"
            }
        }
    ]
}

changecom(//)changequote([[,]])
define(calc, [[esyscmd(perl -e 'use Math::Trig; printf ($1)')]])

//Math
define(__pi,3.141592653589793) //pi

define(__lambda,2.4e-4) //mean free path [m]
define(__uref,620.0) //reference velocity [m/s]
//Geometry Definition
define(__rskimmer,0.5e-3)//radius of the skimmer [m]]
define(__h,calc(__rskimmer/25.0))//grid size [m]
define(__lx,calc(10.0*__rskimmer))
define(__ly,2.5e-2)
define(__lz,calc(100.0*__lambda))
define(__d,calc(10.0*__lambda))//distance between upstream boundary and skimmer [m]
define(__b,60.0e-6)//skimmer inlet thickness (assumed to be 0.05mm, tenth of the radius)
define(__theta,100.0) //inner angle
define(__lp1,60.0*__lambda) //length of plate (parallelogram) 1
//Time Definitions
//define(__ts,calc(0.5*__h/__uref))//time step [s] =3.263e-8s
//define(__tdc,calc(100.0*__lambda/__uref)) //domain crossing time [s]
//define(__ttot,calc(10.0*__tdc))//total simulation time [s]
define(__delay,14000) // guess: 5 domain crossing times at u_ref = 620m/s
define(__tw,293.0) // wall temperature
define(__tol,1.0e-9) // geometry file import abolute tolerance [m]
//Inflow Paramters
define(__tin,47.5) // inflow temperature K
define(__pin,40.0) // inflow pressure Pa
define(__main,4.0) // inflow Mach number
define(__Nref,30e6) // number of particles at reference conditions

{
    "description": [// not interpreted by code
        "ARGON SKIMMER FLOW",
        "D     = 0.5e-3 M        (skimmer diameter)",                     
        "Stagnation conditions:",
        "       p0 = 3000 Pa",
        "       T0 = 293 K",
        "Nozze geometry:",
        "       r* = 3.118332092e-3 M", 
        "       A* = 3.05488324e-5 M2",
        "       r_in = 5e-3 M",
        "       A_in = 7.854e-5 M2",
        " A_in/A* = 2.57095977",
        " Ma_in   = 4.0",
        " T_in    = 47.5 K",
        " p_in    = 40 Pa" 
    ]
    ,"simulation" : {
            "n timesteps"	: calc(2*__delay+1),// total number of timesteps in the simulation, chosen as twice the delay time 
            "info interval" : 1
    },
    "time step control": {
            "type" : "fixed",
            "dt"   : 1.5e-8, // Time Step in [s] Neu: h_ref = 0.1935mm, u_ref = (1+1/sqrt(gamma))*sqrt(gamma*R*T) = 380m/s -> 0.5 h_ref/u_ref = 2.5e-7s  
            "scaling" 	: [ [0,	1.0] ]
    },
    "algorithm": {
            "type"	: "DSMC-MF"//"FP-DSMC",
            //"DSMC"	: {
             //       "type" : "DSMC-MF"
            //},
            //"FOKKER-PLANCK" : {
             //       "type" : "FOKKER-PLANCK"
            //}
    },
    "simulation box": { // bounding box of simulated volume
            "origin": [ 0.0, 0.0, calc(-__d) ], // coordinates of lowest corner, values in m
            "extent": [ __lx, __ly, __lz ] // edge length of box in m
    },
    "gas model": {
        "type"      : "VSS",
        "species"   : "argon", //not interpreted
        "diatomic"  : false,
        "m"         : 6.63e-26,     //[kg]
        "d_ref"     : 4.17e-10,     //[m]
        "alpha"     : 1.0,
        "omega"     : 0.81,
        "T_ref"     : 273.0,        //[K]
        "gamma"     : 1.67          //[-] 1 + 2/f, f=3 (f: DoF)
    },  
    "grid": {
       "topology" : {
           "dimension" : 3,
           "discretization" : {
              "type"  : "uniform",
              "ni"    : calc(int(__lx/__h)), // __lx/h number of cells in x-dimension, approx. 125 cells
              "nj"    : 1, // y
              "nk"    : calc(int(__lz/__h)), // __lz/h z, approx. 1200 cells
              "level" : 0   // background resolution: n(i,j,k) * 2^level
           },
           "ordering" : "hilbert ChI",//"morton",//
           "coordinate system" : {
               "type" : "cartesian"
           }
       },
       "particle grid" : {
            "name"  : "default",
            "level" : 0 
       },
       "data grid" : [
       {
            "name"		 : "sim",
            "is simulation grid" : true,
            "output"	    : {
                "interval" : 700, // time step interval to write output file
                "last" : calc(__delay + 250) // last time step at which to write output
            },
            "averaging"	    : {//instantaneous results
                "type" 	: "none"
            }
        },
        {
            "name" 	    : "sample",
            "output"	    : {
                        "interval"  : 1000 // choose to have output ts coincide with last ts 28000 is multiple of 500
                        ,"first"	: __delay // first time step at which to start writing output files
                },
                "averaging"	: {
                        "type" 	: "delayed", // do exponentially weighted moving time average from timestep 0 to delay-1 with given window size, then arithmetic average
                        "delay"  : __delay,// number of timesteps before beginning time averaging 
                        "window size" : 20
                }
        }
        ],
         "grid adaption" : {
	    "cell volume correction" : {
	       	"correct cell volumes" 	: true,
	    	"remove empty cells"	: true
                //,"boundaries"            : ["Wall1","Wall2","Wall3","Wall ...]
            } 
          // ,"refine intersected cells" : [ {"boundaries" : ["Skimmer","Nozzle"],"level" : 2} ]
          // ,"refine cells inside"      : [
          //      {"boundaries" : ["cylinder","ldisk","rdisk"],"level" : 3}
                //, {"boundaries" : ["cylinder2","ldisk2","rdisk2"],"level" : 0}
          // ]
//		  , "dynamic" : {
//                	  "criterion" : "mfp",
//			  "threshold" : 50.0,
//                        "grid"      : "sample",
//			  "first"     : 50,
//			  "last"      : __delay,
//			  "interval"  : 50
//		}
        },
         "load balancing" : {
                "grid" 	   : "sample",
                "first"    : 250,
                "interval" : 250,
                "last"	   : __delay,
                "threshold" : 0.99
        }
        },
        "condition": [
        {
            "name"  : "reference",      // at given conditions in whole domain, would have this total number of particles
            "p"     : __pin,
            "T"     : __tin,            
            "number of particles" : __Nref 
        },
        {
            "name"	: "freestream",
            "type"	: "stream",
            "Ma"        : __main, // -
            "p"	        : __pin, // Pa 
            "T"	        : __tin , // K
            "direction"	: [ 0.0, 0.0, 1.0 ] 
        },
//        {
//            "name"  : "backgroundone",
//            "type"  : "stream",
//            "p"     : 40.0, // Pa
//            "T"     : __tw, // K
//            "Ma"    : 0.0 // -
//        },
        {
            "name"  : "outflow",
            "type"  : "stream",
            "p"     : 0.01, // Pa
            "T"     : __tw, // K
            "Ma"    : 0.0 // -
        },
        {
                "name"	: "initCond",
                "type"	: "vacuum"
        },
        {	"name" : "vacuumCond",
                "type" : "vacuum"
        },
        {
                "name"	: "specularCond",
                "type"	: "specular"
        },
        {
                "name"	: "surfaceCond",
                "type"	: "diffusive"
        },
	{
		"name" : "periodicCond",
		"type" : "periodic",
		"distance" : __ly
 	}
        ],
        "state": [
                {
                        "name"	: "surfaceState",
                        "T"	: __tw //wall temperature
                }
        ],
	"initialization" : { // initial population of simulation volume
		"condition"	: "vacuumCond"
	},
        "boundary": [
        {
            "name"	: "Wall1",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
              "type" : "parallelogram",
		"vertices" : [ // N=(B-A)x(C-A) MUST point INTO fluid volume!
			[__rskimmer,__ly,0], //A
			[__rskimmer, 0,0], //B
			[calc(__rskimmer+__lp1*sin(__pi/180.0*(__theta-90.0))),__ly,calc(__lp1*cos(__pi/180.0*(__theta-90.0)))]  //C
				]

            } 
            //,"output"   : {
            //    "first"     : -1
            //}
        }
,{
			"name"	: "Wall2",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                    "type" : "parallelogram",
				"vertices" : [ // N=(B-A)x(C-A) MUST point INTO fluid volume!
					[calc(__rskimmer+__b),0,0], //A
					[__rskimmer,0,0], //B
					[calc(__rskimmer+__b),__ly,0]  //C
				]

            } 
}
,{
			"name"	: "Wall3",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                    "type" : "parallelogram",
				"vertices" : [ // N=(B-A)x(C-A) MUST point INTO fluid volume!
					[calc(__rskimmer+__b),0,0], //A
					[calc(__rskimmer+__b),__ly,0],//B
					[__lx,0,calc(__lz-__d)]  //C
				]

            } 
}
,{
			"name"	: "Wall4",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                    "type" : "parallelogram",
				"vertices" : [ // N=(B-A)x(C-A) MUST point INTO fluid volume!
					[__lx,0,calc(__lz-__d)], //A
					[__lx,__ly,calc(__lz-__d)], //B
					[calc(__lx-__b),0,calc(__lz-__d)]  //C
				]

            } 
}
,{
			"name"	: "Wall5",
            "condition"	: "surfaceCond",
            "state"	: "surfaceState",
            "geometry"	: {
                    "type" : "parallelogram",
				"vertices" : [ // N=(B-A)x(C-A) MUST point INTO fluid volume!
					[calc(__rskimmer+__lp1*sin(__pi/180.0*(__theta-90.0))),0,calc(__lp1*cos(__pi/180.0*(__theta-90.0)))], //A
					[calc(__lx-__b),0,calc(__lz-__d)], //B
					[calc(__rskimmer+__lp1*sin(__pi/180.0*(__theta-90.0))),__ly,calc(__lp1*cos(__pi/180.0*(__theta-90.0)))]  //C
				]

            } 
}
,{
			"name"	: "topleft",
            "condition"	: "outflow",
            "geometry"	: {
                    "type" : "parallelogram",
				"vertices" : [ // N=(B-A)x(C-A) MUST point INTO fluid volume!
					[calc(__lx-__b),__ly,calc(__lz-__d)], //A
					[calc(__lx-__b),0,calc(__lz-__d)], //B
					[0,__ly,calc(__lz-__d)]  //C
				]

            } 
}
,{
			"name"	: "LeftBoundary",
            "condition"	: "specularCond",
            "geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "left"
			}
}
,{
			"name"	: "RightBoundary",
            "condition"	: "freestream",
            "geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "right"
			}
}
,{
			"name"	: "BottomBoundary",
            "condition"	: "freestream",
            "geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "bottom"
			}
}
,{
			"name"		: "periodicY0",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "front"
			}
		},		
		{
			"name"		: "periodicY1",
			"type"		: "default",
			"condition"	: "periodicCond",
			"geometry"	: {
				"type"	: "parallelogram",
				"box_face"	: "back"
			}
		}	
            ]
}
